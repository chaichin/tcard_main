/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.huawei.hms.base.availableupdate;

public final class R {
    private R() {}

    public static final class color {
        private color() {}

        public static final int emui_color_gray_1 = 0x7f040037;
        public static final int emui_color_gray_10 = 0x7f040038;
        public static final int emui_color_gray_7 = 0x7f040039;
        public static final int upsdk_blue_text_007dff = 0x7f04006d;
        public static final int upsdk_category_button_select_pressed = 0x7f04006e;
        public static final int upsdk_color_gray_1 = 0x7f04006f;
        public static final int upsdk_color_gray_10 = 0x7f040070;
        public static final int upsdk_color_gray_7 = 0x7f040071;
        public static final int upsdk_white = 0x7f040072;
    }
    public static final class dimen {
        private dimen() {}

        public static final int upsdk_margin_l = 0x7f050076;
        public static final int upsdk_margin_m = 0x7f050077;
        public static final int upsdk_margin_xs = 0x7f050078;
        public static final int upsdk_master_body_2 = 0x7f050079;
        public static final int upsdk_master_subtitle = 0x7f05007a;
    }
    public static final class drawable {
        private drawable() {}

        public static final int upsdk_btn_emphasis_normal_layer = 0x7f060081;
        public static final int upsdk_cancel_bg = 0x7f060082;
        public static final int upsdk_cancel_normal = 0x7f060083;
        public static final int upsdk_cancel_pressed_bg = 0x7f060084;
        public static final int upsdk_third_download_bg = 0x7f060085;
        public static final int upsdk_update_all_button = 0x7f060086;
    }
    public static final class id {
        private id() {}

        public static final int action = 0x7f070027;
        public static final int allsize_textview = 0x7f070041;
        public static final int appsize_textview = 0x7f070043;
        public static final int cancel_bg = 0x7f07004b;
        public static final int cancel_imageview = 0x7f07004c;
        public static final int content_layout = 0x7f070059;
        public static final int content_textview = 0x7f07005a;
        public static final int divider = 0x7f070062;
        public static final int download_info_progress = 0x7f070063;
        public static final int enable_service_text = 0x7f070065;
        public static final int hms_message_text = 0x7f070075;
        public static final int hms_progress_bar = 0x7f070076;
        public static final int hms_progress_text = 0x7f070077;
        public static final int name_layout = 0x7f070096;
        public static final int name_textview = 0x7f070097;
        public static final int scroll_layout = 0x7f0700a9;
        public static final int size_layout = 0x7f0700b9;
        public static final int third_app_dl_progress_text = 0x7f0700d1;
        public static final int third_app_dl_progressbar = 0x7f0700d2;
        public static final int third_app_warn_text = 0x7f0700d3;
        public static final int version_layout = 0x7f0700dd;
        public static final int version_textview = 0x7f0700de;
    }
    public static final class layout {
        private layout() {}

        public static final int activity_endisable_service = 0x7f09001c;
        public static final int hms_download_progress = 0x7f09001e;
        public static final int upsdk_app_dl_progress_dialog = 0x7f090032;
        public static final int upsdk_ota_update_view = 0x7f090033;
    }
    public static final class string {
        private string() {}

        public static final int app_name = 0x7f0c0035;
        public static final int hms_abort = 0x7f0c0052;
        public static final int hms_abort_message = 0x7f0c0053;
        public static final int hms_base_google = 0x7f0c0054;
        public static final int hms_base_vmall = 0x7f0c0055;
        public static final int hms_bindfaildlg_message = 0x7f0c0056;
        public static final int hms_bindfaildlg_title = 0x7f0c0057;
        public static final int hms_cancel = 0x7f0c0058;
        public static final int hms_check_failure = 0x7f0c0059;
        public static final int hms_check_no_update = 0x7f0c005a;
        public static final int hms_checking = 0x7f0c005b;
        public static final int hms_confirm = 0x7f0c005c;
        public static final int hms_download_failure = 0x7f0c005d;
        public static final int hms_download_no_space = 0x7f0c005e;
        public static final int hms_download_retry = 0x7f0c005f;
        public static final int hms_downloading = 0x7f0c0060;
        public static final int hms_downloading_loading = 0x7f0c0061;
        public static final int hms_downloading_new = 0x7f0c0062;
        public static final int hms_gamebox_name = 0x7f0c0063;
        public static final int hms_install = 0x7f0c0064;
        public static final int hms_install_message = 0x7f0c0065;
        public static final int hms_retry = 0x7f0c0069;
        public static final int hms_update = 0x7f0c006a;
        public static final int hms_update_continue = 0x7f0c006b;
        public static final int hms_update_message = 0x7f0c006c;
        public static final int hms_update_message_new = 0x7f0c006d;
        public static final int hms_update_nettype = 0x7f0c006e;
        public static final int hms_update_title = 0x7f0c006f;
        public static final int upsdk_app_dl_installing = 0x7f0c007f;
        public static final int upsdk_app_download_info_new = 0x7f0c0080;
        public static final int upsdk_app_size = 0x7f0c0081;
        public static final int upsdk_app_version = 0x7f0c0082;
        public static final int upsdk_cancel = 0x7f0c0083;
        public static final int upsdk_checking_update_prompt = 0x7f0c0084;
        public static final int upsdk_choice_update = 0x7f0c0085;
        public static final int upsdk_connect_server_fail_prompt_toast = 0x7f0c0086;
        public static final int upsdk_detail = 0x7f0c0087;
        public static final int upsdk_getting_message_fail_prompt_toast = 0x7f0c0088;
        public static final int upsdk_install = 0x7f0c0089;
        public static final int upsdk_no_available_network_prompt_toast = 0x7f0c008a;
        public static final int upsdk_ota_app_name = 0x7f0c008b;
        public static final int upsdk_ota_cancel = 0x7f0c008c;
        public static final int upsdk_ota_force_cancel_new = 0x7f0c008d;
        public static final int upsdk_ota_notify_updatebtn = 0x7f0c008e;
        public static final int upsdk_ota_title = 0x7f0c008f;
        public static final int upsdk_storage_utils = 0x7f0c0090;
        public static final int upsdk_store_url = 0x7f0c0091;
        public static final int upsdk_third_app_dl_cancel_download_prompt_ex = 0x7f0c0092;
        public static final int upsdk_third_app_dl_install_failed = 0x7f0c0093;
        public static final int upsdk_third_app_dl_sure_cancel_download = 0x7f0c0094;
        public static final int upsdk_update_check_no_new_version = 0x7f0c0095;
        public static final int upsdk_updating = 0x7f0c0096;
    }
    public static final class style {
        private style() {}

        public static final int upsdkDlDialog = 0x7f0d0161;
    }
}
