import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';
import 'dart:ui';
import 'package:intl/intl.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppointmentDatePickerInput extends StatefulWidget {
  final String label;
  final controller;
  final min;
  final onsaved;
  final validate;
  AppointmentDatePickerInput(
      {this.label, this.controller, this.min, this.onsaved, this.validate});
  @override
  State<StatefulWidget> createState() {
    return _DatePickerState(controller);
  }
}

class _DatePickerState extends State<AppointmentDatePickerInput> {
  final _formatCtrl;
  final String MIN_DATETIME = '1950-01-01';
  final String MIN_DATETIMETODAY =
      DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String MAX_DATETIME = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String INIT_DATETIME = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String INIT_DATETIME_FOR_DISPLAY =
      DateFormat('dd / MM / yyyy').format(DateTime.now());

  // Set the locale

  // Set the format used for parsing
  String _format = 'yyyy-MMMM-dd';

  // Init the text controller

  //TextEditingController _panelPasscodeCtrl = TextEditingController();

  // Global variable so that anywhere also can access this dateTime
  DateTime _dateTime;
  _DatePickerState(this._formatCtrl);

  @override
  void initState() {
    super.initState();
    _dateTime = DateTime.parse(INIT_DATETIME);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          child: TextFormField(
            style: TextStyle(fontWeight: FontWeight.w500),
            validator: widget.validate,
            onSaved: widget.onsaved,
            readOnly: true,
            onTap: () {
              _showDatePicker();
            },
            controller: _formatCtrl,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.teal)),
              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              isDense: true,
              suffixIconConstraints: BoxConstraints(
                minHeight: 10,
              ),
              suffixIcon: Container(
                padding: EdgeInsets.all(5.0),
                child: SvgPicture.asset("assets/images/ic_calendar.svg",
                    width: 20, height: 20, color: Mycolor.darkGreen),
              ),
              labelText: widget.label,
              labelStyle: TextStyle(
                  color: Mycolor.darkGreen,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Mycolor.darkGreen),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Mycolor.darkGreen),
              ),
            ),
          )),
    );
  }

  void _showDatePicker() {
    showDatePicker(
      context: context,
      firstDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day + 1),
      lastDate: DateTime(DateTime.now().year + 30),
      initialDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day + 1),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light().copyWith(
              primary: Mycolor.darkGreen,
            ),
          ),
          isMaterialAppTheme: true,
          child: child,
        );
      },
    ).then((date) {
      setState(() {
        print("data" + date.toString());
        _dateTime = date;
        if (date != null) {
          _formatCtrl.text = DateFormat('dd / MM / yyyy').format(_dateTime);
        }
      });
    });
  }
}
