import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcard/components/confirmation_dialogs.dart';
import 'package:tcard/models/branches.dart';
import 'package:tcard/screens/appointment_page.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:maps_launcher/maps_launcher.dart';

class LocationItem extends StatelessWidget {
  final Allbranches branch;

  LocationItem({this.branch});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Mycolor.white,
          border:
              Border(bottom: BorderSide(color: Mycolor.darkgrey, width: 1))),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Image.asset(
                          'assets/images/location.png',
                          width: 30,
                          height: 30,
                        ),
                      ),
                      Text(
                        '${branch.distance}KM',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Mycolor.redOrange),
                      )
                    ],
                  ),
                ),

                Expanded(
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(//mainAxisAlignment: MainAxisAlignment.end,
                          //crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,

                          // margin: EdgeInsets.only(
                          //     right: MediaQuery.of(context).size.width * 0.1),
                          // width: MediaQuery.of(context).size.width * 0.62,
                          child: Text(
                            branch.name,
                            maxLines: 1,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                            // textAlign: TextAlign.left,
                          ),
                        ),
                      ]),
                      Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Text(
                            branch.address,
                            //textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w600),
                          ))
                    ],
                  ),
                ),

                Expanded(
                  flex: 2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () async {
                          print(branch.phoneNumber);
                          showDialog(
                              context: context,
                              builder: (context) => DialogConfirm(
                                  title: "",
                                  message: "Call ${branch.phoneNumber}",
                                  proceedtext: 'Ok',
                                  proceed: branch.phoneNumber));
                          // String number = branch.phoneNumber; //set the number here
                          // bool res =
                          //     await FlutterPhoneDirectCaller.callNumber(number);
                          //print(res);
                        },
                        child: Image.asset(
                          'assets/images/call.png',
                          width: 30,
                          height: 30,
                        ),
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AppointmentPage()));
                          },
                          child: Image.asset(
                            'assets/images/appointment.png',
                            width: 30,
                            height: 30,
                          )),
                      InkWell(
                          onTap: () {
                            MapsLauncher.launchCoordinates(
                                double.parse(branch.latitude),
                                double.parse(branch.longitude),
                                branch.name);
                          },
                          child: Image.asset(
                            'assets/images/googlemaps.png',
                            width: 30,
                            height: 30,
                          )),
                    ],
                  ),
                )

                // Container(
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: [
                //       FlatButton(
                //         padding: EdgeInsets.symmetric(horizontal: 10),

                //         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                //         //padding: EdgeInsets.all(0),
                //         //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                //         height: 50,

                //         child: Row(
                //           children: [
                //             SvgPicture.asset(
                //               'assets/images/google-maps.svg',
                //               width: MediaQuery.of(context).size.width * 0.01,
                //               height: MediaQuery.of(context).size.height * 0.04,
                //             ),
                //             Container(
                //                 margin: EdgeInsets.only(left: 5),
                //                 width: MediaQuery.of(context).size.width * 0.1,
                //                 child: Text(
                //                   'Get Direction',
                //                   style: TextStyle(
                //                       fontWeight: FontWeight.bold,
                //                       color: Mycolor.darkGreen,
                //                       fontSize:
                //                           MediaQuery.of(context).size.width *
                //                               0.017),
                //                 ))
                //           ],
                //         ),
                //         onPressed: () => MapsLauncher.launchCoordinates(
                //             double.parse(branch.latitude),
                //             double.parse(branch.longitude),
                //             branch.name),
                //         shape: RoundedRectangleBorder(
                //             borderRadius: BorderRadius.circular(5),
                //             side: BorderSide(color: Mycolor.darkGreen)),
                //       ),
                //       FlatButton(
                //         padding: EdgeInsets.symmetric(horizontal: 10),
                //         //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                //         height: 50,
                //         child: Row(
                //           children: [
                //             SvgPicture.asset(
                //               'assets/images/flat_calendar.svg',
                //               width: MediaQuery.of(context).size.width * 0.01,
                //               height: MediaQuery.of(context).size.height * 0.04,
                //             ),
                //             Container(
                //                 margin: EdgeInsets.only(left: 5),
                //                 width: MediaQuery.of(context).size.width * 0.12,
                //                 child: Text(
                //                   'Make An Appointment',
                //                   maxLines: 2,
                //                   style: TextStyle(
                //                       fontWeight: FontWeight.bold,
                //                       color: Mycolor.darkGreen,
                //                       fontSize:
                //                           MediaQuery.of(context).size.width *
                //                               0.017),
                //                 ))
                //           ],
                //         ),
                //         onPressed: () {
                //           Navigator.push(
                //               context,
                //               MaterialPageRoute(
                //                   builder: (context) => AppointmentPage()));
                //         },
                //         shape: RoundedRectangleBorder(
                //             borderRadius: BorderRadius.circular(5),
                //             side: BorderSide(color: Mycolor.darkGreen)),
                //       ),
                //       FlatButton(
                //         padding: EdgeInsets.symmetric(horizontal: 10),
                //         materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                //         height: 50,
                //         child: Row(
                //           children: [
                //             SvgPicture.asset(
                //               'assets/images/phone-call.svg',
                //               width: MediaQuery.of(context).size.width * 0.01,
                //               height: MediaQuery.of(context).size.height * 0.04,
                //             ),
                //             Container(
                //                 margin: EdgeInsets.only(left: 5),
                //                 width: MediaQuery.of(context).size.width * 0.1,
                //                 child: Text(
                //                   'Call Outlet',
                //                   style: TextStyle(
                //                       fontWeight: FontWeight.bold,
                //                       color: Mycolor.darkGreen,
                //                       fontSize:
                //                           MediaQuery.of(context).size.width *
                //                               0.017),
                //                 ))
                //           ],
                //         ),
                //         onPressed: () async {
                //           print(branch.phoneNumber);
                //           showDialog(
                //               context: context,
                //               builder: (context) => DialogConfirm(
                //                   title: "",
                //                   message: "Call ${branch.phoneNumber}",
                //                   proceedtext: 'Ok',
                //                   proceed: branch.phoneNumber));
                //           // String number = branch.phoneNumber; //set the number here
                //           // bool res =
                //           //     await FlutterPhoneDirectCaller.callNumber(number);
                //           //print(res);
                //         },
                //         shape: RoundedRectangleBorder(
                //             borderRadius: BorderRadius.circular(5),
                //             side: BorderSide(color: Mycolor.darkGreen)),
                //       ),
                //     ],
                //   ),
                // )
              ],
            ),
          ),

          // Expanded(
          //     child: Row(
          //   children: [
          //     SvgPicture.asset('assets/images/location.svg'),
          //   ],
          // )),
          // Expanded(
          //     child: Row(
          //   children: [],
          // ))
        ],
      ),
    );
  }
}
