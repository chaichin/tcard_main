import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

import 'buttons.dart';

class DialogRelogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(children: [
        // Image.asset(
        //   "assets/images/tcard_logo.png",
        //   width: 60,
        //   height: 60,
        // ),
        Text(
          'Dear Member',
          style: TextStyle(
              color: Mycolor.darkGreen,
              fontSize: 24,
              fontWeight: FontWeight.w900),
        )
      ]),
      content: Container(
        // child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Please login again',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Button('LOGIN/SIGNUP HERE !', () {
              Navigator.pushReplacementNamed(context, '/home');
            })
          ],
        ),
      ),
    );
  }
}
