import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class HistoryVoucher extends StatelessWidget {
  final title;
  final name;
  final date;
  final amount;
  final point;
  final location;
  final walletype;
  final index;
  final invoiceid;
  HistoryVoucher(
      {this.title,
      this.name,
      this.date,
      this.amount,
      this.point,
      this.location,
      this.walletype,
      this.index,
      this.invoiceid});
  @override
  Widget build(BuildContext context) {
    return Container(
        // width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
        padding: EdgeInsets.only(left: 0, bottom: 10, right: 0, top: 10),
        child: Column(children: [
          index == 0
              ? Container(
                  // alignment: Alignment.center,
                  color: Mycolor.grey,
                  margin: EdgeInsets.only(bottom: 10),
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  // decoration:
                  //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),

                  child: Text(
                    'Point redemption history',
                    // 'All Locations (${branches.length})',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Mycolor.darkgrey,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                )
              : Container(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            walletype == 'IN'
                                ? invoiceid != null
                                    ? invoiceid
                                    : '-'
                                : title,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        ),

                        // location.toString().isEmpty
                        //     ? Text(
                        //         'Gamuda Walk',
                        //         style: TextStyle(fontWeight: FontWeight.bold),
                        //       )
                        //     : Container()
                      ],
                    ),
                    Text(
                      name,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    walletype != null
                        ? Text(
                            '$date \nReceipt Amount : RM $amount',
                            style: TextStyle(color: Mycolor.darkgrey),
                          )
                        : Text('$date',
                            style: TextStyle(color: Mycolor.darkgrey)),
                  ],
                )),
                walletype != null
                    ? walletype == 'OUT'
                        ? Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                                Text(
                                  '- $point',
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                )
                              ])
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                                Text('+ $point',
                                    style: TextStyle(
                                        color: Colors.lightGreen[700],
                                        fontWeight: FontWeight.bold))
                              ])
                    : Container()
              ],
            ),
          )
        ]));
  }
}
