import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class DropDown extends StatefulWidget {
  final String value;
  final validate;
  final handleChange;
  final text;
  final list;
  final onsaved;
  DropDown(
      {Key key,
      this.value,
      this.validate,
      this.handleChange,
      this.text,
      this.list,
      this.onsaved})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _DropDownState();
  }
}

class _DropDownState extends State<DropDown> {
  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is disposed.
  //   textController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
              child: IndexedStack(
                children: [
                  DropdownButtonFormField<String>(
                    onSaved: widget.onsaved,
                    isExpanded: true,
                    validator: widget.validate,
                    onChanged: widget.handleChange,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal),
                      ),
                      alignLabelWithHint: true,
                      contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      isDense: true,
                      labelText: widget.list[0],
                      labelStyle: TextStyle(
                        color: Mycolor.darkGreen,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Mycolor.darkGreen),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Mycolor.darkGreen),
                      ),
                    ),
                    isDense: true,
                    value: widget.text == widget.list[0] ? null : widget.text,
                    items: widget.list
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        child: Text(
                          value,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        value: value,
                      );
                    }).toList(),
                  )
                ],
              ),
              //     InputDecorator(
              //   decoration: InputDecoration(
              //     // suffixIcon: Icon(Icons.arrow_drop_down),
              //     border: OutlineInputBorder(
              //       borderSide: BorderSide(color: Colors.teal),
              //     ),
              //     alignLabelWithHint: true,
              //     contentPadding: EdgeInsets.fromLTRB(10, 15, 10, 15),
              //     isDense: true,
              //     labelText: widget.list[0],
              //     labelStyle: TextStyle(
              //         color: Mycolor.darkGreen,
              //         fontSize: 14,
              //         fontWeight: FontWeight.w300),
              //     enabledBorder: OutlineInputBorder(
              //       borderSide: BorderSide(color: Mycolor.darkGreen),
              //     ),
              //     focusedBorder: OutlineInputBorder(
              //       borderSide: BorderSide(color: Mycolor.darkGreen),
              //     ),
              //   ),
              //   //isFocused: false,
              //   isEmpty: widget.text == null || widget.text == widget.list[0]
              //       ? true
              //       : false,
              //   child: DropdownButtonHideUnderline(
              //     child: DropdownButtonFormField(
              //       decoration: InputDecoration.collapsed(hintText: ''),
              //       isExpanded: true,
              //       // value: _currentSelectedValue,
              //       validator: widget.validation,
              //       value: widget.text == widget.list[0] ? null : widget.text,
              //       isDense: true,
              //       onChanged: widget.handleChange,
              //       //  (String value) {
              //       //   print(value);

              //       //   setState(() {
              //       //     _currentSelectedValue = value;
              //       //   });
              //       // },
              //       items: widget.list
              //           .map<DropdownMenuItem<String>>((String value) {
              //         return DropdownMenuItem<String>(
              //           child: Text(value, overflow: TextOverflow.ellipsis),
              //           value: value,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              // )
            ),
          ),
        ],
      ),
    ]);
  }
}
