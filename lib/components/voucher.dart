import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';
import 'confirmcashvoucher_dialogs.dart';

class Voucher extends StatelessWidget {
  final redeemed;
  final myvoucher;
  final title;
  final description;
  final pointBalance;
  final voucherid;
  final changeTab;
  Voucher(
      {this.redeemed,
      this.myvoucher,
      this.title,
      this.description,
      this.pointBalance,
      this.voucherid,
      this.changeTab});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print(voucherid);
        showDialog(
            context: context,
            builder: (context) => DialogCVConfirm(
                pointBalance: pointBalance,
                voucherId: voucherid,
                changeTab: changeTab));
      },
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          height: 80,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            //crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('$title',
                        style: TextStyle(
                            height: 1,
                            color:
                                redeemed ? Mycolor.darkgrey : Mycolor.darkGreen,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text('E-Cash',
                        style: TextStyle(
                          height: 1,
                          fontWeight: FontWeight.w600,
                          color:
                              redeemed ? Mycolor.darkgrey : Mycolor.darkGreen,
                          fontSize: 14,
                        )),
                    Text('$description',
                        style: TextStyle(
                            height: 1,
                            color:
                                redeemed ? Mycolor.darkgrey : Mycolor.darkGreen,
                            fontWeight: FontWeight.w500))
                  ],
                ),
              ),
              Container(
                padding: myvoucher
                    ? EdgeInsets.symmetric(horizontal: 5)
                    : EdgeInsets.symmetric(horizontal: 35),

                // alignment: Alignment(1, 0),
                child: myvoucher
                    ? Column(mainAxisAlignment: MainAxisAlignment.center,
                        // /crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                            Text('USE NOW',
                                style: TextStyle(
                                    color: redeemed
                                        ? Mycolor.darkgrey
                                        : Mycolor.darkGreen,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                            Text('Valid untill 30 November 2020',
                                style: TextStyle(
                                    color: redeemed
                                        ? Mycolor.darkgrey
                                        : Mycolor.darkGreen,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 6))
                          ])
                    : Text('COLLECT\nNOW',
                        textWidthBasis: TextWidthBasis.parent,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color:
                                redeemed ? Mycolor.darkgrey : Mycolor.darkGreen,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
              )
            ],
          ),
          decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            image: DecorationImage(
              image: redeemed
                  ? AssetImage('assets/images/voucher-grey.png')
                  : AssetImage('assets/images/voucher-green.png'),
              fit: BoxFit.cover,
            ),
          )),
    );
  }
}
