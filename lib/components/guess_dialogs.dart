import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/utils/mycolor.dart';

class DialogGuest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        title: Column(children: [
          Text('Hi Guest',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w900,
                  color: Mycolor.darkGreen))
        ]),
        content: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Text(
            "It seems that you are not logged in!",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          Button('Login/Signup here!', () {
            Navigator.popUntil(context, ModalRoute.withName('/home'));
          }),
          Button('Cancel', () {
            Navigator.of(context).pop();
          }),
        ]));
  }
}
