import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/utils/mycolor.dart';

class NewSliderItem extends StatelessWidget {
  bool notNull(Object o) => o != null;
  final flex;
  final value;
  final source;
  final title;
  final logo;
  final price;
  final points;
  final productid;
  final views;
  NewSliderItem(
      {this.value,
      this.flex,
      this.source,
      this.title,
      this.logo,
      this.productid,
      this.price,
      this.points,
      this.views});
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: flex ? 15 : 10),
        child: InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () async {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ShoppingPage(productid: productid, views: views)));
              // if (PostData.logintoken != null) {

              //   }

              // else {
              //   showDialog(context: context, builder: (context) => DialogGuest());
              // }
              // Navigator.pushNamed(context, '/shopping');
            },
            child: Padding(
              padding: EdgeInsets.only(left: 0, right: 0),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      CachedNetworkImage(
                        imageUrl: source,
                        imageBuilder: (context, imageProvider) => AspectRatio(
                            aspectRatio: 16 / 9,
                            child: Container(
                              // margin: EdgeInsets.only(bottom: 20),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        offset: Offset(0, 0),
                                        //blurRadius: 10,
                                        color: Colors.black.withOpacity(0.10))
                                  ],
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: flex ? BoxFit.fill : BoxFit.fill,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15),
                                    bottomLeft: Radius.circular(15),
                                    bottomRight: Radius.circular(15),
                                  )
                                  // BorderRadius.circular(30),
                                  // border: Border.all(
                                  //   color: Colors.black,
                                  //   //width: 1,
                                  // ),
                                  ),
                              width: flex ? null : 200,
                              height: flex ? null : 200 * 0.56,
                            )),
                        placeholder: (BuildContext context, String url) =>
                            Container(),
                        // If error happens load image by using NetworkImage
                        errorWidget: (context, url, error) {
                          return Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(100)),
                              image: DecorationImage(
                                image: NetworkImage(source),
                                alignment: Alignment.center,
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      ),
                      Container(
                          padding: EdgeInsets.only(left: flex ? 70 : 50),
                          // padding: EdgeInsets.symmetric(horizontal: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Mycolor.darkGreen,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15))),
                          width: flex ? null : 200,
                          height: flex ? 60 : 35,
                          // /color: Mycolor.redOrange,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      width: flex ? 200 : 100,
                                      //value != 'shopping' ? 180 : null,
                                      // flex: 1,
                                      child: Text(
                                        "$title",
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            height: 1,
                                            fontSize: flex ? 18 : 14,
                                            color: Mycolor.normalWhite),
                                      )),
                                  // Container(

                                  //     // flex: 1,
                                  //     child: Text(
                                  //   "Redeem now!",
                                  //   maxLines: 1,
                                  //   overflow: TextOverflow.ellipsis,
                                  //   textAlign: TextAlign.left,
                                  //   style: TextStyle(
                                  //       fontSize: flex
                                  //           ? MediaQuery.of(context)
                                  //                   .size
                                  //                   .width *
                                  //               0.04
                                  //           : MediaQuery.of(context)
                                  //                   .size
                                  //                   .width *
                                  //               0.03,
                                  //       color: Mycolor.normalWhite,
                                  //       fontWeight: FontWeight.bold),
                                  // )),
                                ],
                              ),
                              value == 'shopping'
                                  ? Expanded(
                                      flex: 3,
                                      child: Container(
                                          padding: EdgeInsets.only(right: 10),
                                          //margin: EdgeInsets.only(right: 10),
                                          alignment: Alignment.centerRight,
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                // Text(
                                                //   'RM$price & $points pts ',
                                                //   style: TextStyle(
                                                //       fontSize: flex
                                                //           ? 10
                                                //           : MediaQuery.of(
                                                //                       context)
                                                //                   .size
                                                //                   .width *
                                                //               0.02,
                                                //       color: Mycolor
                                                //           .normalWhite),
                                                // ),
                                                // Image.asset(
                                                //     'assets/images/teeth.png',
                                                //     width: 10,
                                                //     height: 10),
                                                Icon(
                                                  Icons
                                                      .arrow_forward_ios_rounded,
                                                  size: 15,
                                                  color: Mycolor.normalWhite,
                                                )
                                              ])))
                                  : Expanded(
                                      flex: 3,
                                      child: Container(
                                          padding: EdgeInsets.only(right: 10),
                                          //margin: EdgeInsets.only(right: 10),
                                          alignment: Alignment.centerRight,
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                // Text(
                                                //   'RM$price & $points pts ',
                                                //   style: TextStyle(
                                                //       fontSize: flex
                                                //           ? 10
                                                //           : MediaQuery.of(
                                                //                       context)
                                                //                   .size
                                                //                   .width *
                                                //               0.02,
                                                //       color: Mycolor
                                                //           .normalWhite),
                                                // ),
                                                // Image.asset(
                                                //     'assets/images/teeth.png',
                                                //     width: 10,
                                                //     height: 10),
                                                Icon(
                                                  Icons
                                                      .arrow_forward_ios_rounded,
                                                  size: 15,
                                                  color: Mycolor.normalWhite,
                                                )
                                              ]))),
                            ],
                          )),
                      Positioned(
                          left: flex ? 15 : 10,
                          bottom: flex ? 35 : 15,
                          child: CachedNetworkImage(
                            imageUrl: logo,
                            imageBuilder: (context, imageProvider) => Container(
                              //margin: EdgeInsets.only(left: 30, top: 0),
                              width: flex ? 50 : 40,
                              height: flex ? 50 : 40,
                              decoration: BoxDecoration(
                                image: DecorationImage(image: imageProvider),
                                borderRadius: BorderRadius.circular(100),
                                color: Colors.grey,
                              ),
                            ),
                            placeholder: (BuildContext context, String url) =>
                                Container(),
                            // If error happens load image by using NetworkImage
                            errorWidget: (context, url, error) {
                              return Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100)),
                                  image: DecorationImage(
                                    image: NetworkImage(logo),
                                    alignment: Alignment.center,
                                    fit: BoxFit.cover,
                                    scale: 1,
                                  ),
                                ),
                              );
                            },
                          )),
                    ],
                  )

                  // Container(
                  //     padding: EdgeInsets.only(left: flex ? 70 : 50),
                  //     // padding: EdgeInsets.symmetric(horizontal: 10),
                  //     alignment: Alignment.center,
                  //     decoration: BoxDecoration(
                  //         color: Mycolor.darkGreen,
                  //         borderRadius: BorderRadius.only(
                  //             bottomLeft: Radius.circular(15),
                  //             bottomRight: Radius.circular(15))),
                  //     width: flex
                  //         ? MediaQuery.of(context).size.width
                  //         : 230,
                  //     height: flex ? 45 : 35,
                  //     // /color: Mycolor.redOrange,
                  //     child: Row(
                  //       // mainAxisAlignment: MainAxisAlignment.center,
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       children: [
                  //         Column(
                  //           crossAxisAlignment:
                  //               CrossAxisAlignment.start,
                  //           mainAxisAlignment: MainAxisAlignment.center,
                  //           children: [
                  //             Container(
                  //                 width: flex ? 200 : 100,
                  //                 //value != 'shopping' ? 180 : null,
                  //                 // flex: 1,
                  //                 child: Text(
                  //                   "$title",
                  //                   maxLines: 2,
                  //                   overflow: TextOverflow.ellipsis,
                  //                   textAlign: TextAlign.left,
                  //                   style: TextStyle(
                  //                       height: 1,
                  //                       fontSize: flex ? 18 : 14,
                  //                       color: Mycolor.normalWhite),
                  //                 )),
                  //             // Container(

                  //             //     // flex: 1,
                  //             //     child: Text(
                  //             //   "Redeem now!",
                  //             //   maxLines: 1,
                  //             //   overflow: TextOverflow.ellipsis,
                  //             //   textAlign: TextAlign.left,
                  //             //   style: TextStyle(
                  //             //       fontSize: flex
                  //             //           ? MediaQuery.of(context)
                  //             //                   .size
                  //             //                   .width *
                  //             //               0.04
                  //             //           : MediaQuery.of(context)
                  //             //                   .size
                  //             //                   .width *
                  //             //               0.03,
                  //             //       color: Mycolor.normalWhite,
                  //             //       fontWeight: FontWeight.bold),
                  //             // )),
                  //           ],
                  //         ),
                  //         value == 'shopping'
                  //             ? Expanded(
                  //                 flex: 3,
                  //                 child: Container(
                  //                     padding:
                  //                         EdgeInsets.only(right: 10),
                  //                     //margin: EdgeInsets.only(right: 10),
                  //                     alignment: Alignment.centerRight,
                  //                     child: Row(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.end,
                  //                         children: [
                  //                           // Text(
                  //                           //   'RM$price & $points pts ',
                  //                           //   style: TextStyle(
                  //                           //       fontSize: flex
                  //                           //           ? 10
                  //                           //           : MediaQuery.of(
                  //                           //                       context)
                  //                           //                   .size
                  //                           //                   .width *
                  //                           //               0.02,
                  //                           //       color: Mycolor
                  //                           //           .normalWhite),
                  //                           // ),
                  //                           // Image.asset(
                  //                           //     'assets/images/teeth.png',
                  //                           //     width: 10,
                  //                           //     height: 10),
                  //                           Icon(
                  //                             Icons
                  //                                 .arrow_forward_ios_rounded,
                  //                             size: 15,
                  //                             color:
                  //                                 Mycolor.normalWhite,
                  //                           )
                  //                         ])))
                  //             : Expanded(
                  //                 flex: 3,
                  //                 child: Container(
                  //                     padding:
                  //                         EdgeInsets.only(right: 10),
                  //                     //margin: EdgeInsets.only(right: 10),
                  //                     alignment: Alignment.centerRight,
                  //                     child: Row(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.end,
                  //                         children: [
                  //                           // Text(
                  //                           //   'RM$price & $points pts ',
                  //                           //   style: TextStyle(
                  //                           //       fontSize: flex
                  //                           //           ? 10
                  //                           //           : MediaQuery.of(
                  //                           //                       context)
                  //                           //                   .size
                  //                           //                   .width *
                  //                           //               0.02,
                  //                           //       color: Mycolor
                  //                           //           .normalWhite),
                  //                           // ),
                  //                           // Image.asset(
                  //                           //     'assets/images/teeth.png',
                  //                           //     width: 10,
                  //                           //     height: 10),
                  //                           Icon(
                  //                             Icons
                  //                                 .arrow_forward_ios_rounded,
                  //                             size: 15,
                  //                             color:
                  //                                 Mycolor.normalWhite,
                  //                           )
                  //                         ]))),
                  //       ],
                  //     ))
                  ),

              // Container(
              //     alignment: Alignment.center,
              //     // padding: EdgeInsets.only(left: 10),
              //     margin: EdgeInsets.only(
              //         left: flex
              //             ? MediaQuery.of(context).size.width * 0.602
              //             : 165,
              //         top: 125),
              //     width: 100,
              //     height: 20,
              //     child: Text(
              //       '$views views',
              //       style: TextStyle(color: Mycolor.white),
              //     ),
              //     decoration: BoxDecoration(
              //         color: Mycolor.darkGreen,
              //         borderRadius: BorderRadius.only(
              //             bottomLeft: Radius.circular(20),
              //             topLeft: Radius.circular(20)))),
            )));
  }
}
