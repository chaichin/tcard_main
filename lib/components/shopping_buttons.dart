import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class ShoppingButton extends StatelessWidget {
  final image;
  final label;
  final navigation;

  final size;
  ShoppingButton(this.image, this.label, this.navigation, this.size);
  @override
  Widget build(BuildContext context) {
    return Container(
      //margin: EdgeInsets.all(20),
      child: GestureDetector(
        onTap: navigation,
        child: Column(children: [
          Text(
            '$label',
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: size == 'small' ? Mycolor.grey : Mycolor.redOrange),
          ),
          Image.asset(
            "assets/images/$image.png",
            width: 30,
            height: 30,
          ),
        ]),
      ),
    );
  }
}
