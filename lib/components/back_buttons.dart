import 'package:flutter/material.dart';

class ReturnButton extends StatelessWidget {
  final push;
  final navigation;
  final white;
  ReturnButton({this.push, this.navigation, this.white});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // Navigator.pop(context);
        push == null
            ? Navigator.pop(context)
            : push == 'replace'
                ? Navigator.pushReplacementNamed(context, '/main')
                : Navigator.popUntil(context, ModalRoute.withName('/'));
      },
      child: SizedBox(
        width: 60,
        height: 60,
        child: Container(
            // margin: EdgeInsets.all(8.0),
            padding: EdgeInsets.all(12.0),
            child: white != null
                ? Image.asset('assets/images/backbutton_white.png')
                : Image.asset('assets/images/backbutton_dark.png')),
      ),
    );
  }
}
