import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';
import 'dart:ui';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DatePickerInput extends StatefulWidget {
  final String label;
  final controller;
  final min;
  final onsaved;
  final validate;
  DatePickerInput(
      {this.label, this.controller, this.min, this.onsaved, this.validate});
  @override
  State<StatefulWidget> createState() {
    return _DatePickerState(controller);
  }
}

class _DatePickerState extends State<DatePickerInput> {
  final _formatCtrl;
  final String MIN_DATETIME = '1950-01-01';
  final String MIN_DATETIMETODAY =
      DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String MAX_DATETIME = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String INIT_DATETIME = DateFormat('yyyy-MM-dd').format(DateTime.now());
  final String INIT_DATETIME_FOR_DISPLAY =
      DateFormat('dd / MM / yyyy').format(DateTime.now());

  // Set the locale
  DateTimePickerLocale _locale = DateTimePickerLocale.en_us;

  // Set the format used for parsing
  String _format = 'yyyy-MMMM-dd';

  // Init the text controller

  //TextEditingController _panelPasscodeCtrl = TextEditingController();

  // Global variable so that anywhere also can access this dateTime
  DateTime _dateTime;
  _DatePickerState(this._formatCtrl);

  @override
  void initState() {
    super.initState();
    _dateTime = DateTime.parse(INIT_DATETIME);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Container(
          margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
          child: TextFormField(
            style: TextStyle(fontWeight: FontWeight.w500),
            validator: widget.validate,
            onSaved: widget.onsaved,
            readOnly: true,
            onTap: () {
              _showDatePicker();
            },
            controller: _formatCtrl,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.teal)),
              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              isDense: true,
              suffixIconConstraints: BoxConstraints(
                minHeight: 10,
              ),
              suffixIcon: Container(
                padding: EdgeInsets.all(5.0),
                child: SvgPicture.asset("assets/images/ic_calendar.svg",
                    width: 20, height: 20, color: Mycolor.darkGreen),
              ),
              labelText: widget.label,
              labelStyle: TextStyle(
                  color: Mycolor.darkGreen,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Mycolor.darkGreen),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Mycolor.darkGreen),
              ),
            ),
          )),
    );
  }

  void _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm:
            Text('Choose Date', style: TextStyle(color: Mycolor.darkGreen)),
        cancel: Text('Cancel', style: TextStyle(color: Colors.red)),
      ),
      minDateTime: widget.min
          ? DateTime(
              DateTime.now().year, DateTime.now().month, DateTime.now().day + 1)
          : DateTime.parse(MIN_DATETIME),
      maxDateTime: !widget.min ? DateTime.parse(MAX_DATETIME) : null,
      initialDateTime: _dateTime,
      dateFormat: _format,
      locale: _locale,
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
          _formatCtrl.text = DateFormat('dd / MM / yyyy').format(_dateTime);
        });
      },
    );
  }
}
