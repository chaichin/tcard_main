import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class Button extends StatelessWidget {
  final String value;
  final navigation;
  Button(this.value, this.navigation);
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        minWidth: 120.0,
        //height: 100.0,
        child: RaisedButton(
          color: value == 'Cancel' ? Mycolor.normalWhite : Mycolor.darkGreen,
          textColor: value == 'Cancel' ? Mycolor.darkGreen : Mycolor.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100.0)),
          onPressed: navigation,
          child: Text(
            "$value",
            style: TextStyle(fontWeight: FontWeight.bold, height: 1),
          ),
        ));
  }
}
