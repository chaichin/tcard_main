import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/models/newproduct.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:url_launcher/url_launcher.dart';

class Carousel extends StatefulWidget {
  final product;
  Carousel({this.product});
  @override
  State<StatefulWidget> createState() {
    return CarouselState(product);
  }
}

class CarouselState extends State<Carousel> {
  final product;
  CarouselState(this.product);
  int _current = 0;
  List<BigCarousel> list;
  @override
  void initState() {
    super.initState();
    setState(() {
      list = product;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      Stack(alignment: Alignment.center, children: [
        ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CarouselSlider(
                options: CarouselOptions(
                    viewportFraction: 1,
                    enlargeCenterPage: false,
                    initialPage: 0,
                    enableInfiniteScroll: true,
                    reverse: false,
                    autoPlay: true,
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    }),
                items: list
                    .map((item) => InkWell(
                          onTap: () async {
                            print(item.productId);

                            if (item.productId != null) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ShoppingPage(
                                          productid: item.productId)));
                            } else {
                              if (await canLaunch(item.url)) {
                                await launch(item.url);
                              } else {
                                throw 'Could not launch $item.url)';
                              }
                            }
                          },
                          child: CachedNetworkImage(
                            imageUrl: item.image,
                            imageBuilder: (context, imageProvider) => Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.fill, image: imageProvider),
                                color: Colors.grey,
                              ),
                            ),
                            placeholder: (BuildContext context, String url) =>
                                Container(),
                            // If error happens load image by using NetworkImage
                            errorWidget: (context, url, error) {
                              return Container(
                                // height: MediaQuery.of(context).size.height * 0.25,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(100)),
                                  image: DecorationImage(
                                    image: NetworkImage(item.image),
                                    alignment: Alignment.center,
                                    fit: BoxFit.cover,
                                    scale: 1,
                                  ),
                                ),
                              );
                            },
                          ),
                          // Container(
                          //   decoration:
                          //       BoxDecoration(borderRadius: BorderRadius.circular(30)),
                          //   // padding: EdgeInsets.symmetric(
                          //   //     horizontal: 10, vertical: 10),
                          //   child: Center(
                          //       child: CachedNetworkImage(
                          //     imageUrl: item.image,
                          //     //"https://via.placeholder.com/1920x1080",
                          //     fit: BoxFit.cover,
                          //     height: MediaQuery.of(context).size.width,
                          //     width: MediaQuery.of(context).size.width,
                          //   )),
                          // )
                        ))
                    .toList())),
        Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 150),
            // right: 145,

            // bottom: 0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: list.map((e) {
                  int index = list.indexOf(e);
                  return Container(
                    width: 5,
                    height: 5,
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 2),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Mycolor.darkGreen,
                          // width: 0.7,
                        ),
                        shape: BoxShape.circle,
                        color: _current == index
                            ? Mycolor.darkerGreen
                            : Mycolor.white),
                  );
                }).toList())),
      ])
    ]);
  }
}
