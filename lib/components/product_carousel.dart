import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ProductCarousel extends StatefulWidget {
  final product;
  ProductCarousel({this.product});
  @override
  State<StatefulWidget> createState() {
    return ProductCarouselState(product);
  }
}

class ProductCarouselState extends State<ProductCarousel> {
  final product;
  ProductCarouselState(this.product);
  int _current = 0;
  List list;
  @override
  void initState() {
    super.initState();
    setState(() {
      list = product;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, children: [
      CarouselSlider(
        options: CarouselOptions(
            // height: 160,
            viewportFraction: 1,
            enlargeCenterPage: false,
            initialPage: 0,
            enableInfiniteScroll: true,
            reverse: false,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            onPageChanged: (index, reason) {
              setState(() {
                _current = index;
              });
            }),
        items: list
            .map((item) => AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(
                  // padding: EdgeInsets.symmetric(
                  //     horizontal: 10, vertical: 10),
                  child: Center(
                      child: CachedNetworkImage(
                    imageUrl: item,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 0),
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.90))
                          ],
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.fill,
                          )),
                    ),
                    //"https://via.placeholder.com/1920x1080",
                    fit: BoxFit.fill,
                    // height: MediaQuery.of(context).size.width,
                    // width: MediaQuery.of(context).size.width,
                  )),
                )))
            .toList(),
      ),
      Positioned(
          // left: MediaQuery.of(context).size.width * 0.5,
          bottom: 0,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: list.map((e) {
                int index = list.indexOf(e);
                return Container(
                  width: 5,
                  height: 5,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 2),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Mycolor.darkGreen,
                        width: 0.7,
                      ),
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Mycolor.darkerGreen
                          : Mycolor.white),
                );
              }).toList())),
    ]);
  }
}
