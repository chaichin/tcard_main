import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: SizedBox(
            width: 150,
            height: 150,
            child: Image.asset("assets/images/tcard_logo.png"),
          ),
        ),
      ],
    );
  }
}
