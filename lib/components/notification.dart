import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/notficationstate.dart';

import 'dialog_update.dart';

class NotificationBadge extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationBadgeState();
  }
}

class NotificationBadgeState extends State<NotificationBadge> {
  int counter = 0;

  @override
  void initState() {
    var dbHelper = DBHelper();
    super.initState();
    dbHelper.getCount().then((value) => setState(() {
          counter = value;
          print("getcount" + counter.toString());
        }));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          PostData.update
              ? showDialog(
                  context: context, builder: (context) => DialogUpdate())
              : Navigator.pushNamed(context, '/notification');
        },
        child: Container(
            // minWidth: 100,
            padding: EdgeInsets.only(top: 14),
            child: Stack(children: [
              Image.asset("assets/images/notification.png",
                  width: 30, height: 30),
              Consumer<NotificationState>(
                  builder: (context, notification, child) {
                int totalcounter = counter + notification.counter;
                return totalcounter != 0
                    ? Positioned(
                        right: 0,

                        //top: 1,
                        child: new Container(
                            //padding: EdgeInsets.all(2),
                            decoration: new BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(100),
                            ),
                            constraints: BoxConstraints(
                              minWidth: 20,
                              minHeight: 20,
                            ),
                            child:
                                // Text(
                                //   // '${count.counter}',
                                //   "$counter",
                                //   style: TextStyle(
                                //     color: Colors.white,
                                //     fontSize: 13,
                                //   ),
                                //   textAlign: TextAlign.center,
                                // ),

                                Text(
                              '$totalcounter',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                              textAlign: TextAlign.center,
                            )),
                      )
                    : Container();
              })
            ])));
  }
}
