import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:url_launcher/url_launcher.dart';

class MiniSlider extends StatelessWidget {
  final title;
  final button;
  final image;
  final description;
  final productid;
  final url;
  MiniSlider(
      {this.button,
      this.image,
      this.description,
      this.url,
      this.productid,
      this.title});
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: InkWell(
      onTap: () async {
        print(productid);
        print(url);
        if (url != null) {
          if (await canLaunch(url)) {
            await launch(url);
          } else {
            throw 'Could not launch $url';
          }
        } else {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ShoppingPage(
                        productid: productid,
                      )));
        }
      },
      child: Container(
          margin: EdgeInsets.only(
            left: 15,
          ),
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            child: Container(
              // width: MediaQuery.of(context).size.width * 0.5,
              // height: MediaQuery.of(context).size.height * 0.12,
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
              //margin: EdgeInsets.all(10),
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 0),
                      child: CachedNetworkImage(
                        width: 60,
                        height: 70,
                        imageUrl: image,

                        placeholder: (BuildContext context, String url) =>
                            Container(),
                        // If error happens load image by using NetworkImage
                        errorWidget: (context, url, error) {
                          return Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(100)),
                              image: DecorationImage(
                                image: NetworkImage(image),
                                alignment: Alignment.center,
                                fit: BoxFit.cover,
                                scale: 1,
                              ),
                            ),
                          );
                        },
                      )),
                  // Image.network(image)),
                  Container(
                      alignment: Alignment.centerLeft,
                      width: 100,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "$title",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                            Container(
                              width: 90,
                              height: 40,
                              // decoration: BoxDecoration(
                              //     border: Border.all(color: Mycolor.darkGreen)),
                              child: Text(
                                "$description",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    height: 1,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                            ),
                            SizedBox(
                              height: 0,
                            ),
                            Container(
                                width: 90,
                                padding: EdgeInsets.only(left: 30),
                                height: 20,
                                // decoration: BoxDecoration(
                                //     border:
                                //         Border.all(color: Mycolor.darkGreen)),
                                child: SizedBox(
                                    width: 50,
                                    child: RaisedButton(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 0),
                                      color: Mycolor.redOrange,
                                      textColor: Mycolor.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      onPressed: () async {
                                        print(productid);
                                        print(url);
                                        if (url != null) {
                                          if (await canLaunch(url)) {
                                            await launch(url);
                                          } else {
                                            throw 'Could not launch $url';
                                          }
                                        } else {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      ShoppingPage(
                                                        productid: productid,
                                                      )));
                                        }
                                      },
                                      child: Text(
                                        button,
                                        style: TextStyle(
                                            fontSize: 14,
                                            height: 1,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )))
                          ])),
                ],
              ),
            ),
          )

          // Container(
          //     alignment: Alignment.center,
          //     decoration: BoxDecoration(
          //         color: Mycolor.redOrange,
          //         borderRadius: BorderRadius.only(
          //             bottomLeft: Radius.circular(15),
          //             bottomRight: Radius.circular(15))),
          //     width: MediaQuery.of(context).size.width * 0.5,
          //     height: 30,
          //     // /color: Mycolor.redOrange,
          //     child: Text(
          //       button,
          //       style: TextStyle(color: Mycolor.white),
          //     ))
          ),
    ));
  }
}
