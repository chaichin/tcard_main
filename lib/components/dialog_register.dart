import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/screens/checkout_page.dart';
import 'package:tcard/screens/scan_page.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:tcard/utils/mycolor.dart';

class DialogRegister extends StatelessWidget {
  final title;
  final message;
  final proceedtext;
  final proceed;

  DialogRegister({
    this.title,
    this.message,
    this.proceedtext,
    this.proceed,
  });
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(children: [
        Text(
          title,
          style: TextStyle(
              color: Mycolor.darkGreen,
              fontSize: 24,
              fontWeight: FontWeight.w900),
        )
      ]),
      content: Container(
        // child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Button(this.proceedtext, proceed),
            Button('Back', () {
              Navigator.of(context).pop();
            }),
          ],
        ),
      ),
    );
  }
}
