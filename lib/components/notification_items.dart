import 'package:flutter/material.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:url_launcher/url_launcher.dart';

class NotificationItem extends StatelessWidget {
  final image;
  final value;
  final days;
  final hours;
  final minutes;
  final type;
  final voucherid;
  final productid;
  final url;
  bool notNull(Object o) => o != null;
  NotificationItem(
      {this.image,
      this.value,
      this.productid,
      this.url,
      this.voucherid,
      this.type,
      this.days,
      this.hours,
      this.minutes});
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () async {
          print(type);
          if (type == 'PRODUCT') {
            print(productid);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ShoppingPage(productid: productid, views: null)));
          } else if (type == 'VOUCHER') {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => VoucherDescriptionPage(
                          voucherid: voucherid,
                        )));
          } else if (type == 'URL') {
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw 'Could not launch $url';
            }
          }
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
          padding: EdgeInsets.only(left: 20, bottom: 10, right: 10, top: 10),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                child: Image.asset(
                  'assets/images/$image.png',
                  width: 30,
                  height: 30,
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('$value',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14)),
                      minutes < 5
                          ? Text('now')
                          : minutes > 5 && minutes < 60
                              ? Text('$minutes minutes ago')
                              : hours < 24
                                  ? Text('$hours hours ago')
                                  : Text('$days days ago')
                      // days < 1
                      //     ? hours < 24
                      //         ? minutes < 5
                      //             ? Text('now')
                      //             : Text('$minutes minutes ago')
                      //         : Text('$hours days ago')
                      //     : Text('$days days ago')
                    ].where(notNull).toList(),
                  )),
              type != 'NORMAL'
                  ? Expanded(
                      flex: 1,
                      child: Container(
                          alignment: Alignment.centerRight,
                          child:
                              Icon(Icons.arrow_forward_ios_rounded, size: 15)),
                    )
                  : Container()
            ],
          ),
        ));
  }
}
