import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/utils/mycolor.dart';
import 'dart:async';

class DialogMessage extends StatefulWidget {
  final title;
  final message;
  final success;
  final mode;
  DialogMessage(this.title, this.message, this.success, this.mode);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogMessageState();
  }
}

class DialogMessageState extends State<DialogMessage> {
  Timer _timer;
  int _start = 5;
  @override
  void initState() {
    _timer = new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(

          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              widget.title == 'Try Again'
                  ? "assets/images/fail.png"
                  : "assets/images/success.png",
              width: 60,
              height: 60,
            ),
            Text(
              widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: widget.title == 'Try Again'
                      ? Colors.red
                      : Mycolor.darkGreen),
            )
          ]),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          widget.message == ''
              ? Container()
              : Text(
                  widget.message,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
          widget.mode == '2' && widget.title != 'Try Again'
              ? Text(
                  "* Please show this code to the person in charge.",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              : Container(),
          SizedBox(
            height: 30,
          ),
          widget.mode == '2'
              ? Button(
                  _start != 0 && widget.title != 'Try Again'
                      ? "$_start"
                      : "Proceed", () {
                  print(widget.success);
                  if (widget.success == true) {
                    if (_start == 0 && widget.title != 'Try Again') {
                      Navigator.popUntil(context, ModalRoute.withName('/main'));
                    } else if (widget.title == 'Try Again') {
                      Navigator.popUntil(context, ModalRoute.withName('/main'));
                    }
                  } else {
                    Navigator.popUntil(
                        context, ModalRoute.withName('/checkout'));
                  }
                })
              : Button('Proceed', () {
                  print(widget.success);
                  if (widget.success == true) {
                    Navigator.popUntil(context, ModalRoute.withName('/main'));
                  } else {
                    Navigator.of(context).pop();
                  }
                }),
        ],
      ),
    );
  }
}
