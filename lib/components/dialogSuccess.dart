import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:flutter/scheduler.dart';

class DialogSuccessMessage extends StatelessWidget {
  final title;
  final message;
  final Function changeTab;
  DialogSuccessMessage(this.title, this.message, this.changeTab);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(children: [
        Image.asset(
          title == 'ERROR'
              ? "assets/images/fail.png"
              : "assets/images/success.png",
          width: 60,
          height: 60,
        ),
        Text('$title',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: title == 'ERROR' ? Colors.red : Mycolor.darkGreen))
      ]),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            message,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          Button('OK', () {
            changeTab();
            SchedulerBinding.instance.addPostFrameCallback((_) {
              Navigator.of(context).pop();
            });
            // Navigator.pushReplacementNamed(context, '/wallet');
            //Navigator.popUntil(context, ModalRoute.withName('/main'));
            //Navigator.pushNamed(context, '/wallet');

            // Navigator.of(context).pushNamedAndRemoveUntil(
            //     '/wallet', ModalRoute.withName('/home'));

            //Navigator.pushReplacementNamed(context, '/wallet');

            //Navigator.popAndPushNamed(context, '/wallet');
            // Navigator.popUntil(context, (route) => false)
          }),
        ],
      ),
    );
  }
}
