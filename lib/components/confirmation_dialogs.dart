import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/screens/checkout_page.dart';
import 'package:tcard/screens/scan_page.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:tcard/utils/mycolor.dart';

class DialogConfirm extends StatelessWidget {
  final title;
  final message;
  final proceedtext;
  final proceed;
  final id;
  final mode;
  final points;
  final amountPaid;
  final image;
  final sellingPriceWithPoint;
  final productTitle;
  DialogConfirm(
      {this.title,
      this.message,
      this.proceedtext,
      this.proceed,
      this.id,
      this.mode,
      this.points,
      this.amountPaid,
      this.image,
      this.productTitle,
      this.sellingPriceWithPoint});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(children: [
        // Image.asset(
        //   "assets/images/tcard_logo.png",
        //   width: 60,
        //   height: 60,
        // ),
        Text(
          title,
          style: TextStyle(
              color: Mycolor.darkGreen,
              fontSize: 24,
              fontWeight: FontWeight.w900),
        )
      ]),
      content: Container(
        // child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Button(this.proceedtext, () async {
              this.proceedtext == "Ok"
                  ? await FlutterPhoneDirectCaller.callNumber(proceed)
                  : mode == '1' || mode == '3'
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ScanPage(
                                    id: id,
                                    mode: mode,
                                    points: points,
                                    amountPaid: amountPaid,
                                  )))
                      : Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CheckoutPage(
                                    id: id,
                                    image: image,
                                    sellingPriceWithPoint:
                                        sellingPriceWithPoint,
                                    points: points,
                                    price: amountPaid,
                                    title: productTitle,
                                  )));
            }),
            Button('Cancel', () {
              Navigator.of(context).pop();
            }),
          ],
        ),
      ),
      //),
      // actions: [
      //   Button(this.proceedtext, () async {
      //     this.proceedtext == "Ok"
      //         ? await FlutterPhoneDirectCaller.callNumber(proceed)
      //         : Navigator.push(
      //             context,
      //             MaterialPageRoute(
      //                 builder: (context) => ScanPage(
      //                       id: id,
      //                       mode: mode,
      //                       points: points,
      //                       amountPaid: amountPaid,
      //                     )));
      //   }),
      //   Button('Cancel', () {
      //     Navigator.of(context).pop();
      //   }),
      // ]
    );
  }
}
