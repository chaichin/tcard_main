import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/services/getUserService.dart';
import 'package:tcard/utils/mycolor.dart';

class CustomDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CustomDrawerState();
  }
}

class CustomDrawerState extends State<CustomDrawer> {
  String phone;
  String name;
  String points;
  String profileimage = '';
  TextEditingController email = TextEditingController();
  String profileEmail;
  DefaultCacheManager manager = new DefaultCacheManager();
  @override
  void initState() {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    super.initState();
    if (PostData.logintoken != null) {
      User().profile().then((value) => setState(() {
            name = value[1];
            phone = value[2];
            //points = value[6];
            //points = prefs.getString('PointBalance');
            print(value[7]);
            profileimage = value[7];
          }));
      User().point().then((value) => setState(() {
            points = value;
          }));
      PostData().getProfile().then((value) {
        print(value);
        setState(() {
          profileEmail = value.email;
        });
      });
    }
  }

  // @override
  // void initState() {
  //   setState(() async {
  //     name = await _prefs.then((value) {
  //       return value.getString('name');
  //     });
  //     phone = await _prefs.then((value) {
  //       return value.getString('phone');
  //     });
  //   });
  //   super.initState();
  // }
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Mycolor
              .darkGreen, //This will change the drawer background to blue.
          //other styles

          splashColor: Colors.white.withOpacity(0.25),
        ),
        child: Container(
            width: MediaQuery.of(context).size.width * 0.7,
            child: Drawer(
              child: ListView(
                // padding: EdgeInsets.zero,
                children: [
                  DrawerHeader(
                      margin: EdgeInsets.only(bottom: 0),
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Container(

                          // margin: EdgeInsets.only(top: 50),
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          profileimage == ""
                              ? Container(
                                  child: Image.asset(
                                    'assets/images/profile_pic.png',
                                    width: 60,
                                    height: 60,
                                  ),
                                )
                              : Container(
                                  child: CachedNetworkImage(
                                    imageUrl: profileimage,
                                    imageBuilder: (context, imageProvider) {
                                      return Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(100)),
                                          image: DecorationImage(
                                            image: imageProvider,
                                            alignment: Alignment.topCenter,
                                            fit: BoxFit.cover,
                                            scale: 1,
                                          ),
                                        ),
                                      );
                                    },
                                    placeholder:
                                        (BuildContext context, String url) =>
                                            Container(),
                                    // If error happens load image by using NetworkImage
                                    errorWidget: (context, url, error) {
                                      return Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(100)),
                                          image: DecorationImage(
                                            image: NetworkImage(profileimage),
                                            alignment: Alignment.center,
                                            fit: BoxFit.cover,
                                            scale: 1,
                                          ),
                                        ),
                                      );
                                    },
                                    width: 60,
                                    height: 60,
                                  ),
                                ),
                          const SizedBox(width: 10),
                          Container(
                            //width: 100,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Hi, ${name == null ? 'Guest' : name}',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Mycolor.normalWhite,
                                        fontWeight: FontWeight.w600)),
                                phone == null
                                    ? Container()
                                    : Text('$phone',
                                        style: TextStyle(
                                            color: Mycolor.normalWhite,
                                            fontWeight: FontWeight.w600)),
                                points == null
                                    ? Container()
                                    : Row(
                                        children: [
                                          //const SizedBox(width: 10),
                                          Text('$points points',
                                              style: TextStyle(
                                                  color: Mycolor.normalWhite,
                                                  fontWeight: FontWeight.w400)),
                                          Image.asset(
                                            'assets/images/teeth.png',
                                            width: 15,
                                            height: 15,
                                          ),
                                        ],
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ))),
                  PostData.logintoken == null
                      ? Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 0),
                          child: RaisedButton(
                            color: Mycolor.normalWhite,
                            textColor: Mycolor.darkGreen,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100.0)),
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  '/home', (Route<dynamic> route) => false);
                            },
                            child: Text(
                              'Login/Sign up here!',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ))
                      : Container(),
                  Container(
                      height: MediaQuery.of(context).size.height,
                      // decoration: new BoxDecoration(color: Mycolor.darkGreen),
                      //color: Mycolor.darkGreen,
                      // margin: EdgeInsets.symmetric(horizontal: 10),
                      child: PostData.logintoken != null
                          ? Column(
                              children: [
                                // ListTile(

                                //   title: Text('Wallet',
                                //       style: TextStyle(
                                //           fontWeight: FontWeight.w500)),
                                //   onTap: () {
                                //     Navigator.pushNamed(context, '/wallet');
                                //   },
                                // ),
                                // Padding(
                                //   padding: EdgeInsets.symmetric(horizontal: 10),
                                //   child: Image.asset('assets/images/line.png'),
                                // ),
                                ListTile(
                                  onTap: () {
                                    PostData.update
                                        ? showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogUpdate())
                                        : Navigator.pushNamed(
                                            context, '/voucher');
                                  },
                                  title: Text('E-News',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                ),

                                ListTile(
                                  title: Text('History',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                  onTap: () {
                                    PostData.update
                                        ? showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogUpdate())
                                        : Navigator.pushNamed(
                                            context, '/historytransaction');
                                  },
                                ),

                                ListTile(
                                  title: Text('Make Dental Appointment',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                  onTap: () {
                                    PostData.update
                                        ? showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogUpdate())
                                        : Navigator.pushNamed(
                                            context, '/appointment');
                                  },
                                ),

                                ListTile(
                                  title: Text('Terms & Conditions',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                  onTap: () {
                                    PostData.update
                                        ? showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogUpdate())
                                        : Navigator.pushNamed(context, '/t&c');
                                  },
                                ),

                                ListTile(
                                  title: Text('Update Email',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                  onTap: () {
                                    PostData.update
                                        ? showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogUpdate())
                                        : showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                                  contentPadding:
                                                      EdgeInsets.fromLTRB(
                                                          10, 20, 10, 10),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  20.0))),
                                                  title: Container(
                                                    child: Text(
                                                      'Update Email',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontSize: 24,
                                                          color:
                                                              Mycolor.darkGreen,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  content: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: [
                                                        Form(
                                                          key: _formKey,
                                                          child: Input(
                                                            value: profileEmail,
                                                            textArea: 1,
                                                            textController:
                                                                email,
                                                            validate: (value) {
                                                              if (value
                                                                  .isEmpty) {
                                                                return 'Email field is required*';
                                                              } else if (!RegExp(
                                                                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                                  .hasMatch(
                                                                      value)) {
                                                                return 'Email format is wrong';
                                                              }
                                                              return null;
                                                            },
                                                            onSaved: (value) {
                                                              email.text =
                                                                  value;
                                                            },
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Text(
                                                            'Click submit to change your email'),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Button('Save', () {
                                                          if (_formKey
                                                              .currentState
                                                              .validate()) {
                                                            PostData()
                                                                .updateProfile(
                                                                    email.text)
                                                                .then((value) {
                                                              if (value
                                                                      .statusCode ==
                                                                  200) {
                                                                setState(() {
                                                                  profileEmail =
                                                                      email
                                                                          .text;
                                                                });
                                                                Navigator.pop(
                                                                    context);
                                                                showDialog(
                                                                    context:
                                                                        context,
                                                                    builder: (context) => DialogMessage(
                                                                        'Update Successful',
                                                                        'Dear $name\nYou have successfully change your email',
                                                                        false,
                                                                        '1'));

                                                                email.clear();
                                                              }
                                                            });
                                                          }
                                                        }),
                                                      ]),
                                                ));
                                  },
                                ),
                                Expanded(
                                    child: Align(
                                  child: ListTile(
                                    title: Text('Logout',
                                        style: TextStyle(
                                            color: Mycolor.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18)),
                                    trailing: Image.asset(
                                        'assets/images/logout.png',
                                        width: 30,
                                        height: 30),
                                    onTap: () async {
                                      SharedPreferences preferences =
                                          await SharedPreferences.getInstance();
                                      // var dir = await getTemporaryDirectory();

                                      // dir.deleteSync(recursive: true);
                                      preferences.clear();

                                      manager.emptyCache();
                                      DBHelper().deleteAll();
                                      //DBHelper().deletedatabase();
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil('/home',
                                              (Route<dynamic> route) => false);
                                      //  Navigator.pushNamed(context, '/');
                                      // Respond to button press
                                    },
                                  ),
                                ))

                                // Container(
                                //     margin: EdgeInsets.only(right: 0, top: 0),
                                //     child: Align(
                                //         alignment: Alignment.bottomRight,
                                //         child: FlatButton.icon(
                                //           textColor: Colors.grey,
                                //           onPressed: () async {
                                //             SharedPreferences preferences =
                                //                 await SharedPreferences
                                //                     .getInstance();
                                //             var dir =
                                //                 await getTemporaryDirectory();

                                //             dir.deleteSync(recursive: true);
                                //             preferences.clear();

                                //             manager.emptyCache();
                                //             DBHelper().deleteAll();
                                //             //DBHelper().deletedatabase();
                                //             Navigator.of(context)
                                //                 .pushNamedAndRemoveUntil(
                                //                     '/home',
                                //                     (Route<dynamic> route) =>
                                //                         false);
                                //             //  Navigator.pushNamed(context, '/');
                                //             // Respond to button press
                                //           },
                                //           icon: Icon(Icons.logout, size: 18),
                                //           label: Text(
                                //             "Logout",
                                //           ),
                                //         )))
                              ],
                            )
                          : Column(
                              children: [
                                ListTile(
                                  title: Text('Terms & Conditions',
                                      style: TextStyle(
                                          color: Mycolor.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                  onTap: () {
                                    Navigator.pushNamed(context, '/t&c');
                                  },
                                ),
                              ],
                            )),
                ],
              ),
            )));
  }
}
