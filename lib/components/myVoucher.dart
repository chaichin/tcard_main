import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:tcard/utils/mycolor.dart';

class MyVoucher extends StatelessWidget {
  final redeemed;
  final myvoucher;
  final title;
  final expiredate;
  final voucherid;
  MyVoucher(
      {this.redeemed,
      this.myvoucher,
      this.title,
      this.expiredate,
      this.voucherid});
  @override
  Widget build(BuildContext context) {
    var expirydate = DateFormat('dd-MM-yyyy').format(expiredate);
    return GestureDetector(
      onTap: () {
        print(voucherid);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VoucherDescriptionPage(
                      voucherid: voucherid,
                    )));
      },
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          width: 400,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            //crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                height: 100,
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  //mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      title,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                              redeemed ? Mycolor.darkgrey : Mycolor.darkGreen,
                          fontSize: MediaQuery.of(context).size.width * 0.05,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  // alignment: Alignment(1, 0),
                  child: Column(mainAxisAlignment: MainAxisAlignment.center,
                      // /crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('USE NOW',
                            style: TextStyle(
                                color: redeemed
                                    ? Mycolor.darkgrey
                                    : Mycolor.darkGreen,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                        Text('Valid untill',
                            style: TextStyle(
                                color: redeemed
                                    ? Mycolor.darkgrey
                                    : Mycolor.darkGreen,
                                fontWeight: FontWeight.w400,
                                fontSize: 14)),
                        Text('$expirydate',
                            style: TextStyle(
                                color: redeemed
                                    ? Mycolor.darkgrey
                                    : Mycolor.darkGreen,
                                fontWeight: FontWeight.w600,
                                fontSize: 14))
                      ]))
            ],
          ),
          decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            image: DecorationImage(
              image: redeemed
                  ? AssetImage('assets/images/voucher-grey.png')
                  : AssetImage('assets/images/voucher-green.png'),
              fit: BoxFit.cover,
            ),
          )),
    );
  }
}
