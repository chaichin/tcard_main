import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class tcardProgressDialog extends StatelessWidget {
  tcardProgressDialog();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return null;
      },
      child: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 64,
                  height: 64,
                  child: Image.asset("assets/images/tcard_logo.png"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: LinearProgressIndicator(
                  backgroundColor: Mycolor.lightGreen,
                  valueColor: AlwaysStoppedAnimation<Color>(Mycolor.darkGreen),
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Mycolor.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black,
                    blurRadius: 2.0,
                    spreadRadius: 0.0,
                    offset: Offset(2.0, 2.0))
              ]),
          width: 100,
        ),
      ),
    );
  }
}
