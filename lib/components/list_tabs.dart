import 'package:flutter/material.dart';
import 'package:tcard/models/products.dart';

import 'newSlider.dart';

class ListTab extends StatelessWidget {
  final List<Products> products;
  final type;
  final value;
  ListTab({this.products, this.type, this.value});
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.only(right: 20),
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        itemCount: products.length,
        itemBuilder: (context, index) {
          List<String> imgList =
              products[index].images.split(',').map((e) => e).toList();
          if (type == 'SPECIAL') {
            if (products[index].panelType != 'NONE' &&
                products[index].panelType != null) {
              String typeP = products[index].productType == 'SHOPPING'
                  ? 'shopping'
                  : 'merchant';
              return NewSliderItem(
                flex: true,
                value: typeP,
                logo: products[index].merchantLogo,
                source: imgList[0],
                title: products[index].title,
                productid: products[index].productId,
                views: products[index].numberOfViews,
                price: products[index].sellingPrice,
                points: products[index].pointsNeeded,
              );
            } else {
              return Container();
            }
          } else {
            if (products[index].productType == type) {
              return NewSliderItem(
                flex: true,
                value: value,
                logo: products[index].merchantLogo,
                source: imgList[0],
                title: products[index].title,
                productid: products[index].productId,
                views: products[index].numberOfViews,
                price: products[index].sellingPrice,
                points: products[index].pointsNeeded,
              );
            } else {
              return Container();
            }
          }
        });
  }
}
