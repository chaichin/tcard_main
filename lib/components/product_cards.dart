import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class Productcard extends StatefulWidget {
  final description;
  final termsCondition;

  Productcard({this.description, this.termsCondition});
  @override
  State<StatefulWidget> createState() {
    return ProductcardState();
  }
}

class ProductcardState extends State<Productcard> {
  bool flag = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 5, top: 10, left: 20),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Description ',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  // textAlign: TextAlign.start,
                ),
              ),
              // Divider(
              //   thickness: 1,
              // ),
              flag
                  ? Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '${widget.description}',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    )
                  : Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        children: [
                          Text(
                            '${widget.description}',
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 14),
                            textAlign: TextAlign.justify,
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 5, top: 10),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Terms & Conditions',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                              // textAlign: TextAlign.start,
                            ),
                          ),
                          Text(
                            '${widget.termsCondition}',
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 14),
                            textAlign: TextAlign.justify,
                          )
                        ],
                      ),
                    ),
              // Divider(
              //   thickness: 1,
              // ),
              InkWell(
                onTap: () {
                  setState(() {
                    flag = !flag;
                    print(flag);
                  });
                },
                child: Container(
                  padding: EdgeInsets.only(bottom: 15, top: 5),
                  alignment: Alignment.center,
                  child: Text(
                    flag ? ' See more ' : 'See less',
                    style: TextStyle(
                        color: Mycolor.redOrange, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
