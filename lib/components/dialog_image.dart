import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:url_launcher/url_launcher.dart';

class DialogImage extends StatefulWidget {
  final image;
  final url;
  final productid;
  DialogImage({this.image, this.url, this.productid});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogImageState();
  }
}

class DialogImageState extends State<DialogImage> {
  bool show = false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Stack(children: [
            InkWell(
                onTap: () async {
                  print(widget.productid);
                  print(widget.url);
                  if (widget.url != null) {
                    if (await canLaunch(widget.url)) {
                      await launch(widget.url);
                    } else {
                      throw 'Could not launch ${widget.url}';
                    }
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ShoppingPage(
                                  productid: widget.productid,
                                )));
                  }
                },
                child: CachedNetworkImage(
                  // width: 300,
                  // height: 300,
                  imageUrl: widget.image,
                  imageBuilder: (context, imageProvider) => Container(
                    width: 300,
                    height: 300,
                    margin: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  // placeholder: (context, url) => CircularProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )),
            Positioned(
              right: 0,
              child: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ]),
          Container(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Theme(
                  data: ThemeData(unselectedWidgetColor: Colors.white),
                  child: Checkbox(
                    value: show,
                    onChanged: (bool value) async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool('show', value);
                      print('here' + value.toString());
                      setState(() {
                        show = value;
                      });

                      print('dsa' + show.toString());
                    },
                  )),
              Text(
                'Do not show again',
                style: TextStyle(color: Colors.white),
              )
            ],
          ))
        ]));
  }
}
