import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MerchantItem extends StatelessWidget {
  final source;
  final navigation;
  MerchantItem({this.source, this.navigation});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: navigation,
      child: Container(
        child: CachedNetworkImage(
          imageUrl: source,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 0),
                    //blurRadius: 10,
                    color: Colors.black.withOpacity(0.10))
              ],
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.all(Radius.circular(100)),
              // BorderRadius.circular(30),
              // border: Border.all(
              //   color: Colors.black,
              //   //width: 1,
              // ),
            ),
          ),
          placeholder: (BuildContext context, String url) => Container(),
          // If error happens load image by using NetworkImage
          errorWidget: (context, url, error) {
            return Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100)),
                image: DecorationImage(
                  image: NetworkImage(source),
                  alignment: Alignment.center,
                  fit: BoxFit.cover,
                  scale: 1,
                ),
              ),
            );
          },
        ),

        // child: Image.asset(
        //   'assets/images/square.png',
        // ),
      ),
    );
  }
}
