import 'package:flutter/material.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/utils/mycolor.dart';

class SliderItem extends StatelessWidget {
  bool notNull(Object o) => o != null;
  final flex;
  final value;
  final source;
  final title;
  final logo;
  final productid;
  final views;
  SliderItem(
      {this.value,
      this.flex,
      this.source,
      this.title,
      this.logo,
      this.productid,
      this.views});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ShoppingPage(productid: productid, views: views)));

        // Navigator.pushNamed(context, '/shopping');
      },
      child: Stack(
        children: [
          Container(
              //padding: EdgeInsets.all(0.0),
              height: 180,
              width: flex ? MediaQuery.of(context).size.width : 260.0,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              //color: Colors.red,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 0),
                        //blurRadius: 10,
                        color: Colors.black.withOpacity(0.10))
                  ],
                  image: DecorationImage(
                    image: NetworkImage(source),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(21),
                      bottomRight: Radius.circular(21))
                  // BorderRadius.circular(30),
                  // border: Border.all(
                  //   color: Colors.black,
                  //   //width: 1,
                  // ),
                  ),
              child: Container(
                // width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 20),
                margin: EdgeInsets.only(
                  top: 130,
                ),
                // color: Colors.white,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    color: Mycolor.grey),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        flex: 2,
                        child: Text(
                          "$title",
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        )),
                    value == 'shopping'
                        ? Expanded(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(right: 10),
                                alignment: Alignment.centerRight,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(
                                        'RM25 & 50 pts ',
                                        style: TextStyle(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      Image.asset('assets/images/teeth.png',
                                          width: 10, height: 10)
                                    ])))
                        : Expanded(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(right: 10),
                                alignment: Alignment.centerRight,
                                child: Icon(Icons.arrow_forward_ios_rounded,
                                    size: 15)),
                          )
                  ],
                ),
              )),
          Container(
              margin: EdgeInsets.only(
                  left: !flex ? 230 : MediaQuery.of(context).size.width * 0.806,
                  top: 133),
              width: 50,
              child: Text(
                'Views : $views',
                style: TextStyle(color: Mycolor.white),
              ),
              decoration: BoxDecoration(
                  color: Mycolor.darkGreen,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      topLeft: Radius.circular(20)))),
          !flex
              ? Container(
                  margin: EdgeInsets.only(left: 40, top: 5),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    image: DecorationImage(image: NetworkImage(logo)),
                    color: Colors.grey,
                  ),
                )
              : flex && value == 'merchant'
                  ? Container(
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.1,
                          top: 5),
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        image: DecorationImage(image: NetworkImage(logo)),
                        color: Colors.grey,
                      ),
                    )
                  : null
        ].where(notNull).toList(),
      ),
    );
  }
}
