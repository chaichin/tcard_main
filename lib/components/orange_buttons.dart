import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class OrangeButton extends StatelessWidget {
  final String value;
  final navigation;
  final flex;
  OrangeButton(this.value, this.navigation, this.flex);
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        minWidth: !flex ? MediaQuery.of(context).size.width * 0.4 : 0,
        height: 100.0,
        child: RaisedButton(
          color: Mycolor.redOrange,
          textColor: Mycolor.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
          onPressed: navigation,
          child: Text(
            "$value",
            style: TextStyle(
                fontSize: !flex
                    ? MediaQuery.of(context).size.width * 0.03
                    : MediaQuery.of(context).size.width * 0.02,
                fontWeight: FontWeight.w300),
          ),
        ));
  }
}
