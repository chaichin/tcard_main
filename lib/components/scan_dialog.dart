import 'package:flutter/material.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/utils/mycolor.dart';
import 'dart:async';

class ScanDialogMessage extends StatefulWidget {
  final title;
  final message;
  final success;
  final Function action;
  ScanDialogMessage({this.title, this.message, this.success, this.action});
  @override
  State<StatefulWidget> createState() {
    return ScanDialogMessageState();
  }
}

class ScanDialogMessageState extends State<ScanDialogMessage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(

          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              widget.title == 'Try Again'
                  ? "assets/images/fail.png"
                  : "assets/images/success.png",
              width: 60,
              height: 60,
            ),
            Text(
              widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: widget.title == 'Try Again'
                      ? Colors.red
                      : Mycolor.darkGreen),
            )
          ]),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          widget.message == ''
              ? Container()
              : Text(
                  widget.message,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
          SizedBox(
            height: 30,
          ),
          Button('Proceed', widget.action),
        ],
      ),
    );
  }
}
