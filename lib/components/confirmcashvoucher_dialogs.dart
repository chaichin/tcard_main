import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialogSuccess.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/utils/mycolor.dart';

class DialogCVConfirm extends StatelessWidget {
  final pointBalance;
  final voucherId;
  final changeTab;
  DialogCVConfirm({this.pointBalance, this.voucherId, this.changeTab});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        title: Column(children: [
          Image.asset(
            "assets/images/success.png",
            width: 60,
            height: 60,
          ),
          Text('Dear Member',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Mycolor.darkGreen))
        ]),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Click Redeem cash voucher will be\nredeemed.",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 30,
            ),
            Button('Redeem', () async {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                Navigator.of(context).pop();
              });
              PostData()
                  .redeemCashVoucher(pointBalance, voucherId)
                  .then((value) {
                showDialog(
                    context: context,
                    builder: (context) => DialogSuccessMessage(
                        value.data['status'],
                        value.data['message'],
                        changeTab));
              });
              //Navigator.of(context).pop();
              // Navigator.popUntil(context, ModalRoute.withName('/home'));
              // Navigator.pushNamed(context, '/wallet');
            }),
            Button('Cancel', () {
              Navigator.of(context).pop();
            }),
          ],
        ),

        //),
        actions: []);
  }
}
