import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class DropDownSlot extends StatefulWidget {
  final String value;
  final validate;
  final handleChange;
  final text;
  final list;
  DropDownSlot(
      {Key key,
      this.value,
      this.validate,
      this.handleChange,
      this.text,
      this.list})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _DropDownSlotState();
  }
}

class _DropDownSlotState extends State<DropDownSlot> {
  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is disposed.
  //   textController.dispose();
  //   super.dispose();
  // }
  List<String> initialSlot = ['Available Slot'];
  // @override
  // void initState() {
  //   PostData().getAppointmentDetails().then((value) {
  //     value.clinics.map((e) {
  //       if (e.clinicName == widget.value) {
  //         initialSlot.addAll(e.workingHours);
  //       }
  //     });
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Container(
              margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
              child: InputDecorator(
                decoration: InputDecoration(
                  // suffixIcon: Icon(Icons.arrow_drop_down),
                  alignLabelWithHint: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                  isDense: true,
                  labelText: widget.value,
                  labelStyle: TextStyle(
                      color: Mycolor.darkGreen,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Mycolor.darkGreen),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Mycolor.darkGreen),
                  ),
                ),
                //isFocused: false,
                isEmpty: widget.text == null || widget.text == widget.list[0]
                    ? true
                    : false,
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    isExpanded: true,
                    // value: _currentSelectedValue,
                    value: widget.text == widget.list[0] ? null : widget.text,
                    isDense: true,
                    onChanged: widget.handleChange,
                    //  (String value) {
                    //   print(value);

                    //   setState(() {
                    //     _currentSelectedValue = value;
                    //   });
                    // },
                    items: initialSlot
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        child: Text(
                          value,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        value: value,
                      );
                    }).toList(),
                  ),
                ),
              )),
        ),
      ],
    );
  }
}
