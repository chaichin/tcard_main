import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class HistoryItem extends StatelessWidget {
  final displayPrice;
  final discountType;
  final discountText;
  final image;
  final title;
  final date;
  final point;
  final location;
  final walletype;
  final index;
  final showDisplay;
  final productType;
  HistoryItem(
      {this.title,
      this.date,
      this.point,
      this.location,
      this.walletype,
      this.index,
      this.discountType,
      this.displayPrice,
      this.image,
      this.discountText,
      this.showDisplay,
      this.productType});

  @override
  Widget build(BuildContext context) {
    return Container(
        // width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
        padding: EdgeInsets.only(left: 0, bottom: 10, right: 0, top: 10),
        child: Column(children: [
          index == 0
              ? Container(
                  // alignment: Alignment.center,
                  color: Mycolor.grey,
                  margin: EdgeInsets.only(bottom: 10),
                  padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  // decoration:
                  //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),

                  child: Text(
                    'Claimed rewards',
                    // 'All Locations (${branches.length})',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Mycolor.darkgrey,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                )
              : Container(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: CachedNetworkImage(
                    imageUrl: image,
                    width: 80,
                    height: 50,
                    fit: BoxFit.cover,
                  ),
                ),

                // Container(
                //     margin: EdgeInsets.symmetric(horizontal: 10),
                //     width: 80,
                //     height: 50,
                //     decoration: BoxDecoration(
                //         border: Border.all(color: Colors.red),
                //         image: DecorationImage(image: NetworkImage(image)))),
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        productType == 'VOUCHER'
                            ? Expanded(
                                child: Text(
                                  showDisplay,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w900,
                                      color: showDisplay == 'FREE'
                                          ? Colors.red
                                          : Mycolor.darkerGreen),
                                ),
                              )
                            : Container(),

                        // location.toString().isEmpty
                        //     ? Text(
                        //         'Gamuda Walk',
                        //         style: TextStyle(fontWeight: FontWeight.bold),
                        //       )
                        //     : Container()
                        productType != 'VOUCHER'
                            ? Expanded(
                                child: Text(
                                  title,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Mycolor.darkerGreen),
                                ),
                              )
                            : Container(),
                        Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Text(
                              productType,
                              style: TextStyle(
                                color: Mycolor.darkGreen,
                                fontWeight: FontWeight.bold,
                              ),
                            ))
                      ],
                    ),
                    Row(
                      children: [
                        productType == 'VOUCHER'
                            ? Expanded(
                                child: Text(
                                  title,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Mycolor.darkerGreen),
                                ),
                              )
                            : Container()

                        // location.toString().isEmpty
                        //     ? Text(
                        //         'Gamuda Walk',
                        //         style: TextStyle(fontWeight: FontWeight.bold),
                        //       )
                        //     : Container()
                      ],
                    ),
                    Text(
                        productType == 'VOUCHER' &&
                                showDisplay == "Expired voucher"
                            ? 'Expired by $date'
                            : '$date',
                        style: TextStyle(
                            color: Mycolor.darkgrey,
                            fontWeight: FontWeight.w400)),
                  ],
                )),
              ],
            ),
          )
        ]));
  }
}
