import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:provider/provider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/Notification.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/services/huawei_notification_service.dart';
import 'package:tcard/services/notificationService.dart';
import 'package:tcard/utils/mycolor.dart';

import 'buttons.dart';
import 'dialogs.dart';

class OtpDialog extends StatelessWidget {
  final data;

  OtpDialog({Key key, this.data}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var platform = Theme.of(context).platform;
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          'OTP Verification',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Mycolor.darkGreen,
              fontSize: 24,
              fontWeight: FontWeight.w900),
        )
      ]),
      content: Container(
        // child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'A SMS has been sent to ${data['phone']}',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context, '/signup');
              },
              child: Row(
                children: [
                  Expanded(
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Text("Wrong Number?",
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Mycolor.urlBlue,
                                fontSize: 14,
                                fontWeight: FontWeight.w300)),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    //width: MediaQuery.of(context).size.width * 0.9,
                    margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: Theme(
                      data: ThemeData(
                        inputDecorationTheme: InputDecorationTheme(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Mycolor.darkGreen,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Mycolor.darkGreen,
                            ),
                          ),
                        ),
                      ),
                      child: OTPTextField(
                        length: 6,

                        //width: MediaQuery.of(context).size.width,
                        fieldWidth: 35,
                        style: TextStyle(fontSize: 14),
                        textFieldAlignment: MainAxisAlignment.spaceEvenly,
                        fieldStyle: FieldStyle.box,
                        onCompleted: (pin) {
                          // showDialog(
                          //     context: context,
                          //     builder: (context) =>
                          //         tcardProgressDialog());
                          data['vCode'] = pin;
                          var response = PostData().callRegisterAPI(data);
                          response.then((value) {
                            print(value);
                            if (value.statusCode == 200) {
                              var dbHelper = DBHelper();
                              String message =
                                  "Thank you for signing up with tcard!\nYour tcard account have been created successfully!\nYou'll be the first to hear about all of our latest arrivals, new services, and exclusive promotions!\n\n\nBest Regards,\ntcard App Team";
                              Notifications notification = Notifications(
                                  null,
                                  "tcard account created successfully','Dear  ${data['name']}",
                                  'https://via.placeholder.com/50x50',
                                  message,
                                  null,
                                  null,
                                  null,
                                  'NORMAL',
                                  DateTime.now().toString(),
                                  'unseen');
                              dbHelper.save(notification);
                              PostData()
                                  .callLoginAPI(data['phone'], data['vCode'])
                                  .then((value) {
                                print(value.data);
                                FlutterHmsGmsAvailability.isGmsAvailable
                                    .then((google) {
                                  final p = Provider.of<NotificationState>(
                                      context,
                                      listen: false);

                                  if (google || Platform.isIOS) {
                                    final n = NotificationService();

                                    n.fcm();
                                    n.addListener(() {
                                      p.add();
                                    });
                                  } else {
                                    FlutterHmsGmsAvailability.isHmsAvailable
                                        .then((huawei) {
                                      if (huawei) {
                                        final huawei = HuaweiNotification();
                                        huawei.getToken();
                                        huawei.addListener(() {
                                          p.add();
                                        });
                                      }
                                    });
                                  }
                                });

                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    '/main', (Route<dynamic> route) => false);
                              });
                            } else if (value.statusCode == 400) {
                              showDialog(
                                  context: context,
                                  builder: (context) => DialogMessage(
                                      'Try Again',
                                      value.data['message'],
                                      false,
                                      '1'));
                            }
                          });
                          //rint('Completed: ' + pin);
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    child: Center(
                      child: Text("Didn't Receive?",
                          style: TextStyle(
                              color: Mycolor.darkGreen,
                              fontSize: 14,
                              fontWeight: FontWeight.w300)),
                    ),
                  ),
                )
              ],
            ),
            InkWell(
                onTap: () {
                  var response = PostData().callOtpAPI(data['phone']);
                  response.then((value) {
                    print('platform' + platform.toString());
                    if (value.statusCode == 200) {
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) => DialogMessage(
                              'Verification Code',
                              value.data['message'],
                              false,
                              '1'));
                    }
                  });
                },
                child: Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text("Resend",
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Mycolor.urlBlue,
                                fontSize: 14,
                                fontWeight: FontWeight.w300)),
                      ),
                    )
                  ],
                )),
            Button('Cancel', () {
              Navigator.of(context).pop();
            }),
          ],
        ),
      ),
    );
  }
}
