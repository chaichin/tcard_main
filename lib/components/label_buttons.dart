import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcard/utils/mycolor.dart';

class LabelButton extends StatelessWidget {
  final image;
  final label;
  final navigation;
  final type;
  final size;
  LabelButton(this.image, this.label, this.navigation, this.type, this.size);
  @override
  Widget build(BuildContext context) {
    return Container(
      //margin: EdgeInsets.all(20),
      child: GestureDetector(
        onTap: navigation,
        child: Column(children: [
          type == 'image'
              ? Image.asset(
                  "assets/images/$image.png",
                  width: size == 'small'
                      ? MediaQuery.of(context).size.width * 0.1
                      : 60,
                  height: size == 'small'
                      ? MediaQuery.of(context).size.height * 0.03
                      : 60,
                )
              : SvgPicture.asset(
                  "assets/images/$image.svg",
                  width: MediaQuery.of(context).size.width * 0.1,
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
          label == 'History'
              ? Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 2),
                      child: Image.asset(
                        'assets/images/teeth.png',
                        width: 15,
                        height: 15,
                      ),
                    ),
                    Text(
                      '$label',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: size == 'small'
                              ? Mycolor.redOrange
                              : Mycolor.darkGreen),
                    )
                  ],
                )
              : Text(
                  '$label',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: size == 'small'
                          ? Mycolor.redOrange
                          : Mycolor.darkGreen),
                )
        ]),
      ),
    );
  }
}
