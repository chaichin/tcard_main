import 'package:flutter/material.dart';
import 'package:tcard/utils/mycolor.dart';

class Input extends StatelessWidget {
  final String value;
  final int textArea;
  final textController;
  final keypad;
  final validate;
  final onSaved;
  final Key formkey;
  Input(
      {Key key,
      this.value,
      this.textArea,
      this.textController,
      this.keypad,
      this.onSaved,
      this.validate,
      this.formkey})
      : super(key: key);
//   @override
//   State<StatefulWidget> createState() {
//     return _InputState();
//   }
// }

// class _InputState extends State<Input> {

  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is disposed.
  //   textController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Container(
              margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
              child: TextFormField(
                //key: formkey,
                keyboardType: keypad == true ? TextInputType.number : null,

                controller: textController,
                maxLines: textArea,
                obscureText: value == 'Pin Number' ? true : false,
                validator: validate,
                onSaved: onSaved,
                style: TextStyle(fontWeight: FontWeight.w500),
                decoration: InputDecoration(
                  //errorText: 'required*',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.teal),
                  ),

                  alignLabelWithHint: true,
                  contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  isDense: true,

                  labelText: value,
                  labelStyle: TextStyle(
                    color: Mycolor.darkGreen,
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Mycolor.darkGreen),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Mycolor.darkGreen),
                  ),
                ),
              )),
        ),
      ],
    );
  }
}
