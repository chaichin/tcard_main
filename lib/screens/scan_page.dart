import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialog_voucher.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/scan_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/utils/mycolor.dart';

import 'package:qr_code_scanner/qr_code_scanner.dart';

class ScanPage extends StatefulWidget {
  final id;
  final mode;
  final points;
  final amountPaid;
  final quantity;
  ScanPage({this.id, this.mode, this.points, this.amountPaid, this.quantity});
  @override
  State<StatefulWidget> createState() {
    return ScanPageState(
        id: this.id,
        mode: this.mode,
        points: this.points,
        amountPaid: amountPaid,
        quantity: quantity);
  }
}

class ScanPageState extends State<ScanPage> {
  final id;
  final mode;
  final points;
  final amountPaid;
  final quantity;
  ScanPageState(
      {this.id, this.mode, this.points, this.amountPaid, this.quantity});
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController controller;
  @override
  void initState() {
    super.initState();

    // Enable hybrid composition.
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Scan',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: Column(children: [
          Expanded(
            child: QRView(
              overlay: QrScannerOverlayShape(
                borderColor: Mycolor.darkGreen,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300,
              ),
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          // FlatButton(
          //     onPressed: () {
          //       PostData().getRedeem(id.toString(), mode, "IDOSKI").then(
          //           (value) => showDialog(
          //               context: context,
          //               builder: (context) => DialogMessage(
          //                   value.data['status'], value.data['message'])));
          //     },
          //     child: Text('test'))
        ]));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;

    controller.scannedDataStream.listen((scanData) {
      controller.pauseCamera();
      print('scanData' + scanData);
      var validateqr = scanData.split('://');
      print("id+" + id);
      print("mode " + mode.toString());

      // print('amount' + amountPaid);
      // print('points' + points);
      //print("points " + points);
      print(validateqr[0]);
      print(validateqr[1]);
      if (validateqr[0] == "tcardBC") {
        if (mode == '3') {
          PostData().getRedeemVoucher(id, validateqr[1]).then((value) {
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => DialogVoucher(
                    value.data['status'], value.data['message'], true, '1'));
          });
        } else {
          PostData()
              .getRedeem(id, mode, validateqr[1], points, amountPaid, quantity)
              .then((value) {
            print(value);
            print(id);
            print(value);
            print(amountPaid);
            print(quantity);
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => DialogMessage(
                    value.data['status'], value.data['message'], true, mode));
          });
        }
      } else {
        // showDialog(
        //     context: context,
        //     builder: (context) =>
        //         DialogMessage("Try Again", "Invalid QR Code", false, '1'));
        showDialog(
            context: context,
            builder: (context) => ScanDialogMessage(
                  title: "Try Again",
                  message: "Invalid QR Code",
                  success: false,
                  action: () {
                    Navigator.pop(context);
                    controller.resumeCamera();
                  },
                ));
      }
      // setState(() {
      //   qrText = scanData;
      //   //Navigator.pushNamed(context, '/voucher');
      // });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
