import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/confirmation_dialogs.dart';
import 'package:tcard/components/product_cards.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/voucher.dart';
import 'package:tcard/utils/mycolor.dart';

class VoucherDescriptionPage extends StatefulWidget {
  final voucherid;

  VoucherDescriptionPage({Key key, this.voucherid}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return VoucherDescriptionPageState(voucherid: voucherid);
  }
}

class VoucherDescriptionPageState extends State<VoucherDescriptionPage> {
  Future<Voucher> voucher;
  Voucher v;
  final voucherid;
  int dayLeft;
  String title;
  String image;
  String description;
  String termsCondition;
  VoucherDescriptionPageState({this.voucherid});
  // @override
  // void initState() {
  //   super.initState();

  //   PostData().getVoucherDetails(voucherid).then((value) {
  //     setState(() {

  //     });
  //   });
  // }
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    voucher = PostData().getVoucherDetails(voucherid);
  }

  @override
  Widget build(BuildContext context) {
    voucher = PostData().getVoucherDetails(voucherid);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // print(latitude);
    // print(longitude);
    return FutureBuilder(
        future: voucher,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            v = snapshot.data;
            dayLeft =
                snapshot.data.expiryDate.difference(DateTime.now()).inDays;
            image = snapshot.data.image;
            title = snapshot.data.title;
            description = snapshot.data.description;
            termsCondition = snapshot.data.termsCondition;
            return Scaffold(
                body: Container(
                    color: Mycolor.white,
                    child: Theme(
                        data: Theme.of(context)
                            .copyWith(accentColor: Mycolor.darkGreen),
                        child: RefreshIndicator(
                            key: _refreshIndicatorKey,
                            onRefresh: _handleRefresh,
                            child: ListView(
                              children: [
                                image != null
                                    ? Stack(
                                        children: [
                                          AspectRatio(
                                              aspectRatio: 16 / 9,
                                              child: Container(
                                                child: CachedNetworkImage(
                                                  imageUrl: image,
                                                  fit: BoxFit.fill,
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  placeholder:
                                                      (BuildContext context,
                                                              String url) =>
                                                          Container(),
                                                  // If error happens load image by using NetworkImage
                                                  errorWidget:
                                                      (context, url, error) {
                                                    return Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    100)),
                                                        image: DecorationImage(
                                                          image: NetworkImage(
                                                              image),
                                                          alignment:
                                                              Alignment.center,
                                                          fit: BoxFit.cover,
                                                          scale: 1,
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ),
                                                //width: MediaQuery.of(context).size.width,
                                                height: 200,
                                              )),
                                          ReturnButton(),
                                        ],
                                      )
                                    : Container(),
                                Container(
                                    alignment: Alignment.centerLeft,
                                    //padding: EdgeInsets.symmetric(horizontal: 10),
                                    child: Column(children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 20),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          '$title',
                                          style: TextStyle(
                                              fontSize: 24,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      Productcard(
                                        description: description,
                                        termsCondition: termsCondition,
                                      ),
                                    ])),
                              ],
                            )))),
                bottomNavigationBar: Container(
                    color: Mycolor.normalWhite,
                    padding: EdgeInsets.only(bottom: 10),
                    child: Container(
                        padding:
                            EdgeInsets.only(left: 15, right: 15, bottom: 10),
                        //margin: EdgeInsets.only(bottom: 10),
                        height: 60,
                        color: Mycolor.normalWhite,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset(
                                  'assets/images/time.png',
                                  width: 30,
                                  height: 30,
                                ),
                                Text(
                                  '  $dayLeft days left!',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Mycolor.darkgrey,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),

                            // Container(
                            //   alignment: Alignment.center,
                            //   height: 50,
                            //   color: Colors.black38,
                            //   width: MediaQuery.of(context).size.width * 0.5,
                            //   child: Text(
                            //     '$dayLeft DAYS LEFT!',
                            //     textAlign: TextAlign.center,
                            //     style: TextStyle(
                            //         fontSize: 15, fontWeight: FontWeight.bold),
                            //   ),
                            // ),

                            Button('Scan', () {
                              if (PostData.logintoken != null) {
                                showDialog(
                                    context: context,
                                    builder: (context) => DialogConfirm(
                                        title: 'Dear Member',
                                        message:
                                            "Click proceed to Scan QR at your preferred outlet",
                                        proceedtext: 'Proceed',
                                        id: v.vTransId,
                                        mode: "3"));
                              }
                            }),
                            // InkWell(
                            //   onTap: () {
                            //     if (PostData.logintoken != null) {
                            //       showDialog(
                            //           context: context,
                            //           builder: (context) => DialogConfirm(
                            //               title: 'Dear Member',
                            //               message:
                            //                   "Click Proceed\nvoucher will be redeemed",
                            //               proceedtext: 'Proceed',
                            //               id: v.vTransId,
                            //               mode: "3"));
                            //     }
                            //     // else {
                            //     //   showDialog(
                            //     //       context: context,
                            //     //       builder: (context) => DialogGuest());
                            //     // }
                            //   },
                            //   child: Container(
                            //     alignment: Alignment.center,
                            //     height: 30,
                            //     //color: Mycolor.darkGreen,
                            //     decoration: BoxDecoration(
                            //         color: Mycolor.darkGreen,
                            //         borderRadius: BorderRadius.circular(50)),
                            //     width: MediaQuery.of(context).size.width * 0.5,
                            //     child: Text(
                            //       'REDEEM',
                            //       textAlign: TextAlign.center,
                            //       style: TextStyle(
                            //           color: Mycolor.white,
                            //           fontSize: 14,
                            //           fontWeight: FontWeight.bold),
                            //     ),
                            //   ),
                            // )
                          ],
                        ))));
          } else {
            return LoadingOverlay(
                color: Colors.grey,
                progressIndicator: tcardProgressDialog(),
                child: Center(),
                isLoading: true);
          }
        });
  }
}
