import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/main.dart';
import 'package:tcard/models/Notification.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:tcard/services/huawei_notification_service.dart';
import 'package:tcard/services/notificationService.dart';

import 'package:tcard/utils/mycolor.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';
import 'main_page.dart';

class LaunchScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LaunchScreenState();
  }
}

// void openApp() async {
//   RemoteMessage initialMessage =
//       await FirebaseMessaging.instance.getInitialMessage();

//   // If the message also contains a data property with a "type" of "chat",
//   // navigate to a chat screen
//   if (initialMessage?.data['type'] == 'PRODUCT') {
//     NavKey.navKey.currentState.push(MaterialPageRoute(
//         builder: (context) => ShoppingPage(
//               productid: initialMessage.data['productId'],
//             )));
//   }
//   FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
//     if (message.data['type'] == 'PRODUCT') {
//       NavKey.navKey.currentState.push(MaterialPageRoute(
//           builder: (context) => ShoppingPage(
//                 productid: initialMessage.data['productId'],
//               )));
//     }
//   });
// }

class _LaunchScreenState extends State<LaunchScreen> {
  FlutterLocalNotificationsPlugin fltrNotification;
  var navigate;
  @override
  initState() {
    super.initState();
    //  openApp();
    initUniLinks();
  }

  // void getInitialNotification() async {
  //   var initialNotification = await Push.getInitialNotification();
  //   print("getInitialNotification: " + initialNotification.toString());
  //   Notifications notification = Notifications(
  //       null,
  //       initialNotification['extras']['title'],
  //       initialNotification['extras']['image'],
  //       initialNotification['extras']['message'],
  //       initialNotification['extras']['productId'],
  //       initialNotification['extras']['url'],
  //       initialNotification['extras']['vTransId'],
  //       initialNotification['extras']['type'],
  //       DateTime.now().toString(),
  //       'unseen');
  //   DBHelper().save(notification);
  //   // If the message also contains a data property with a "type" of "chat",
  //   // navigate to a chat screen
  //   if (initialNotification['extras']['type'] == 'PRODUCT') {
  //     Timer(Duration(seconds: 3), () {
  //       Navigator.push(
  //           context,
  //           MaterialPageRoute(
  //               builder: (context) => ShoppingPage(
  //                     productid: initialNotification['extras']['productId'],
  //                   )));
  //     });
  //   } else if (initialNotification['extras']['type'] == 'URL') {
  //     if (await canLaunch(
  //       initialNotification['extras']['url'],
  //     )) {
  //       await launch(initialNotification['extras']['url']);
  //     } else {
  //       throw 'Could not launch ${initialNotification['extras']['url']}';
  //     }
  //   } else if (initialNotification['extras']['type'] == 'VOUCHER') {
  //     Timer(Duration(seconds: 3), () {
  //       Navigator.push(
  //           context,
  //           MaterialPageRoute(
  //               builder: (context) => VoucherDescriptionPage(
  //                     voucherid: initialNotification['extras']['vTransId'],
  //                   )));
  //     });
  //   }
  //   if (!mounted) return;

  //   Push.onNotificationOpenedApp.listen(_onNotificationOpenedApp);
  // }

  // void _onNotificationOpenedApp(var remoteMessage) async {
  //   print("onNotificationOpenedApp: " + remoteMessage.toString());
  //   if (remoteMessage['extras']['type'] == 'PRODUCT') {
  //     NavKey.navKey.currentState.push(MaterialPageRoute(
  //         builder: (context) =>
  //             ShoppingPage(productid: remoteMessage['extras']['productId'])));
  //   } else if (remoteMessage['extras']['type'] == 'URL') {
  //     if (await canLaunch(
  //       remoteMessage['extras']['url'],
  //     )) {
  //       await launch(remoteMessage['extras']['url']);
  //     } else {
  //       throw 'Could not launch ${remoteMessage['extras']['url']}';
  //     }
  //   } else if (remoteMessage['extras']['type'] == 'VOUCHER') {
  //     NavKey.navKey.currentState.push(MaterialPageRoute(
  //         builder: (context) => VoucherDescriptionPage(
  //               voucherid: remoteMessage['extras']['vTransId'],
  //             )));
  //   }
  // }

  openApp() async {
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    // Notifications notification = Notifications(
    //     null,
    //     initialMessage.data['title'],
    //     initialMessage.data['image'],
    //     initialMessage.data['message'],
    //     initialMessage.data['productId'],
    //     initialMessage.data['url'],
    //     initialMessage.data['vTransId'],
    //     initialMessage.data['type'],
    //     DateTime.now().toString(),
    //     'unseen');

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage?.data['type'] == 'PRODUCT') {
      Timer(Duration(seconds: 3), () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ShoppingPage(
                      productid: initialMessage.data['productId'],
                    )));
      });
    } else if (initialMessage.data['type'] == 'URL') {
      if (await canLaunch(
        initialMessage.data['url'],
      )) {
        await launch(initialMessage.data['url']);
      } else {
        throw 'Could not launch ${initialMessage.data['url']}';
      }
    } else if (initialMessage.data['type'] == 'VOUCHER') {
      Timer(Duration(seconds: 3), () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VoucherDescriptionPage(
                      voucherid: initialMessage.data['vTransId'],
                    )));
      });
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      if (message.data['type'] == 'PRODUCT') {
        Timer(Duration(seconds: 3), () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      ShoppingPage(productid: message.data['productId'])));
        });
      } else if (message.data['type'] == 'URL') {
        if (await canLaunch(
          initialMessage.data['url'],
        )) {
          await launch(initialMessage.data['url']);
        } else {
          throw 'Could not launch ${initialMessage.data['url']}';
        }
      } else if (message.data['type'] == 'VOUCHER') {
        Timer(Duration(seconds: 3), () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VoucherDescriptionPage(
                        voucherid: message.data['vTransId'],
                      )));
        });
      }
    });
    // DBHelper().save(notification);
  }

  Future notificationSelected(String payload) async {
    print('paylo' + payload);
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    NavKey.navKey.currentState.push(MaterialPageRoute(
        builder: (context) => ShoppingPage(
              productid: payload,
            )));

    // await Navigator.push(
    //   context,
    //   MaterialPageRoute<void>(
    //       builder: (context) => ShoppingPage(productid: payload)),
    // );
  }

  bool _visible = true;
  String getid;
  Future<Null> initUniLinks() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      String initialLink = await getInitialLink();
      print(initialLink);
      getid = initialLink == null ? null : initialLink.split('=')[1];
      initialLink == null ? autoLogin(null) : autoLogin(getid);
    } on PlatformException {}
  }

  autoLogin(String link) async {
    FlutterHmsGmsAvailability.isGmsAvailable.then((google) {
      print(google);
      // if (google) {
      PostData().tokenValidation().then((value) {
        print('tokenva' + value.toString());
        if (value.statusCode == 200) {
          if (google || Platform.isIOS) {
            final p = Provider.of<NotificationState>(context, listen: false);

            final n = NotificationService();

            n.fcm();
            n.addListener(() {
              p.add();
            });
          } else {
            FlutterHmsGmsAvailability.isHmsAvailable.then((huawei) {
              if (huawei) {
                final p =
                    Provider.of<NotificationState>(context, listen: false);
                final n = HuaweiNotification();
                n.getToken();

                n.addListener(() {
                  p.add();
                });
              }
            });
          }
          if (link != null) {
            setState(() {
              _visible = !_visible;
            });
            Timer(Duration(seconds: 3), () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MainPage(deeplink: link)));
            });
          } else {
            setState(() {
              _visible = !_visible;
            });
            Timer(Duration(seconds: 3), () {
              Navigator.pushReplacementNamed(context, '/main');
            });
            openApp();

            HuaweiNotification().getInitialNotification();
          }
          // link != null
          //     ? Navigator.pushReplacement(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => MainPage(deeplink: link)))
          //     : Navigator.pushReplacementNamed(context, '/main');

        } else {
          //Navigator.pushReplacementNamed(context, '/home');
          if (link != null) {
            setState(() {
              _visible = !_visible;
            });
            Timer(Duration(seconds: 3), () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MainPage(deeplink: link)));
            });
          } else {
            print('here');
            setState(() {
              _visible = !_visible;
            });
            Timer(Duration(seconds: 3), () {
              Navigator.pushReplacementNamed(context, '/home');
            });
            openApp();
            HuaweiNotification().getInitialNotification();
          }
        }
      });

      // } else {
      //   HuaweiNotification().getToken();
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        backgroundColor: Mycolor.darkGreen,
        body: AnimatedOpacity(
            // If the widget is visible, animate to 0.0 (invisible).
            // If the widget is hidden, animate to 1.0 (fully visible).
            opacity: _visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 3000),
            // The green box must be a child of the AnimatedOpacity widget.
            child: Center(
                child:
                    //  AnimatedSplash(
                    //   imagePath: 'assets/images/splash_logo.png',
                    //   home: navigate,
                    //   duration: 2500,
                    //   type: AnimatedSplashType.StaticDuration,
                    // ),
                    Image.asset(
              'assets/images/splash_logo.png',
              width: 200,
              height: 200,
            ))
            //         Image.asset(
            //   'assets/images/splash_logo.png',
            //   width: 200,
            //   height: 200,
            // )

            ));
  }
}
