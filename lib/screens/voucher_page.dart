import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/myVoucher.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/useVoucher.dart';
import 'package:tcard/utils/mycolor.dart';

class VoucherPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return VoucherPageState();
  }
}

class VoucherPageState extends State<VoucherPage> {
  Future<List<UseVoucher>> voucher;
  @override
  void initState() {
    super.initState();

    //print(voucher.length);
  }

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    voucher = PostData().getUseVoucher();
  }

  @override
  Widget build(BuildContext context) {
    voucher = PostData().getUseVoucher();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'E-News',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: FutureBuilder(
            future: voucher,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<UseVoucher> voucher = snapshot.data;
                return Column(children: [
                  Container(
                    // alignment: Alignment.center,
                    color: Mycolor.grey,
                    padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    // decoration:
                    //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),

                    child: Text(
                      'E-News that are available',
                      // 'All Locations (${branches.length})',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Mycolor.darkgrey,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Expanded(
                      child: Theme(
                          data: Theme.of(context)
                              .copyWith(accentColor: Mycolor.darkGreen),
                          child: RefreshIndicator(
                              key: _refreshIndicatorKey,
                              onRefresh: _handleRefresh,
                              child: ListView.builder(
                                  itemCount: voucher.length,
                                  itemBuilder: (context, index) {
                                    if (voucher[index].status == 'VALID') {
                                      return MyVoucher(
                                        redeemed: false,
                                        title: voucher[index].title,
                                        expiredate: voucher[index].expiryDate,
                                        voucherid: voucher[index].vTransId,
                                      );
                                    } else {
                                      return Container();
                                    }
                                  }))))
                ]);
              } else if (snapshot.hasError) {
                if (PostData.update) {
                  return DialogUpdate();
                } else if (PostData.statusCode == 403) {
                  return DialogRelogin();
                } else {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    title: Column(children: [
                      Text(
                        'Connection Error',
                        style: TextStyle(
                            color: Mycolor.darkGreen,
                            fontSize: 24,
                            fontWeight: FontWeight.w900),
                      )
                    ]),
                  );
                }
              } else {
                return LoadingOverlay(
                    color: Colors.grey,
                    progressIndicator: tcardProgressDialog(),
                    child: Center(),
                    isLoading: true);
              }
            })

        // ListView(
        //   children: [
        //     Container(
        //       padding: EdgeInsets.all(10),
        //       width: MediaQuery.of(context).size.width,
        //       // decoration:
        //       //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        //       child: Text(
        //         'Voucher',
        //         textAlign: TextAlign.center,
        //         style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        //       ),
        //     ),

        //     // Voucher(redeemed: false, myvoucher: true),
        //     // Voucher(redeemed: true, myvoucher: true),
        //     // Voucher(redeemed: false, myvoucher: true),
        //   ],
        // )
        );
  }
}
