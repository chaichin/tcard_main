import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/config/postData.dart';

import 'package:tcard/utils/mycolor.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:tcard/components/Buttons.dart';

//import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  final bool isLoading = false;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String getid;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      body: SafeArea(
        child: Container(
          //  alignment: Alignment.center,
          decoration: BoxDecoration(color: Mycolor.white),
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          //color: Mycolor.white,
          child: Stack(
            // alignment: Alignment.topCenter,
            children: [
              Image.asset(
                'assets/images/background.png',
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                margin: EdgeInsets.only(top: 120, bottom: 50),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Mycolor.normalWhite,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 150,
                        height: 150,
                        child: Image.asset("assets/images/tcard_logo.png"),
                      ),
                      Container(
                        height: 50,
                        alignment: Alignment.center,
                        child: Text(
                          'Welcome user',
                          style: TextStyle(
                              color: Mycolor.darkGreen,
                              fontSize: 24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 30, vertical: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Text(
                                        'Already a user?',
                                        textAlign: TextAlign.center,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        child: Button('View as Guest', () {
                                          // PostData.logintoken =
                                          //     'ZVtFmZDq9XFgWz892TxGSGMgWzKufBaD';
                                          PostData.logintoken = null;
                                          PostData.panel = false;
                                          Navigator.pushNamed(context, '/main');
                                        }),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 50),
                                          child: Button('Login', () async {
                                            //Show dialog
                                            // showDialog(
                                            //     context: context,
                                            //     builder: (context) => tcardProgressDialog());

                                            // Proceed call API
                                            // var response = callAPI();
                                            // response.then(
                                            //     (value) => Navigator.of(context).pop('value'));

                                            Navigator.pushNamed(
                                                context, '/login');
                                          })),
                                      Divider(
                                        thickness: 2,

                                        //indent: 0,
                                        //endIndent: 10,
                                        color: Mycolor.grey,
                                      ),
                                      Text(
                                        'New to tcard?',
                                        textAlign: TextAlign.center,
                                      ),
                                      Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 50),
                                          child: Button('Sign up', () {
                                            Navigator.pushNamed(
                                                context, '/signup');
                                          })),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.pushNamed(context, '/t&c');
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 10),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                RichText(
                                                    text: TextSpan(
                                                        text: 'Privacy policy',
                                                        style: TextStyle(
                                                            decoration:
                                                                TextDecoration
                                                                    .underline,
                                                            color:
                                                                Mycolor.urlBlue,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w300)))
                                              ]),
                                        ),
                                      ),
                                    ],
                                  )),
                            ],
                          ))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

// Future<void> callAPI() async {
//   String deviceId = await PlatformDeviceId.getDeviceId;
//   var bytes = utf8.encode("1476681660000VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
//   var digest = sha1.convert(bytes);
//   print(deviceId);
//   print(digest);
//   try {
//     // Declare the response item
//     Response response;

//     // New instance of dio
//     var dio = DioHelper.getDioInstance();

//     // Create the form data
//     FormData formData = new FormData.fromMap({
//       // "name": "wendux",
//       // "age": 25,
//       "isPanel": "N",
//       "panelPasscode": "",
//       "name": "Abu Bin Ahmad",
//       "phoneNumber": "0162117959",
//       "vCode": "116880",
//       "dateOfBirth": "1992-11-26",
//       "gender": "M",
//       "deviceId": "$deviceId",
//       "deviceType": "",
//       "millis": "1476681660000",
//       "secret": "$digest",
//     });

//     // Call the API
//     response = await dio.post("/register", data: formData);

//     // Print the response
//     var finalValue = response.data;
//     print(finalValue['value'].toString());
//   } catch (e) {
//     print(e);
//   }
// }
