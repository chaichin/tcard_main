import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/newSlider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/merchants.dart';
import 'package:tcard/utils/mycolor.dart';

class MerchantPage extends StatefulWidget {
  final merchantId;
  final title;
  final logo;
  final merchantname;

  MerchantPage({this.title, this.logo, this.merchantId, this.merchantname});
  @override
  State<StatefulWidget> createState() {
    return MerchantPageState(
        title: title,
        logo: logo,
        merchantId: merchantId,
        merchantName: merchantname);
  }
}

class MerchantPageState extends State<MerchantPage> {
  // Future<List<Merchants>> _merchant;
  final merchantId;
  final title;
  final logo;
  final merchantName;
  MerchantPageState(
      {this.title, this.logo, this.merchantId, this.merchantName});

  // @override
  // void initState() {
  //   super.initState();
  //   PostData().getMerchant(merchantId).then((value) {
  //     setState(() {
  //       merchant = value;
  //     });
  //   });
  // }
  final PagingController<int, Merchants> _pagingController =
      PagingController(firstPageKey: 1);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      // PostData().getProductApi(pageKey).then((value) {
      //   newItems = value.merchant;
      // });
      PostData().getMerchant(merchantId, pageKey).then((value) {
        final isLastPage = value.length == 0;

        if (isLastPage) {
          _pagingController.appendLastPage(value);
        } else {
          final nextPageKey = pageKey + 1;
          _pagingController.appendPage(value, nextPageKey);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  //     GlobalKey<RefreshIndicatorState>();
  // Future<Null> _handleRefresh() async {
  //   setState(() {
  //     _merchant = PostData().getMerchant(merchantId);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    //_merchant = PostData().getMerchant(merchantId);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // return FutureBuilder(
    //     future: _merchant,
    //     builder: (context, snapshot) {
    //       if (snapshot.hasData) {
    //         List<Merchants> merchant = snapshot.data;
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(bottom: 5),
                child: Text(
                  'Organiser',
                  //merchantName,
                  style: TextStyle(
                      fontSize: 30,
                      color: Mycolor.darkerGreen,
                      fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
        body: Theme(
            data: Theme.of(context).copyWith(accentColor: Mycolor.darkGreen),
            child: RefreshIndicator(
                onRefresh: () => Future.sync(
                      // 2
                      () => _pagingController.refresh(),
                    ),
                child: Stack(children: [
                  PagedListView<int, Merchants>(
                      padding: EdgeInsets.only(right: 20, left: 20),
                      physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      pagingController: _pagingController,
                      builderDelegate: PagedChildBuilderDelegate<Merchants>(
                          itemBuilder: (context, item, index) {
                        List<String> merchantImages =
                            item.images.split(',').map((e) => e).toList();
                        return Container(
                            margin: EdgeInsets.only(top: index == 0 ? 80 : 0),
                            child: NewSliderItem(
                              flex: true,
                              value: 'merchant',
                              source: merchantImages[0],
                              title: item.title,
                              logo: item.merchantLogo,
                              productid: item.productId,
                              views: item.numberOfViews,
                            ));
                      })),
                  // ListView.builder(
                  //     padding: EdgeInsets.only(right: 20),
                  //     physics: const BouncingScrollPhysics(
                  //         parent: AlwaysScrollableScrollPhysics()),
                  //     itemCount: merchant.length,
                  //     itemBuilder: (context, index) {
                  //       List<String> merchantImages = merchant[index]
                  //           .images
                  //           .split(',')
                  //           .map((e) => e)
                  //           .toList();
                  //       return Container(
                  //           margin:
                  //               EdgeInsets.only(top: index == 0 ? 80 : 10),
                  //           child: NewSliderItem(
                  //             flex: true,
                  //             value: 'merchant',
                  //             source: merchantImages[0],
                  //             title: merchant[index].title,
                  //             logo: merchant[index].merchantLogo,
                  //             productid: merchant[index].productId,
                  //             views: merchant[index].numberOfViews,
                  //           ));
                  //     }),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        //margin: EdgeInsets.only(top: 40),
                        color: Mycolor.white,
                        //alignment: Alignment.center,
                        height: 70,
                        width: MediaQuery.of(context).size.height,
                        child: Row(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image.network(
                              logo,
                            ),
                          ),
                          // Image.network(logo),
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                title,
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                merchantName,
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )
                            ],
                          )
                        ])),
                  ),
                ]))));
    //   } else {
    //     return LoadingOverlay(
    //         color: Colors.grey,
    //         progressIndicator: tcardProgressDialog(),
    //         child: Center(),
    //         isLoading: true);
    //   }
  }
}
