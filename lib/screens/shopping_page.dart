import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/confirmation_dialogs.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/guess_dialogs.dart';
import 'package:tcard/components/product_cards.dart';
import 'package:tcard/components/product_carousel.dart';
import 'package:tcard/components/shopping_buttons.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/product.dart';
import 'package:tcard/screens/appointment_page.dart';
import 'package:tcard/screens/branches_page.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';

class ShoppingPage extends StatefulWidget {
  final productid;
  final views;
  ShoppingPage({Key key, this.productid, this.views}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return ShoppingPageState(productid: productid, views: views);
  }
}

class ShoppingPageState extends State<ShoppingPage> {
  Future<Product> _product;
  Product product;
  final productid;
  final views;
  int dayLeft;
  String sellingPriceWithPoint;
  String productType;
  String sellingPrice;
  String displayPrice;
  String discountType;
  String discountText;
  String title;
  String description;
  String termsCondition;
  String points;
  String merchantName;
  String showDisplay;
  bool showappointment;
  bool flag = true;
  bool hasBeenRedeemed;
  String numberofViews;
  ShoppingPageState({this.productid, this.views});
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    setState(() {
      _product = PostData().getProduct(productid);
    });
  }

  @override
  void initState() {
    PostData().getViews(productid).then((value) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    _product = PostData().getProduct(productid);

    return FutureBuilder(
        future: _product,
        builder: (context, snapshot) {
          print('error' + snapshot.hasError.toString());
          if (snapshot.hasData) {
            Product product = snapshot.data;
            List<String> imgList =
                product.images.split(',').map((e) => e).toList();
            print('product' + product.toString());
            String chatLive = product.chatLink;
            title = product.title;
            sellingPrice = product.sellingPrice;
            hasBeenRedeemed = product.hasBeenRedeemed;
            productType = product.type;
            sellingPriceWithPoint = product.sellingPriceWithPoint;
            displayPrice = product.displayPrice;
            discountType = product.discountType;
            discountText = product.discountText;
            showappointment = product.shouldShowAppointment;
            description = product.description;
            termsCondition = product.termsCondition;
            dayLeft = product.expiryDate.difference(DateTime.now()).inDays;
            points = product.pointsNeeded;
            String logo = product.merchantLogo;
            print(sellingPrice);
            numberofViews = product.numberofViews;
            merchantName = product.merchantName;

            if (displayPrice != '0') {
              if (discountType == 'NONE') {
                showDisplay = "RM $displayPrice";
              } else if (discountType == 'FLAT') {
                showDisplay = discountText;
              } else if (discountType == 'PERCENT') {
                showDisplay = discountText;
              } else if (discountType == 'NOSHOW') {
                showDisplay = '';
              }
            } else if (displayPrice == '0') {
              if (discountType == 'NONE') {
                showDisplay = 'FREE';
              } else if (discountType == 'NOSHOW') {
                showDisplay = '';
              } else {
                showDisplay = 'FREE';
              }
            }
            return Scaffold(
                // appBar: AppBar(
                //   centerTitle: true,
                //   elevation: 0,
                //   backgroundColor: Mycolor.white,
                //   leading: ReturnButton(),
                //   title: Image.asset("assets/images/logo.png",
                //       width: 80, height: 60),
                // ),
                body: Container(
                    color: Mycolor.white,
                    child: Theme(
                        data: Theme.of(context)
                            .copyWith(accentColor: Mycolor.darkGreen),
                        child: RefreshIndicator(
                            key: _refreshIndicatorKey,
                            onRefresh: _handleRefresh,
                            child: ListView(
                              children: [
                                Stack(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(boxShadow: [
                                        BoxShadow(
                                            offset: Offset(0, 0),
                                            blurRadius: 5,
                                            color:
                                                Colors.black.withOpacity(0.30))
                                      ]),
                                      child: ProductCarousel(
                                        product: imgList,
                                      ),
                                    ),
                                    ReturnButton(),
                                  ],
                                ),
                                Container(
                                    alignment: Alignment.centerLeft,
                                    //padding: EdgeInsets.symmetric(horizontal: 10),
                                    child: Column(children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 15),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        //alignment: Alignment.centerRight,
                                        child: Row(children: [
                                          productType == '1'
                                              ? Text(
                                                  "$showDisplay",
                                                  style: TextStyle(
                                                      fontSize: 24,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          showDisplay == 'FREE'
                                                              ? Colors.red
                                                              : Mycolor
                                                                  .darkerGreen),
                                                  textAlign: TextAlign.start,
                                                )
                                              : Container(),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          discountText != null &&
                                                  discountType == 'NONE' &&
                                                  productType == '1'
                                              ? Text(
                                                  discountText,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          Mycolor.darkerGreen),
                                                  textAlign: TextAlign.start,
                                                )
                                              : Container(),
                                        ]),
                                      ),

                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 20),
                                        margin:
                                            EdgeInsets.only(top: 0, bottom: 10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          '$title',
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Mycolor.darkerGreen,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      productType == '2'
                                          ? Container(
                                              padding: EdgeInsets.only(
                                                  right: 20, bottom: 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  sellingPriceWithPoint !=
                                                              '0.00' ||
                                                          points != '0'
                                                      ? Column(
                                                          children: [
                                                            Text(
                                                              'RM ${double.parse(sellingPriceWithPoint).toStringAsFixed(2)}',
                                                              style: TextStyle(
                                                                  fontSize: 18,
                                                                  color: Mycolor
                                                                      .darkGreen,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            Row(children: [
                                                              points != '0'
                                                                  ? Text(
                                                                      '+ ${double.parse(points).toInt()} points',
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Mycolor.darkerGreen),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                    )
                                                                  : Container(),
                                                              points != '0'
                                                                  ? Image.asset(
                                                                      'assets/images/teeth.png',
                                                                      width: 13,
                                                                      height:
                                                                          13)
                                                                  : Container(),
                                                            ]),
                                                          ],
                                                        )
                                                      : Container(),
                                                  sellingPriceWithPoint !=
                                                              '0.00' ||
                                                          points != '0'
                                                      ? Padding(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      20),
                                                          child: Text('or',
                                                              style: TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold)),
                                                        )
                                                      : Container(),
                                                  Column(
                                                    children: [
                                                      Text(
                                                          'RM ${double.parse(displayPrice).toStringAsFixed(2)}',
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              color: Mycolor
                                                                  .darkGreen,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      showDisplay != ''
                                                          ? Text(
                                                              'RM $showDisplay',
                                                              style: TextStyle(
                                                                  color: Mycolor
                                                                      .darkerGreen,
                                                                  decoration: discountType ==
                                                                          "FLAT"
                                                                      ? TextDecoration
                                                                          .lineThrough
                                                                      : TextDecoration
                                                                          .none,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            )
                                                          : Container()
                                                    ],
                                                  )
                                                ],
                                              ),
                                            )
                                          : Container(),

                                      Productcard(
                                        description: description,
                                        termsCondition: termsCondition,
                                      ),
                                      // Card(
                                      //   shape: RoundedRectangleBorder(
                                      //       borderRadius:
                                      //           BorderRadius.circular(10.0)),
                                      //   margin: EdgeInsets.symmetric(
                                      //       horizontal: 20, vertical: 20),
                                      //   child:)
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Stack(
                                          children: [
                                            Card(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0)),
                                                margin: EdgeInsets.only(
                                                    left: 20,
                                                    right: 20,
                                                    top: 10),
                                                child: Container(
                                                  height: 50,
                                                  decoration: BoxDecoration(),
                                                )),
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 10),
                                              padding: EdgeInsets.only(
                                                  left: 20, top: 5),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    width: 60,
                                                    height: 60,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      image: DecorationImage(
                                                          image: NetworkImage(
                                                              logo)),
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        left: 10),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    decoration: BoxDecoration(),
                                                    width: 230,
                                                    height: 40,
                                                    child: Text(
                                                      merchantName,
                                                      textAlign: TextAlign.left,
                                                      maxLines: 2,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          height: 1,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18,
                                                          color: Mycolor
                                                              .darkergrey),
                                                    ),
                                                  ),
                                                  // Padding(
                                                  //   padding: EdgeInsets.only(
                                                  //       right: 10, left: 40),
                                                  //   child: InkWell(
                                                  //     onTap: () async {
                                                  //       Position geolocation;
                                                  //       try {
                                                  //         geolocation = await Geolocator
                                                  //             .getCurrentPosition(
                                                  //                 desiredAccuracy:
                                                  //                     LocationAccuracy
                                                  //                         .high);
                                                  //       } catch (e) {
                                                  //         geolocation = null;
                                                  //       }

                                                  //       if (PostData
                                                  //               .logintoken !=
                                                  //           null) {
                                                  //         if (geolocation !=
                                                  //             null) {
                                                  //           Navigator.push(
                                                  //               context,
                                                  //               MaterialPageRoute(
                                                  //                   builder: (context) => BranchesPage(
                                                  //                       productid:
                                                  //                           productid,
                                                  //                       location:
                                                  //                           geolocation)));
                                                  //         } else {
                                                  //           showDialog(
                                                  //               context:
                                                  //                   context,
                                                  //               builder: (context) =>
                                                  //                   DialogMessage(
                                                  //                       'Try Again',
                                                  //                       'Please enable location service',
                                                  //                       false));
                                                  //         }
                                                  //       } else {
                                                  //         showDialog(
                                                  //             context: context,
                                                  //             builder: (context) =>
                                                  //                 DialogGuest());
                                                  //       }
                                                  //     },
                                                  //     child: Image.asset(
                                                  //       'assets/images/location.png',
                                                  //       width: 20,
                                                  //       height: 20,
                                                  //     ),
                                                  //   ),
                                                  // )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      productType == '1'
                                          ? Container(
                                              height: 60,
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 30),
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 10),
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    ShoppingButton(
                                                        'share', 'Share', () {
                                                      String text =
                                                          "Check this out, only available in T-Card!\n\n$title\n\nTo find out more,click the link below:\n\nhttp://www.tcardmy.com/item/?id=" +
                                                              productid;
                                                      Share.share(
                                                        text,
                                                      );
                                                    }, 'big'),
                                                    VerticalDivider(
                                                      thickness: 2,
                                                      width: 1,
                                                      //indent: 0,
                                                      //endIndent: 10,
                                                      color: Mycolor.grey,
                                                    ),

                                                    ShoppingButton(
                                                        chatLive != null
                                                            ? 'chat'
                                                            : 'chat_disabled',
                                                        'Chat Now', () async {
                                                      String url = chatLive;
                                                      if (await canLaunch(
                                                          url)) {
                                                        await launch(url);
                                                      } else {
                                                        throw 'Could not launch $url';
                                                      }
                                                    },
                                                        chatLive != null
                                                            ? 'big'
                                                            : 'small'),
                                                    VerticalDivider(
                                                      thickness: 2,
                                                      width: 1,
                                                      //indent: 0,
                                                      //endIndent: 10,
                                                      color: Mycolor.grey,
                                                    ),
                                                    ShoppingButton(
                                                        'location', 'Outlets',
                                                        () async {
                                                      Position geolocation;
                                                      try {
                                                        geolocation = await Geolocator
                                                            .getCurrentPosition(
                                                                desiredAccuracy:
                                                                    LocationAccuracy
                                                                        .high);
                                                      } catch (e) {
                                                        geolocation = null;
                                                      }

                                                      if (PostData.logintoken !=
                                                          null) {
                                                        if (geolocation !=
                                                            null) {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => BranchesPage(
                                                                      productid:
                                                                          productid,
                                                                      location:
                                                                          geolocation)));
                                                        } else {
                                                          showDialog(
                                                              context: context,
                                                              builder: (context) =>
                                                                  DialogMessage(
                                                                      'Try Again',
                                                                      'Please enable location service',
                                                                      false,
                                                                      '1'));
                                                        }
                                                      } else {
                                                        showDialog(
                                                            context: context,
                                                            builder: (context) =>
                                                                DialogGuest());
                                                      }
                                                    }, 'big'),
                                                    VerticalDivider(
                                                      thickness: 2,
                                                      width: 1,
                                                      //indent: 0,
                                                      //endIndent: 10,
                                                      color: Mycolor.grey,
                                                    ),

                                                    ShoppingButton(
                                                        showappointment
                                                            ? 'appointment'
                                                            : 'appointment_disabled',
                                                        'Appointment',
                                                        () async {
                                                      if (showappointment) {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        AppointmentPage(
                                                                          merchantname:
                                                                              merchantName,
                                                                          logo:
                                                                              logo,
                                                                        )));
                                                      }

                                                      // Navigator.pushNamed(
                                                      //     context, '/appointment',
                                                      //     arguments: AppointmentPage(
                                                      //       merchantname: merchantName,
                                                      //       logo: logo,
                                                      //     ));
                                                    },
                                                        showappointment
                                                            ? 'big'
                                                            : 'small'),
                                                    // showappointment
                                                    //     ? OrangeButton('Make Appointment',
                                                    //         () {
                                                    //         Navigator.pushNamed(
                                                    //             context, '/appointment');
                                                    //       }, showappointment)
                                                    //     : Container(),
                                                  ]),
                                            )
                                          : Container(
                                              height: 60,
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: productType == '1'
                                                      ? 30
                                                      : 50),
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 10),
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    ShoppingButton(
                                                        'share', 'Share', () {
                                                      String text =
                                                          "Check this out, only available in T-Card!\n\nMake an appointment now\n\nTo find out more,click the link below:\n\nhttp://www.tcardmy.com/item/?id=" +
                                                              productid;
                                                      Share.share(
                                                        text,
                                                      );
                                                    }, 'big'),
                                                    VerticalDivider(
                                                      thickness: 2,
                                                      width: 1,
                                                      //indent: 0,
                                                      //endIndent: 10,
                                                      color: Mycolor.grey,
                                                    ),
                                                    ShoppingButton(
                                                        chatLive != null
                                                            ? 'chat'
                                                            : 'chat_disabled',
                                                        'Chat Now', () async {
                                                      String url = chatLive;
                                                      if (await canLaunch(
                                                          url)) {
                                                        await launch(url);
                                                      } else {
                                                        throw 'Could not launch $url';
                                                      }
                                                    },
                                                        chatLive != null
                                                            ? 'big'
                                                            : 'small'),
                                                    VerticalDivider(
                                                      thickness: 2,
                                                      width: 1,
                                                      //indent: 0,
                                                      //endIndent: 10,
                                                      color: Mycolor.grey,
                                                    ),
                                                    ShoppingButton(
                                                        chatLive != null
                                                            ? 'location'
                                                            : 'location',
                                                        'Outlets', () async {
                                                      Position geolocation;
                                                      try {
                                                        geolocation = await Geolocator
                                                            .getCurrentPosition(
                                                                desiredAccuracy:
                                                                    LocationAccuracy
                                                                        .high);
                                                      } catch (e) {
                                                        geolocation = null;
                                                      }

                                                      if (PostData.logintoken !=
                                                          null) {
                                                        if (geolocation !=
                                                            null) {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => BranchesPage(
                                                                      productid:
                                                                          productid,
                                                                      location:
                                                                          geolocation)));
                                                        } else {
                                                          showDialog(
                                                              context: context,
                                                              builder: (context) =>
                                                                  DialogMessage(
                                                                      'Try Again',
                                                                      'Please enable location service',
                                                                      false,
                                                                      '1'));
                                                        }
                                                      } else {
                                                        showDialog(
                                                            context: context,
                                                            builder: (context) =>
                                                                DialogGuest());
                                                      }
                                                    },
                                                        chatLive != null
                                                            ? 'big'
                                                            : 'small'),
                                                  ]),
                                            ),
                                    ])),
                              ],
                            )))),
                bottomNavigationBar: Container(
                    color: Mycolor.normalWhite,
                    padding: EdgeInsets.only(bottom: 10),
                    child: Container(
                        padding:
                            EdgeInsets.only(left: 15, right: 15, bottom: 10),
                        //    margin: EdgeInsets.only(bottom: 10),
                        height: 60,
                        color: Mycolor.normalWhite,
                        child: productType == '1'
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/time.png',
                                        width: 30,
                                        height: 30,
                                      ),
                                      Text(
                                        '  $dayLeft days left!',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Mycolor.darkgrey,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ],
                                  ),

                                  // Container(
                                  //   alignment: Alignment.center,
                                  //   height: 50,
                                  //   color: Colors.black38,
                                  //   width: MediaQuery.of(context).size.width * 0.5,
                                  //   child: Text(
                                  //     '$dayLeft DAYS LEFT!',
                                  //     textAlign: TextAlign.center,
                                  //     style: TextStyle(
                                  //         fontSize: 15, fontWeight: FontWeight.bold),
                                  //   ),
                                  // ),

                                  !hasBeenRedeemed
                                      ? Button('Scan', () {
                                          if (PostData.logintoken != null) {
                                            showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    DialogConfirm(
                                                      title: 'Dear Member',
                                                      message:
                                                          "Click proceed to Scan QR at your preferred outlet",
                                                      proceedtext: 'Proceed',
                                                      id: product.productId,
                                                      mode: product.type,
                                                      points:
                                                          product.pointsNeeded,
                                                      amountPaid:
                                                          product.sellingPrice,
                                                    ));
                                          } else {
                                            showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    DialogGuest());
                                          }
                                        })
                                      : ButtonTheme(
                                          minWidth: 120.0,
                                          //height: 100.0,
                                          child: RaisedButton(
                                            color: Colors.red,
                                            textColor: Mycolor.white,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        100.0)),
                                            onPressed: () {},
                                            child: Text(
                                              "Unavailable",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  height: 1),
                                            ),
                                          ))

                                  // InkWell(
                                  //   onTap: () {
                                  //     if (PostData.logintoken != null) {
                                  //       showDialog(
                                  //           context: context,
                                  //           builder: (context) => DialogConfirm(
                                  //                 title: 'Dear Member',
                                  //                 message:
                                  //                     "Click Proceed offer will be\nredeemed.",
                                  //                 proceedtext: 'Proceed',
                                  //                 id: product.productId,
                                  //                 mode: product.type,
                                  //                 points: product.pointsNeeded,
                                  //                 amountPaid: product.sellingPrice,
                                  //               ));
                                  //     } else {
                                  //       showDialog(
                                  //           context: context,
                                  //           builder: (context) => DialogGuest());
                                  //     }
                                  //   },
                                  //   child: Container(
                                  //     alignment: Alignment.center,
                                  //     height: 30,

                                  //     //color: Mycolor.darkGreen,
                                  //     decoration: BoxDecoration(
                                  //         color: Mycolor.darkGreen,
                                  //         borderRadius: BorderRadius.circular(50)),
                                  //     width:
                                  //         MediaQuery.of(context).size.width * 0.4,
                                  //     child: Text(
                                  //       'REDEEM',
                                  //       textAlign: TextAlign.center,
                                  //       style: TextStyle(
                                  //           color: Mycolor.white,
                                  //           fontSize: 18,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ),
                                  // )
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.6,
                                    child: Text(
                                      'Product can only purchased in store,\nplease visit to your nearest store.',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Mycolor.darkgrey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),

                                  // Container(
                                  //   alignment: Alignment.center,
                                  //   height: 50,
                                  //   color: Colors.black38,
                                  //   width: MediaQuery.of(context).size.width * 0.5,
                                  //   child: Text(
                                  //     '$dayLeft DAYS LEFT!',
                                  //     textAlign: TextAlign.center,
                                  //     style: TextStyle(
                                  //         fontSize: 15, fontWeight: FontWeight.bold),
                                  //   ),
                                  // ),

                                  InkWell(
                                    onTap: () {
                                      if (PostData.logintoken != null) {
                                        showDialog(
                                            context: context,
                                            builder: (context) => DialogConfirm(
                                                title: 'Dear Member',
                                                message:
                                                    'Product can only purchased in store,\nplease visit to your nearest store.',
                                                proceedtext: 'Proceed',
                                                id: product.productId,
                                                mode: product.type,
                                                points: product.pointsNeeded,
                                                amountPaid:
                                                    product.sellingPrice,
                                                image: imgList[0],
                                                sellingPriceWithPoint:
                                                    sellingPriceWithPoint,
                                                productTitle: title));
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (context) =>
                                                DialogGuest());
                                      }
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 30,

                                      //color: Mycolor.darkGreen,
                                      decoration: BoxDecoration(
                                          color: Mycolor.darkGreen,
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      child: Text(
                                        'Buy',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Mycolor.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            height: 1),
                                      ),
                                    ),
                                  )
                                ],
                              ))));
          } else if (snapshot.hasError) {
            if (PostData.update) {
              return DialogUpdate();
            } else if (PostData.statusCode == 403) {
              return DialogRelogin();
            } else {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                title: Column(children: [
                  Text(
                    'Connection Error',
                    style: TextStyle(
                        color: Mycolor.darkGreen,
                        fontSize: 24,
                        fontWeight: FontWeight.w900),
                  )
                ]),
              );
            }
          } else {
            return LoadingOverlay(
                color: Colors.grey,
                progressIndicator: tcardProgressDialog(),
                child: Center(),
                isLoading: true);
          }
        });
  }
}
