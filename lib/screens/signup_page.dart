import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialog_register.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/otp_dialog.dart';
import 'package:tcard/utils/mycolor.dart';
import 'dart:ui';
import 'package:intl/intl.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/components/datepicker.dart';
import 'package:tcard/components/logo.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/config/postData.dart';
import 'package:age/age.dart';

class SignupPage extends StatefulWidget {
  SignupPage({Key key}) : super(key: key);

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  // Init the text controller
  TextEditingController _formatCtrl = TextEditingController();
  TextEditingController _panelPasscodeCtrl = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  // Global variable so that anywhere also can access this dateTime
  String _gender;
  bool _checkedPanelValue = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    var platform = Theme.of(context).platform;
    return Scaffold(
      body: Builder(
        builder: (context) => SafeArea(
          child: Container(
              decoration: BoxDecoration(color: Mycolor.white),
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: ListView(children: [
                Stack(
                  children: [
                    Image.asset(
                      'assets/images/background.png',
                    ),
                    Row(
                      children: [
                        ReturnButton(),
                      ],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 120),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Mycolor.normalWhite,
                        child: Column(
                          children: [
                            Logo(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 0.0),
                                  child: Text(
                                    "Sign Up",
                                    style: TextStyle(
                                        color: Mycolor.darkGreen,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                                'Fill in your personal information\nin order to plan you dental care\n'),
                            Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Input(
                                    value: 'Full Name*',
                                    textArea: 1,
                                    textController: name,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Name field is required*';
                                      } else if (value
                                              .trimLeft()
                                              .trimRight()
                                              .split(' ')
                                              .length <
                                          2) {
                                        return 'Name must atleast 2 words';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      name.text = value;
                                    },
                                  ),
                                  Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        DatePickerInput(
                                          label: 'Date Of Birth*',
                                          controller: _formatCtrl,
                                          min: false,
                                          onsaved: (value) {
                                            _formatCtrl = value;
                                          },
                                          validate: (value) {
                                            print('date1' + value);
                                            if (value.isEmpty) {
                                              return null;
                                            } else if (Age.dateDifference(
                                                        fromDate: DateFormat(
                                                                'dd / mm / yyyy')
                                                            .parse(value),
                                                        toDate: DateTime.now())
                                                    .years <
                                                12) {
                                              return 'Must atleast 12 years old';
                                            }
                                            return null;
                                          },
                                        ),
                                        // Container(
                                        //     margin: EdgeInsets.only(
                                        //         top: 5, bottom: 5),
                                        //     // padding: EdgeInsets.only(top: 0),
                                        //     child: Theme(
                                        //         data: Theme.of(context)
                                        //             .copyWith(
                                        //                 unselectedWidgetColor:
                                        //                     Mycolor.darkGreen,
                                        //                 disabledColor:
                                        //                     Mycolor.darkGreen),
                                        //         child: Row(
                                        //           crossAxisAlignment:
                                        //               CrossAxisAlignment.center,
                                        //           mainAxisAlignment:
                                        //               MainAxisAlignment.center,
                                        //           children: [
                                        //             Radio(
                                        //               visualDensity:
                                        //                   VisualDensity.compact,
                                        //               materialTapTargetSize:
                                        //                   MaterialTapTargetSize
                                        //                       .shrinkWrap,
                                        //               activeColor:
                                        //                   Mycolor.darkGreen,
                                        //               value: "Male",
                                        //               groupValue: _gender,
                                        //               onChanged:
                                        //                   (String value) {
                                        //                 setState(() {
                                        //                   _gender = value;
                                        //                 });
                                        //               },
                                        //             ),
                                        //             Text(
                                        //               'Male',
                                        //               style: TextStyle(
                                        //                   fontSize: 14,
                                        //                   fontWeight:
                                        //                       FontWeight.bold,
                                        //                   color: Mycolor
                                        //                       .darkGreen),
                                        //             )
                                        //           ],
                                        //         ))),
                                        // Container(
                                        //   alignment: Alignment.topCenter,
                                        //   margin: EdgeInsets.only(
                                        //       right: 20, top: 5, bottom: 5),
                                        //   //  EdgeInsets.only(right: 20, top: 0),
                                        //   child: Theme(
                                        //       data: Theme.of(context).copyWith(
                                        //           unselectedWidgetColor:
                                        //               Mycolor.darkGreen,
                                        //           disabledColor:
                                        //               Mycolor.darkGreen),
                                        //       child: Row(
                                        //         children: [
                                        //           Radio(
                                        //             visualDensity:
                                        //                 VisualDensity.compact,
                                        //             materialTapTargetSize:
                                        //                 MaterialTapTargetSize
                                        //                     .shrinkWrap,
                                        //             activeColor:
                                        //                 Mycolor.darkGreen,
                                        //             value: "Female",
                                        //             groupValue: _gender,
                                        //             onChanged: (String value) {
                                        //               setState(() {
                                        //                 _gender = value;
                                        //               });
                                        //             },
                                        //           ),
                                        //           Text(
                                        //             'Female',
                                        //             style: TextStyle(
                                        //                 fontSize: 14,
                                        //                 fontWeight:
                                        //                     FontWeight.bold,
                                        //                 color:
                                        //                     Mycolor.darkGreen),
                                        //           )
                                        //         ],
                                        //       )),
                                        // )

                                        // Expanded(
                                        //   flex: 1,
                                        //   child: ListTile(
                                        //     title: Text("Male",
                                        //         style: TextStyle(
                                        //             color: Mycolor.darkGreen,
                                        //             fontSize: 14)),
                                        //     leading: Radio(
                                        //       activeColor: Mycolor.darkGreen,
                                        //       value: "Male",
                                        //       groupValue: _gender,
                                        //       onChanged: (String value) {
                                        //         setState(() {
                                        //           _gender = value;
                                        //         });
                                        //       },
                                        //     ),
                                        //   ),
                                        // ),
                                        // Expanded(
                                        //   flex: 1,
                                        //   child: ListTile(
                                        //     title: Text("Male",
                                        //         style: TextStyle(
                                        //           color: Mycolor.darkGreen,
                                        //         )),
                                        //     leading: Radio(
                                        //       activeColor: Mycolor.darkGreen,
                                        //       value: "Male",
                                        //       groupValue: _gender,
                                        //       onChanged: (String value) {
                                        //         setState(() {
                                        //           _gender = value;
                                        //         });
                                        //       },
                                        //     ),
                                        //   ),
                                        // ),
                                      ]),
                                  // Theme(
                                  //   data: Theme.of(context).copyWith(
                                  //       unselectedWidgetColor:
                                  //           Mycolor.darkGreen,
                                  //       disabledColor: Mycolor.darkGreen),
                                  //   child: Row(
                                  //     // /mainAxisAlignment: MainAxisAlignment.start,
                                  //     // crossAxisAlignment: CrossAxisAlignment.start,
                                  //     children: [
                                  //       Expanded(
                                  //         child: ListTile(
                                  //           title: Text("Male",
                                  //               style: TextStyle(
                                  //                 color: Mycolor.darkGreen,
                                  //               )),
                                  //           leading: Radio(
                                  //             activeColor: Mycolor.darkGreen,
                                  //             value: "Male",
                                  //             groupValue: _gender,
                                  //             onChanged: (String value) {
                                  //               setState(() {
                                  //                 _gender = value;
                                  //               });
                                  //             },
                                  //           ),
                                  //         ),
                                  //       ),
                                  //       Expanded(
                                  //         child: ListTile(
                                  //           title: Text("Female",
                                  //               style: TextStyle(
                                  //                 color: Mycolor.darkGreen,
                                  //               )),
                                  //           leading: Radio(
                                  //             activeColor: Mycolor.darkGreen,
                                  //             value: "Female",
                                  //             groupValue: _gender,
                                  //             onChanged: (String value) {
                                  //               setState(() {
                                  //                 _gender = value;
                                  //               });
                                  //             },
                                  //           ),
                                  //         ),
                                  //       ),
                                  //     ],
                                  //   ),
                                  // ),
                                  Input(
                                    value: 'Phone Number*',
                                    textArea: 1,
                                    textController: phone,
                                    keypad: true,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Phone Number field is required*';
                                      } else if (checkphoneregex(value) ==
                                          false) {
                                        return 'Invalid Malaysia number/Singapore number';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      phone.text = value;
                                    },
                                    //validate: true,
                                  ),
                                  Input(
                                    value: 'Email*',
                                    textArea: 1,
                                    textController: email,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Email field is required*';
                                      } else if (!RegExp(
                                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                          .hasMatch(value)) {
                                        return 'Email format is wrong';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      email.text = value;
                                    },
                                  ),
                                  Input(
                                    value: 'Panel Code*',
                                    textArea: 1,
                                    textController: _panelPasscodeCtrl,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  right: 10.0, left: 5.0, top: 10.0),
                              child: Button('Send OTP', () {
                                if (_formKey.currentState.validate()) {
                                  print('success');
                                  if (_formatCtrl.text.isEmpty) {
                                    showDialog(
                                        context: context,
                                        builder: (context) => DialogRegister(
                                              title: 'Dear Member',
                                              message:
                                                  "Are you sure you want to register without Date of Birth? You might miss out on great gifts from TCARD!",
                                              proceedtext: 'Proceed',
                                              proceed: () {
                                                var response = PostData()
                                                    .callOtpAPI(phone.text);
                                                response.then((value) {
                                                  print('platform' +
                                                      platform.toString());
                                                  if (value.statusCode == 200) {
                                                    showDialog(
                                                        context: context,
                                                        builder: (context) =>
                                                            OtpDialog(data: {
                                                              'deviceType': platform ==
                                                                      TargetPlatform
                                                                          .iOS
                                                                  ? 'iOS'
                                                                  : 'Android',
                                                              'name': name.text,
                                                              'dob': '',
                                                              'gender': _gender ==
                                                                      'Female'
                                                                  ? 'F'
                                                                  : 'M',
                                                              'email':
                                                                  email.text,
                                                              'phone':
                                                                  phone.text,
                                                              'panel':
                                                                  _panelPasscodeCtrl
                                                                          .text
                                                                          .isNotEmpty
                                                                      ? 'Y'
                                                                      : 'N',
                                                              'panelcode':
                                                                  _panelPasscodeCtrl
                                                                          .text
                                                                          .isNotEmpty
                                                                      ? _panelPasscodeCtrl
                                                                          .text
                                                                      : 0,
                                                            }));
                                                  } else {
                                                    Navigator.of(context).pop();
                                                    showDialog(
                                                        context: context,
                                                        builder: (context) =>
                                                            DialogMessage(
                                                                'Try Again',
                                                                value.data[
                                                                    'message'],
                                                                false,
                                                                '1'));
                                                  }
                                                });
                                              },
                                            ));
                                  } else {
                                    var response =
                                        PostData().callOtpAPI(phone.text);
                                    response.then((value) {
                                      print('platform' + platform.toString());
                                      if (value.statusCode == 200) {
                                        showDialog(
                                            context: context,
                                            builder: (context) =>
                                                OtpDialog(data: {
                                                  'deviceType': platform ==
                                                          TargetPlatform.iOS
                                                      ? 'iOS'
                                                      : 'Android',
                                                  'name': name.text,
                                                  'dob': _formatCtrl.text,
                                                  'gender': _gender == 'Female'
                                                      ? 'F'
                                                      : 'M',
                                                  'email': email.text,
                                                  'phone': phone.text,
                                                  'panel': _panelPasscodeCtrl
                                                          .text.isNotEmpty
                                                      ? 'Y'
                                                      : 'N',
                                                  'panelcode':
                                                      _panelPasscodeCtrl
                                                              .text.isNotEmpty
                                                          ? _panelPasscodeCtrl
                                                              .text
                                                          : 0,
                                                }));
                                        // showDialog(
                                        //     context: context,
                                        //     builder: (context) =>
                                        //         TCardProgressDialog());

                                        // Navigator.pushReplacement(
                                        //     context,
                                        //     MaterialPageRoute(
                                        //         builder: (context) =>
                                        //             OtpVerificationPage(data: {
                                        //               'deviceType': platform ==
                                        //                       TargetPlatform.iOS
                                        //                   ? 'iOS'
                                        //                   : 'Android',
                                        //               'name': name.text,
                                        //               'dob': _formatCtrl.text,
                                        //               'gender': _gender == 'Male'
                                        //                   ? 'M'
                                        //                   : 'F',
                                        //               'email': email.text,
                                        //               'phone': phone.text,
                                        //               'panel':
                                        //                   _checkedPanelValue ==
                                        //                           true
                                        //                       ? 'Y'
                                        //                       : 'N',
                                        //               'panelcode':
                                        //                   _checkedPanelValue ==
                                        //                           true
                                        //                       ? _panelPasscodeCtrl
                                        //                           .text
                                        //                       : 0,
                                        //             })));
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (context) => DialogMessage(
                                                'Try Again',
                                                value.data['message'],
                                                false,
                                                '1'));
                                      }
                                    });
                                  }
                                  //Proceed call API

                                }
                              }),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 100),
                              child: Divider(
                                thickness: 2,

                                //indent: 0,
                                //endIndent: 10,
                                color: Mycolor.grey,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushReplacementNamed(
                                    context, '/login');
                                _showToast(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10.0, bottom: 10.0),
                                    height: 30,
                                    child: Center(
                                      child: Text("Already have an account?"),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10.0, bottom: 10.0),
                                    height: 30,
                                    child: Center(
                                      child: Text(" Login ",
                                          style: TextStyle(
                                              color: Mycolor.urlBlue,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                  Text('here!')
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ])),
        ),
      ),
    );
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('I was clicked'),
        action: SnackBarAction(
            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  bool checkphoneregex(String phone) {
    if (RegExp(r"^01\d{8,9}$").hasMatch(phone)) {
      return true;
    } else if (RegExp(r"^(65)(6|8|9)\d{7}$").hasMatch(phone)) {
      return true;
    } else {
      return false;
    }
  }

  // void _showDatePicker() {
  //   DatePicker.showDatePicker(
  //     context,
  //     onMonthChangeStartWithFirstDate: true,
  //     pickerTheme: DateTimePickerTheme(
  //       showTitle: true,
  //       confirm:
  //           Text('Choose Date', style: TextStyle(color: Mycolor.darkGreen)),
  //       cancel: Text('Cancel', style: TextStyle(color: Colors.red)),
  //     ),
  //     minDateTime: DateTime.parse(MIN_DATETIME),
  //     maxDateTime: DateTime.parse(MAX_DATETIME),
  //     initialDateTime: _dateTime,
  //     dateFormat: _format,
  //     locale: _locale,
  //     onCancel: () => print('onCancel'),
  //     onChange: (dateTime, List<int> index) {
  //       setState(() {
  //         _dateTime = dateTime;
  //       });
  //     },
  //     onConfirm: (dateTime, List<int> index) {
  //       setState(() {
  //         _dateTime = dateTime;
  //         _formatCtrl.text = DateFormat('dd / MM / yyyy').format(_dateTime);
  //       });
  //     },
  //   );
  // }
}
