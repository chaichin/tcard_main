import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/utils/mycolor.dart';

class ConvertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Convert',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/coming.png',
                width: 120,
                height: 120,
              ),
              Text(
                'Something exciting\nis coming soon!',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Mycolor.grey),
                textAlign: TextAlign.center,
              )
            ],
          ))
        ]));
  }
}
