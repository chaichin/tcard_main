import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:provider/provider.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/services/huawei_notification_service.dart';
import 'package:tcard/services/notificationService.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/components/logo.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<LoginPage> {
  final userController = TextEditingController();
  final passwordController = TextEditingController();

  Future<bool> loader() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => tcardProgressDialog());
  }

  final _formKey = GlobalKey<FormState>();
  // @override
  // void initState() {
  //   super.initState();
  //   setState(() {
  //     loading = false;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        body: Builder(
      builder: (context) => SafeArea(
          child: Container(
              decoration: BoxDecoration(color: Mycolor.white),
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: ListView(children: [
                Stack(
                  children: [
                    Image.asset(
                      'assets/images/background.png',
                    ),
                    Row(
                      children: [
                        ReturnButton(),
                      ],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 120),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Mycolor.normalWhite,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Logo(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 0.0),
                                  child: Text(
                                    "Login",
                                    style: TextStyle(
                                        color: Mycolor.darkGreen,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                                'Fill in your personal information\nin order to plan you dental care\n'),
                            Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Input(
                                    value: 'Phone Number',
                                    textArea: 1,
                                    textController: userController,
                                    keypad: true,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Phone Number field is required*';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      userController.text = value;
                                    },
                                  ),
                                  Input(
                                    value: 'Pin Number',
                                    textArea: 1,
                                    textController: passwordController,
                                    keypad: true,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Pin Number field is required*';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      passwordController.text = value;
                                    },
                                  ),
                                ],
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, '/reset');
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 10, right: 25),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      RichText(
                                          text: TextSpan(
                                              text: 'Reset Pin',
                                              style: TextStyle(
                                                  decoration:
                                                      TextDecoration.underline,
                                                  color: Mycolor.urlBlue,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w300)))
                                    ]),
                              ),
                            ),
                            Center(
                                child: Container(
                              margin: EdgeInsets.only(top: 0),
                              child: Button('Login', () {
                                //Navigator.pushNamed(context, '/home');

                                print(userController.text);
                                print(passwordController.text);
                                // if (userController.text.isEmpty ||
                                //     passwordController.text.isEmpty) {
                                //   showDialog(
                                //       context: context,
                                //       builder: (context) => DialogMessage(
                                //           'Try Again',
                                //           'Both Phone number and Password field is required'));
                                // } else {
                                if (_formKey.currentState.validate()) {
                                  var response = PostData().callLoginAPI(
                                      userController.text,
                                      passwordController.text);
                                  response.then((value) {
                                    if (value.statusCode == 200) {
                                      print(value.data);

                                      showDialog(
                                          context: context,
                                          builder: (context) =>
                                              tcardProgressDialog());
                                      FlutterHmsGmsAvailability.isGmsAvailable
                                          .then((google) {
                                        final p =
                                            Provider.of<NotificationState>(
                                                context,
                                                listen: false);

                                        if (google || Platform.isIOS) {
                                          final n = NotificationService();

                                          n.fcm();
                                          n.addListener(() {
                                            p.add();
                                          });
                                        } else {
                                          FlutterHmsGmsAvailability
                                              .isHmsAvailable
                                              .then((huawei) {
                                            if (huawei) {
                                              final huawei =
                                                  HuaweiNotification();
                                              huawei.getToken();
                                              huawei.addListener(() {
                                                p.add();
                                              });
                                            }
                                          });
                                        }
                                      });
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil('/main',
                                              (Route<dynamic> route) => false);

                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //     builder: (context) =>
                                      //         MainPage(selectedPage: 0),
                                      //   ),
                                      // );
                                    } else {
                                      showDialog(
                                          context: context,
                                          builder: (context) => DialogMessage(
                                              'Try Again',
                                              value.data['message'],
                                              false,
                                              '1'));
                                    }
                                  });
                                }
                                // showDialog(
                                //     context: context,
                                //     builder: (context) => tcardProgressDialog());

                                //Proceed call API

                                // Navigator.pushReplacementNamed(context, '/home');
                              }),
                            )),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 100),
                              child: Divider(
                                thickness: 2,

                                //indent: 0,
                                //endIndent: 10,
                                color: Mycolor.grey,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Don't have an account?"),
                                InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(context, '/signup');
                                  },
                                  child: Text(
                                    ' Register ',
                                    style: TextStyle(
                                        color: Mycolor.urlBlue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Text('here!')
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ]))),
    ));
  }
}
