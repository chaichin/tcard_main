import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/newSlider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/search.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/back_buttons.dart';

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchPageState();
  }
}

class SearchPageState extends State<SearchPage>
    with SingleTickerProviderStateMixin {
  final searchQueryController = TextEditingController();
  List<Search> products = [];
  String search = '';
  String filter = 'all';
  bool filterType;
  @override
  void initState() {
    PostData().searchProduct(search, filter).then((value) {
      setState(() {
        products = value;
      });
    });
    print(products);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: TextField(
            textInputAction: TextInputAction.go,
            controller: searchQueryController,
            onChanged: (text) {
              // setState(() {});
              PostData().searchProduct(text, filter).then((value) {
                setState(() {
                  print('value' + value.toString());
                  products = value;
                });
              });
            },
            //autofocus: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIconConstraints: BoxConstraints(
                minHeight: 30,
                minWidth: 30,
              ),
              prefixIcon: Padding(
                  padding: const EdgeInsets.only(left: 5, right: 0),
                  child: Image.asset('assets/images/search.png',
                      fit: BoxFit.scaleDown, width: 0, height: 0)),
              contentPadding:
                  EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
              isDense: true,
              hintText: "Search Data...",
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(
                  25.0,
                ),
                borderSide: BorderSide(color: Mycolor.darkerGreen, width: 1.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(
                  25.0,
                ),
                borderSide: BorderSide(color: Mycolor.darkerGreen, width: 1.0),
              ),
              hintStyle: TextStyle(color: Mycolor.darkerGreen),
            ),
            style: TextStyle(color: Mycolor.darkerGreen, fontSize: 20.0),
            // onChanged: (search) {
            //   setState(() {
            //     search = search;
            //   });
            // }
          ),
          actions: [
            // InkWell(
            //   onTap: () {
            //     print(searchQueryController.text);

            //     PostData()
            //         .searchProduct(searchQueryController.text)
            //         .then((value) => setState(() {
            //               filter = null;
            //               products =
            //                   searchQueryController.text.isEmpty ? [] : value;
            //             }));
            //   },
            //   child: Icon(
            //     Icons.search,
            //     color: Mycolor.darkGreen,
            //     size: 40,
            //   ),
            // )
          ],
          //Image.asset("assets/images/logo.png", width: 80, height: 60),
        ),
        body: Stack(children: [
          Container(
              padding: EdgeInsets.only(
                top: 40,
              ),
              child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  physics: const BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  itemCount: products.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    List<String> merchantImages = products[index]
                        .images
                        .split(',')
                        .map((e) => e)
                        .toList();
                    print(merchantImages);
                    return NewSliderItem(
                        value: 'shopping',
                        flex: true,
                        source: merchantImages[0],
                        title: products[index].title,
                        logo: products[index].merchantLogo,
                        productid: products[index].productId,
                        views: products[index].numberOfViews);
                  })),
          Container(
              padding: EdgeInsets.only(left: 20, right: 50),
              child: Row(children: [
                Text(
                  'Filter:',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Mycolor.darkgrey),
                ),
                Expanded(
                  child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Mycolor.darkgrey,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Radio(
                                visualDensity: VisualDensity.compact,
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                activeColor: Mycolor.darkgrey,
                                groupValue: filter,
                                value: 'all',
                                onChanged: (value) {
                                  setState(() {
                                    filter = value;
                                  });
                                  PostData()
                                      .searchProduct(
                                          searchQueryController.text, value)
                                      .then((value) {
                                    setState(() {
                                      products = value;
                                    });
                                  });
                                  print('fil' + filter);
                                },
                              ),
                              Text(
                                'All',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Mycolor.darkgrey),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                visualDensity: VisualDensity.compact,
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                activeColor: Mycolor.darkgrey,
                                groupValue: filter,
                                value: 'free',
                                onChanged: (value) {
                                  setState(() {
                                    filter = value;
                                  });
                                  PostData()
                                      .searchProduct(
                                          searchQueryController.text, value)
                                      .then((value) {
                                    setState(() {
                                      products = value;
                                    });
                                  });
                                  print('fil' + filter);
                                },
                              ),
                              Text(
                                'Free',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Mycolor.darkgrey),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Row(
                                children: [
                                  Radio(
                                    visualDensity: VisualDensity.compact,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    activeColor: Mycolor.darkgrey,
                                    groupValue: filter,
                                    value: 'discount',
                                    onChanged: (value) {
                                      setState(() {
                                        filter = value;
                                      });
                                      PostData()
                                          .searchProduct(
                                              searchQueryController.text, value)
                                          .then((value) {
                                        setState(() {
                                          products = value;
                                        });
                                      });
                                      print('fil' + filter);
                                    },
                                  ),
                                  Text(
                                    'Discount',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Mycolor.darkgrey),
                                  )
                                ],
                              ),
                            ],
                          )
                        ],
                      )),
                ),
              ]))
        ])
        // Padding(
        //   padding: const EdgeInsets.only(top: 0, left: 0, right: 0),
        //   child: Column(
        //     children: [
        //       // give the tab bar a height [can change hheight to preferred height]
        //       Container(
        //         margin:
        //             const EdgeInsets.only(top: 10, left: 3, right: 3, bottom: 10),
        //         height: 35,
        //         decoration: BoxDecoration(
        //           color:
        //               //Colors.grey[300],
        //               Mycolor.white,
        //           border: Border.all(color: Mycolor.darkGreen),
        //           borderRadius: BorderRadius.circular(
        //             25.0,
        //           ),
        //         ),
        //         child: TabBar(
        //           controller: _tabController,
        //           // give the indicator a decoration (color and border radius)
        //           indicator: BoxDecoration(
        //             borderRadius: BorderRadius.circular(
        //               25.0,
        //             ),
        //             color: Mycolor.darkGreen,
        //           ),
        //           labelColor: Colors.white,
        //           unselectedLabelColor: Mycolor.darkGreen,
        //           tabs: PostData.panel
        //               ? [
        //                   // first tab [you can add an icon using the icon property]
        //                   Tab(
        //                     // text: 'Special Treats',
        //                     child: Text(
        //                       'Special Treats',
        //                       style: TextStyle(fontSize: 12),
        //                     ),
        //                   ),

        //                   // second tab [you can add an icon using the icon property]
        //                   Tab(
        //                       child: Text(
        //                     'Merchant',
        //                     style: TextStyle(fontSize: 12),
        //                   )),
        //                   Tab(
        //                       child: Text(
        //                     'Shopping',
        //                     style: TextStyle(fontSize: 12),
        //                   )),
        //                 ]
        //               : [
        //                   Tab(
        //                       child: Text(
        //                     'Merchant',
        //                     style: TextStyle(fontSize: 12),
        //                   )),
        //                   Tab(
        //                       child: Text(
        //                     'Shopping',
        //                     style: TextStyle(fontSize: 12),
        //                   )),
        //                 ],
        //         ),
        //       ),
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         children: [
        //           Text('Filter: '),
        //           RaisedButton(
        //             color:
        //                 filter == 'Free' ? Mycolor.darkgrey : Mycolor.redOrange,
        //             textColor: Mycolor.white,
        //             shape: RoundedRectangleBorder(
        //                 borderRadius: BorderRadius.circular(100.0)),
        //             onPressed: () {
        //               setState(() {
        //                 filter = filter == 'Free' ? '' : 'Free';
        //                 searchQueryController.text = '';
        //                 PostData()
        //                     .searchProduct(filter)
        //                     .then((value) => setState(() {
        //                           products = filter == '' ? [] : value;
        //                           print(products);
        //                         }));
        //               });
        //             },
        //             child: Text(
        //               "Free",
        //               style: TextStyle(fontWeight: FontWeight.w300),
        //             ),
        //           ),
        //           RaisedButton(
        //             color: filter == 'Discount'
        //                 ? Mycolor.darkgrey
        //                 : Mycolor.redOrange,
        //             textColor: Mycolor.white,
        //             shape: RoundedRectangleBorder(
        //                 borderRadius: BorderRadius.circular(100.0)),
        //             onPressed: () {
        //               setState(() {
        //                 filter = filter == 'Discount' ? '' : 'Discount';
        //                 searchQueryController.text = '';
        //                 PostData()
        //                     .searchProduct(filter)
        //                     .then((value) => setState(() {
        //                           products = filter == '' ? [] : value;
        //                         }));
        //               });
        //             },
        //             child: Text(
        //               "Discount",
        //               style: TextStyle(fontWeight: FontWeight.w300),
        //             ),
        //           )
        //         ],
        //       ),
        //       // tab bar view here
        //       Expanded(
        //         child: TabBarView(
        //           controller: _tabController,
        //           children: PostData.panel
        //               ? [
        //                   ListTab(
        //                       value: 'merchant',
        //                       products: products,
        //                       type: 'SPECIAL'),
        //                   ListTab(
        //                       value: 'merchant',
        //                       products: products,
        //                       type: 'MERCHANT'),
        //                   ListTab(
        //                       value: 'shopping',
        //                       products: products,
        //                       type: 'SHOPPING')
        //                 ]
        //               : [
        //                   ListTab(
        //                       value: 'merchant',
        //                       products: products,
        //                       type: 'MERCHANT'),
        //                   ListTab(
        //                       value: 'shopping',
        //                       products: products,
        //                       type: 'SHOPPING')
        //                 ],
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        // ])
        );
  }
}
