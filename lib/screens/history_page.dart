import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:tcard/components/history_voucher.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/historyPoint.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/back_buttons.dart';

class HistoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HistoryPageState();
  }
}

class HistoryPageState extends State<HistoryPage> {
  final PagingController<int, HistoryPoint> _pagingController =
      PagingController(firstPageKey: 1);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      // PostData().getProductApi(pageKey).then((value) {
      //   newItems = value.merchant;
      // });
      PostData().getHistoryPoint('Point', pageKey).then((value) {
        final isLastPage = value.length == 0;

        if (isLastPage) {
          _pagingController.appendLastPage(value);
        } else {
          final nextPageKey = pageKey + 1;
          _pagingController.appendPage(value, nextPageKey);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'History',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: Theme(
            data: Theme.of(context).copyWith(accentColor: Mycolor.darkGreen),
            child: RefreshIndicator(
                onRefresh: () => Future.sync(
                      // 2
                      () => _pagingController.refresh(),
                    ),
                child: PagedListView<int, HistoryPoint>(
                  // padding: EdgeInsets.only(right: 20),
                  physics: const BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  pagingController: _pagingController,
                  builderDelegate: PagedChildBuilderDelegate<HistoryPoint>(
                      itemBuilder: (context, item, index) {
                    return HistoryVoucher(
                      index: index,
                      title:
                          item.voucherSerial != null ? item.voucherSerial : '',
                      amount: item.endAmount,
                      name: item.title != null ? item.title : '-',
                      date: DateFormat('dd/MM/yyyy hh:mm a')
                          .format(item.insertDate),
                      point: item.transferAmount,
                      walletype: item.walletType,
                      invoiceid: item.invoiceId,
                    );
                  }),
                ))));
  }
}
