import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:flutter_switch/flutter_switch.dart';

import 'scan_page.dart';

class CheckoutPage extends StatefulWidget {
  final id;
  final points;
  final sellingPriceWithPoint;
  final price;
  final image;
  final title;
  CheckoutPage(
      {this.points,
      this.title,
      this.sellingPriceWithPoint,
      this.price,
      this.image,
      this.id});
  @override
  State<StatefulWidget> createState() {
    return CheckoutPageState();
  }
}

class CheckoutPageState extends State<CheckoutPage> {
  int quantity = 0;
  double total = 0;
  double subtotal = 0;
  double subtotalwithpoint = 0;
  double subtotalactual = 0;
  double usePointPrice = 0;
  int point = 0;
  bool status = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Mycolor.white,
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Checkout',
              //merchantName,
              style: TextStyle(
                fontSize: 24,
                color: Mycolor.darkerGreen,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      width: MediaQuery.of(context).size.width,
                      height: 70,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CachedNetworkImage(
                            imageUrl: widget.image,
                            width: 120,
                            height: 70,
                            fit: BoxFit.cover,
                          ),
                          // Image.network(
                          //   widget.image,
                          //   width: 120,
                          //   height: 70,
                          //   fit: BoxFit.cover,
                          // ),
                          Container(
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.title,
                                    style: TextStyle(
                                        color: Mycolor.darkGreen,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    'RM ${double.parse(widget.price).toStringAsFixed(2)}',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Mycolor.darkGreen,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ))
                        ],
                      )),
                  Container(
                    //  width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Quantity',
                          style: TextStyle(
                              color: Mycolor.darkGreen,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          width: 80,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                width: 20,
                                height: 20,
                                child: new FloatingActionButton(
                                  heroTag: "btn1",
                                  backgroundColor: Mycolor.grey,
                                  shape: RoundedRectangleBorder(),
                                  onPressed: () {
                                    setState(() {
                                      if (quantity != 0) {
                                        quantity--;

                                        subtotalactual =
                                            double.parse(widget.price) *
                                                quantity;

                                        point =
                                            int.parse(widget.points) * quantity;
                                        usePointPrice = double.parse(
                                                widget.sellingPriceWithPoint) *
                                            quantity;
                                        subtotal = status
                                            ? usePointPrice
                                            : subtotalactual;
                                        total =
                                            status ? usePointPrice : subtotal;
                                        print('subtotal' + subtotal.toString());
                                      }
                                    });
                                  },
                                  child: new Icon(
                                    Icons.remove,
                                    color: Colors.black,
                                    size: 15,
                                  ),
                                ),
                              ),
                              Text('$quantity',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18)),
                              Container(
                                  width: 20,
                                  height: 20,
                                  child: new FloatingActionButton(
                                    heroTag: "btn2",
                                    shape: RoundedRectangleBorder(),
                                    onPressed: () {
                                      setState(() {
                                        quantity++;

                                        subtotalactual =
                                            double.parse(widget.price) *
                                                quantity;

                                        point =
                                            int.parse(widget.points) * quantity;
                                        usePointPrice = double.parse(
                                                widget.sellingPriceWithPoint) *
                                            quantity;
                                        subtotal = status
                                            ? usePointPrice
                                            : subtotalactual;
                                        total =
                                            status ? usePointPrice : subtotal;
                                        print('sbtotal' +
                                            subtotal.toStringAsFixed(2));
                                      });
                                    },
                                    child: new Icon(
                                      Icons.add,
                                      color: Colors.black,
                                      size: 15,
                                    ),
                                    backgroundColor: Mycolor.darkgrey,
                                  )),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    color: Color(0xFFFEE0C6),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Icon(Icons.info_outline),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'Read and understand',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  height: 1),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 30,
                            ),
                            Container(
                                // decoration: BoxDecoration(
                                //     border:
                                //         Border.all(color: Mycolor.darkerGreen)),
                                width: 290,
                                child: Text(
                                  'Product can only be purchased in selected store. Please visit your nearest store or contact us if you have any inquiries .',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                ))
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sub-total :',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Mycolor.darkGreen),
                          ),
                          Text(
                            'RM ${subtotal.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Mycolor.darkGreen),
                          ),
                        ],
                      ),
                      Divider(thickness: 2, color: Mycolor.darkergrey),
                      widget.sellingPriceWithPoint != '0.00' ||
                              widget.points != '0'
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Use points',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Mycolor.darkGreen),
                                ),
                                Row(children: [
                                  Text(
                                    '($point points)  ',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                      'RM ${usePointPrice.toStringAsFixed(2)}  ',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold)),
                                  FlutterSwitch(
                                    activeColor: Colors.green,
                                    width: 40.0,
                                    height: 20.0,
                                    valueFontSize: 25.0,
                                    toggleSize: 18.0,
                                    value: status,
                                    borderRadius: 30.0,
                                    padding: 1.0,
                                    //showOnOff: true,
                                    onToggle: (val) {
                                      setState(() {
                                        status = val;
                                        usePointPrice = double.parse(
                                                widget.sellingPriceWithPoint) *
                                            quantity;
                                        subtotal = val
                                            ? usePointPrice
                                            : subtotalactual;
                                        total = val
                                            ? usePointPrice
                                            : subtotalactual;
                                        print('use' + usePointPrice.toString());
                                      });
                                    },
                                  )
                                ]),
                              ],
                            )
                          : Container(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Mycolor.darkGreen),
                          ),
                          Text(
                            'RM ${total.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Mycolor.darkGreen),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          // InkWell(
                          //   onTap: () {
                          //     if (quantity != 0) {
                          //       Navigator.push(
                          //           context,
                          //           MaterialPageRoute(
                          //               builder: (context) => ScanPage(
                          //                   id: widget.id,
                          //                   mode: '2',
                          //                   points:
                          //                       status ? point.toString() : '0',
                          //                   amountPaid: total.toString(),
                          //                   quantity: quantity.toString())));
                          //     } else {
                          //       showDialog(
                          //           context: context,
                          //           builder: (context) => DialogMessage(
                          //               'Try Again',
                          //               'Please set your quantity',
                          //               false,
                          //               '1'));
                          //     }
                          //   },
                          //   child:
                          Button('Checkout', () {
                            print('checkout quantity' + quantity.toString());
                            if (quantity != 0) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ScanPage(
                                          id: widget.id,
                                          mode: '2',
                                          points:
                                              status ? point.toString() : '0',
                                          amountPaid: total.toString(),
                                          quantity: quantity)));
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (context) => DialogMessage(
                                      'Try Again',
                                      'Please set your quantity',
                                      false,
                                      '1'));
                            }
                          })
                          //   Container(
                          //     alignment: Alignment.center,
                          //     height: 30,

                          //     //color: Mycolor.darkGreen,
                          //     decoration: BoxDecoration(
                          //         color: Mycolor.darkGreen,
                          //         borderRadius: BorderRadius.circular(50)),
                          //     width: MediaQuery.of(context).size.width * 0.3,
                          //     child: Text(
                          //       'Checkout',
                          //       textAlign: TextAlign.center,
                          //       style: TextStyle(
                          //           color: Mycolor.white,
                          //           fontSize: 14,
                          //           height: 1,
                          //           fontWeight: FontWeight.bold),
                          //     ),
                          //   ),
                          // )
                        ],
                      )
                    ],
                  ))
            ]));
  }
}
