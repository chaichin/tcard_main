import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialog_image.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/notification.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/tabs/wallet_tab.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/screens/tabs/home_tab.dart';
import 'package:tcard/screens/tabs/merchant_tab.dart';
import 'package:tcard/screens/tabs/scan_tab.dart';
import 'package:tcard/screens/tabs/shopping_tab.dart';
import 'package:tcard/components/drawer.dart';

class MainPage extends StatefulWidget {
  final shoppingTab;

  final deeplink;
  MainPage({this.deeplink, this.shoppingTab});
  @override
  State<StatefulWidget> createState() {
    return _MainPageState(
      deeplink: deeplink,
    );
  }
}

class _MainPageState extends State<MainPage> {
  final deeplink;
  int _index = 0;
  bool showvalue = false;
  int tabindex;
  _MainPageState({this.deeplink});

  @override
  void initState() {
    print('chc' + deeplink.toString());

    super.initState();
    if (deeplink != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ShoppingPage(productid: deeplink)));
      });
    }
    getShow().then((value) {
      print('pref' + value.toString());
      PostData().getDialog().then((dialog) {
        if (dialog.carouselId != null) {
          if (!value) {
            Future.delayed(Duration.zero, () {
              showAlert(context, dialog);
            });
          }
        }
      });
    });
  }

  Future<bool> getShow() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('show') == null ? false : prefs.getBool('show');
  }

  Widget body() {
    switch (_index) {
      case 0:
        return HomeTab(
          changeMerchant: () {
            setState(() {
              _index = 1;
              tabindex = PostData.panel ? 1 : 0;
            });
          },
          changeShopping: () {
            setState(() {
              _index = 1;
              tabindex = PostData.panel ? 2 : 1;
            });
          },
          changeSpecial: () {
            setState(() {
              _index = 1;
              tabindex = 0;
            });
          },
        );
        break;
      case 1:
        return ShoppingTab(
          selectedTab: tabindex,
        );
        break;
      // case 2:
      //   return ScanTab();
      //   break;
      case 3:
        return MerchantTab();
        break;
      case 4:
        return WalletTab(
          selectedTab: () {
            onTabTapped(4);
          },
        );
        break;
    }
    return HomeTab();
  }

  void onTabTapped(int index) {
    setState(() {
      //_index = index;
      _index = index == 2 ? _index : index;
      tabindex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(_index);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return WillPopScope(
        onWillPop: () async {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text(
                      "Are you sure to quit tcard App?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Mycolor.darkGreen),
                    ),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Button('Exit', () {
                          SystemNavigator.pop();
                        }),
                        Button('Cancel', () {
                          Navigator.pop(context);
                        })
                      ],
                    ),
                  )).then((value) => value ?? false);
        },
        child: Scaffold(
            backgroundColor: Mycolor.white,
            drawer: CustomDrawer(),
            appBar: AppBar(
                brightness: Brightness.light,
                elevation: 0,
                centerTitle: true,
                backgroundColor: Mycolor.white,
                title: Text(
                  _index == 0
                      ? 'HOME'
                      : _index == 1
                          ? 'SHOPPING'
                          : _index == 2
                              ? 'SCAN'
                              : _index == 3
                                  ? 'MERCHANT'
                                  : 'WALLET',
                  style: TextStyle(
                      fontSize: 24,
                      color: Mycolor.darkerGreen,
                      fontWeight: FontWeight.w900),
                ),
                //Image.asset("assets/images/logo.png", width: 80, height: 60),
                iconTheme: IconThemeData(color: Mycolor.darkGreen),
                actions: [
                  _index == 0 || _index == 1
                      ? InkWell(
                          onTap: () {
                            PostData.update
                                ? showDialog(
                                    context: context,
                                    builder: (context) => DialogUpdate())
                                : Navigator.pushNamed(context, '/search');
                            // showAlert(context);
                          },
                          child: Container(
                              // minWidth: 100,

                              child: Image.asset('assets/images/search.png',
                                  width: 30, height: 30)),
                        )
                      : Container(),
                  const SizedBox(
                    width: 5,
                  ),
                  _index == 0 || _index == 1
                      ? Container(
                          margin: EdgeInsets.only(right: 10),
                          child: NotificationBadge(),
                        )
                      : Container()

                  // const SizedBox(
                  //   width: 0,
                  // ),
                ],
                leading: Builder(
                  builder: (BuildContext context) {
                    return IconButton(
                      icon: Image.asset("assets/images/menu.png",
                          width: 25, height: 30),
                      onPressed: () => Scaffold.of(context).openDrawer(),
                    );
                  },
                )
                //actions: [],
                ),
            body: body(),
            bottomNavigationBar: BottomNavigationBar(
              unselectedFontSize: 14,
              selectedFontSize: 14,
              selectedItemColor: Mycolor.darkerGreen,
              selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
              type: BottomNavigationBarType.fixed,
              onTap: onTabTapped,
              currentIndex: _index,
              items: [
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/home.png",
                      width: 40,
                      height: 40,
                    ),
                    activeIcon: Image.asset(
                      "assets/images/home-white.png",
                      width: 40,
                      height: 40,
                    ),
                    label: 'Home'),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/shopping.png",
                      width: 40,
                      height: 40,
                    ),
                    activeIcon: Image.asset(
                      "assets/images/shopping-white.png",
                      width: 40,
                      height: 40,
                    ),
                    label: 'Shopping'),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      //"assets/images/scan.png",
                      "assets/images/tcard_logo.png",
                      width: 40,
                      height: 40,
                    ),
                    activeIcon: Image.asset(
                      //"assets/images/scan-white.png",
                      "assets/images/tcard_logo.png",
                      width: 40,
                      height: 40,
                    ),

                    // label: 'Points'
                    label: 'TCARD'),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/merchant.png",
                      width: 40,
                      height: 40,
                    ),
                    activeIcon: Image.asset(
                      "assets/images/merchant-white.png",
                      width: 40,
                      height: 40,
                    ),
                    label: 'Merchant'),
                BottomNavigationBarItem(
                    icon: Image.asset(
                      "assets/images/profile.png",
                      width: 40,
                      height: 40,
                    ),
                    activeIcon: Image.asset(
                      "assets/images/profile-white.png",
                      width: 40,
                      height: 40,
                    ),
                    label: 'Wallet')
                //       "assets/images/home-white.png"))
              ],
            )));
  }

  void showAlert(BuildContext context, dialogimage) {
    showDialog(
        context: context,
        builder: (context) => DialogImage(
              image: dialogimage.image,
              url: dialogimage.url,
              productid: dialogimage.productId,
            ));
  }
}
