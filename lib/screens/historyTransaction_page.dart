import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/historyTransaction.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/history_items.dart';

class HistoryTransactionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HistoryTransactionPageState();
  }
}

class HistoryTransactionPageState extends State<HistoryTransactionPage> {
  //List<HistoryTransaction> history = [];
  // @override
  // void initState() {
  //   super.initState();
  //   PostData().getHistory('Transaction').then((value) {
  //     setState(() {
  //       history = value;
  //     });
  //   });
  // }

  // final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  //     GlobalKey<RefreshIndicatorState>();
  // Future<Null> _handleRefresh() async {
  //   setState(() {
  //     PostData().getHistory('Transaction').then((value) {
  //       setState(() {
  //         history = value;
  //       });
  //     });
  //   });
  // }
  final PagingController<int, HistoryTransaction> _pagingController =
      PagingController(firstPageKey: 1);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      // PostData().getProductApi(pageKey).then((value) {
      //   newItems = value.merchant;
      // });
      PostData().getHistory('Transaction', pageKey).then((value) {
        final isLastPage = value.length == 0;

        if (isLastPage) {
          _pagingController.appendLastPage(value);
        } else {
          final nextPageKey = pageKey + 1;
          _pagingController.appendPage(value, nextPageKey);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'History',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: FutureBuilder(
            future: PostData().getHistory('Transaction', 1),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Theme(
                    data: Theme.of(context)
                        .copyWith(accentColor: Mycolor.darkGreen),
                    child: RefreshIndicator(
                        onRefresh: () => Future.sync(
                              // 2
                              () => _pagingController.refresh(),
                            ),
                        child: PagedListView<int, HistoryTransaction>(
                          //padding: EdgeInsets.only(right: 20),
                          physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          pagingController: _pagingController,
                          builderDelegate:
                              PagedChildBuilderDelegate<HistoryTransaction>(
                                  itemBuilder: (context, item, index) {
                            List<String> imgList =
                                item.images.split(',').map((e) => e).toList();
                            String showDisplay;

                            if (item.productType != 'MERCHANT' &&
                                item.productType != 'SHOPPING') {
                              if (item.status == 'USED') {
                                showDisplay = 'Used E-Cash';
                              } else {
                                showDisplay = 'Expired E-Cash';
                              }
                            } else {
                              if (item.displayPrice != '0') {
                                if (item.discountType == 'NONE') {
                                  showDisplay = item.displayPrice;
                                } else if (item.discountType == 'FLAT') {
                                  showDisplay = item.discountText;
                                } else if (item.discountType == 'PERCENT') {
                                  showDisplay = item.discountText;
                                } else if (item.discountType == 'NOSHOW') {
                                  showDisplay = '';
                                }
                              } else if (item.displayPrice == '0') {
                                if (item.discountType == 'NONE') {
                                  showDisplay = 'FREE';
                                } else if (item.discountType == 'NOSHOW') {
                                  showDisplay = '';
                                } else {
                                  showDisplay = 'FREE';
                                }
                              }
                            }

                            print(item.displayPrice);
                            return HistoryItem(
                              showDisplay: showDisplay,
                              displayPrice: item.displayPrice,
                              discountType: item.discountType,
                              discountText: item.discountText,
                              image: imgList[0],
                              index: index,
                              title: item.title,
                              date: item.productType == 'MERCHANT' ||
                                      item.productType == 'SHOPPING'
                                  ? DateFormat('dd/MM/yyyy hh:mm a')
                                      .format(item.transactionDate)
                                  : item.status == 'USED'
                                      ? DateFormat('dd/MM/yyyy hh:mm a')
                                          .format(item.transactionDate)
                                      : DateFormat('dd/MM/yyyy hh:mm a')
                                          .format(item.expiryDate),
                              walletype: null,
                              productType: item.productType == 'MERCHANT'
                                  ? 'PRODUCT'
                                  : item.productType == 'SHOPPING'
                                      ? item.productType
                                      : 'E-Cash',
                            );
                          }),
                        )));
              } else if (snapshot.hasError) {
                if (PostData.update) {
                  return DialogUpdate();
                } else if (PostData.statusCode == 403) {
                  return DialogRelogin();
                } else {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    title: Column(children: [
                      Text(
                        'Connection Error',
                        style: TextStyle(
                            color: Mycolor.darkGreen,
                            fontSize: 24,
                            fontWeight: FontWeight.w900),
                      )
                    ]),
                  );
                }
              } else {
                return LoadingOverlay(
                    color: Colors.grey,
                    progressIndicator: tcardProgressDialog(),
                    child: Center(),
                    isLoading: true);
              }
            })

        // RefreshIndicator(
        //   key: _refreshIndicatorKey,
        //   onRefresh: _handleRefresh,
        //   child: ListView(
        //     children: [
        //       Container(
        //         padding: EdgeInsets.all(10),
        //         width: MediaQuery.of(context).size.width,
        //         // decoration:
        //         //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        //         child: Container(
        //           padding: EdgeInsets.all(10),
        //           width: MediaQuery.of(context).size.width,
        //           // decoration:
        //           //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        //           child: Text(
        //             'History',
        //             textAlign: TextAlign.left,
        //             style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        //           ),
        //         ),
        //       ),
        //       Column(
        //           children: history
        //               .map((e) => HistoryItem(
        //                     title: e.productTitle,
        //                     amount: e.amountCharged,
        //                     date: DateFormat('dd/MM/yyyy hh:mm a')
        //                         .format(e.transactionDate),
        //                     walletype: null,
        //                   ))
        //               .toList())
        //     ],
        //   ),
        // )
        );
  }
}
