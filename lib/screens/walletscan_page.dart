import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/utils/mycolor.dart';

import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:url_launcher/url_launcher.dart';

class WalletScanPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WalletScanPageState();
  }
}

class WalletScanPageState extends State<WalletScanPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController controller;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Scan',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: Column(children: [
          Expanded(
            child: QRView(
              overlay: QrScannerOverlayShape(
                borderColor: Mycolor.darkGreen,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300,
              ),
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          // FlatButton(
          //     onPressed: () {
          //       PostData().getRedeem(id.toString(), mode, "IDOSKI").then(
          //           (value) => showDialog(
          //               context: context,
          //               builder: (context) => DialogMessage(
          //                   value.data['status'], value.data['message'])));
          //     },
          //     child: Text('test'))
        ]));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      controller.pauseCamera();
      setState(() {
        qrText = scanData;
        //Navigator.pushNamed(context, '/voucher');
      });
      print(scanData);
      var validateqr = scanData.split('://');
      if (validateqr[0] == "tcardclaimreceipt") {
        // print(validateqr[1]);
        //var receiptdata = validateqr[1].split('#');

        PostData().claimReceipt(scanData).then((value) => showDialog(
            context: context,
            builder: (context) => DialogMessage(
                value.data['status'], value.data['message'], true, '1')));
        // controller.resumeCamera();
      } else if (validateqr[0] == "tcardurl") {
        String url = validateqr[1];

        if (await canLaunch(url)) {
          await launch(url);
        } else {
          throw 'Could not launch $url';
        }
      } else {
        showDialog(
            context: context,
            builder: (context) =>
                DialogMessage("Try Again", "Invalid QR Code", false, '1'));
        //controller.resumeCamera();
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
