import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';
import 'package:provider/provider.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/Notification.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/services/huawei_notification_service.dart';
import 'package:tcard/services/notificationService.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:tcard/components/logo.dart';
import 'package:tcard/config/postData.dart';

class OtpVerification extends StatelessWidget {
  OtpVerification({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "Montserrat",
        primaryColor: Mycolor.darkGreen,
        primaryColorDark: Mycolor.darkGreen,
      ),
      home: OtpVerificationPage(),
    );
  }
}

class OtpVerificationPage extends StatefulWidget {
  final data;

  OtpVerificationPage({Key key, this.data}) : super(key: key);

  @override
  _OtpVerificationState createState() => _OtpVerificationState(data: data);
}

class _OtpVerificationState extends State<OtpVerificationPage> {
  final data;

  _OtpVerificationState({this.data});

  @override
  Widget build(BuildContext context) {
    var platform = Theme.of(context).platform;
    // @override
    // void initState() {
    //   super.initState();
    //     PostData().callOtpAPI(data['phone']);

    // }
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/background.png'),
                  fit: BoxFit.cover)),
          child: Column(
            children: [
              Row(
                children: [
                  ReturnButton(),
                ],
              ),
              Expanded(
                child: Column(
                  //mainAxisSize: MainAxisSize.min,
                  children: [
                    Logo(),
                    Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 10.0),
                                child: Text(
                                  "OTP Verification ",
                                  style: TextStyle(
                                      color: Mycolor.darkGreen, fontSize: 24),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        top: 20.0, left: 10, right: 10),
                                    child: Text(
                                        "A SMS has been sent to your number at 010-XXX XXXX",
                                        style: TextStyle(
                                            color: Mycolor.darkGreen,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300)),
                                  ),
                                ),
                              )
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context, '/signup');
                            },
                            child: Row(
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Container(
                                      margin: EdgeInsets.only(top: 10.0),
                                      child: Text("Wrong Number?",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: Mycolor.urlBlue,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300)),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.9,
                                margin:
                                    EdgeInsets.only(top: 20.0, bottom: 20.0),
                                child: Theme(
                                  data: ThemeData(
                                    inputDecorationTheme: InputDecorationTheme(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Mycolor.darkGreen,
                                        ),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Mycolor.darkGreen,
                                        ),
                                      ),
                                    ),
                                  ),
                                  child: OTPTextField(
                                    length: 6,
                                    width: MediaQuery.of(context).size.width,
                                    fieldWidth: 50,
                                    style: TextStyle(fontSize: 18),
                                    textFieldAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    fieldStyle: FieldStyle.box,
                                    onCompleted: (pin) {
                                      // showDialog(
                                      //     context: context,
                                      //     builder: (context) =>
                                      //         tcardProgressDialog());
                                      data['vCode'] = pin;
                                      var response =
                                          PostData().callRegisterAPI(data);
                                      response.then((value) {
                                        print(value);
                                        if (value.statusCode == 200) {
                                          var dbHelper = DBHelper();
                                          String message =
                                              "Thank you for signing up with tcard!\nYour tcard account have been created successfully!\nYou'll be the first to hear about all of our latest arrivals, new services, and exclusive promotions!\n\n\nBest Regards,\ntcard App Team";
                                          Notifications notification = Notifications(
                                              null,
                                              "tcard account created successfully','Dear  ${data['name']}",
                                              'https://via.placeholder.com/50x50',
                                              "sd",
                                              message,
                                              null,
                                              null,
                                              'NORMAL',
                                              'unseen',
                                              DateTime.now().toString());
                                          dbHelper.save(notification);
                                          PostData()
                                              .callLoginAPI(
                                                  data['phone'], data['vCode'])
                                              .then((value) {
                                            print(value.data);
                                            FlutterHmsGmsAvailability
                                                .isGmsAvailable
                                                .then((google) {
                                              final p = Provider.of<
                                                      NotificationState>(
                                                  context,
                                                  listen: false);

                                              if (google || Platform.isIOS) {
                                                final n = NotificationService();

                                                n.fcm();
                                                n.addListener(() {
                                                  p.add();
                                                });
                                              } else {
                                                FlutterHmsGmsAvailability
                                                    .isHmsAvailable
                                                    .then((huawei) {
                                                  if (huawei) {
                                                    final huawei =
                                                        HuaweiNotification();
                                                    huawei.getToken();
                                                    huawei.addListener(() {
                                                      p.add();
                                                    });
                                                  }
                                                });
                                              }
                                            });

                                            Navigator.pushReplacementNamed(
                                                context, '/main');
                                          });
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (context) =>
                                                  DialogMessage(
                                                      'Try Again',
                                                      value.data['message'],
                                                      false,
                                                      '1'));
                                        }
                                      });
                                      //rint('Completed: ' + pin);
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  child: Center(
                                    child: Text("Didn't Receive?",
                                        style: TextStyle(
                                            color: Mycolor.darkGreen,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300)),
                                  ),
                                ),
                              )
                            ],
                          ),
                          InkWell(
                              onTap: () {
                                var response =
                                    PostData().callOtpAPI(data['phone']);
                                response.then((value) {
                                  print('platform' + platform.toString());
                                  if (value.statusCode == 200) {
                                  } else {
                                    showDialog(
                                        context: context,
                                        builder: (context) => DialogMessage(
                                            'Verification Code',
                                            value.data['message'],
                                            false,
                                            '1'));
                                  }
                                });
                              },
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Center(
                                      child: Text("Resend",
                                          style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: Mycolor.urlBlue,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300)),
                                    ),
                                  )
                                ],
                              ))
                        ])),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
