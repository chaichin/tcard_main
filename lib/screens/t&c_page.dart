import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:io';

class Term extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TermState();
  }
}

class TermState extends State<Term> {
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Terms & Conditions',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: WebView(
          initialUrl: 'https://tcardmy.com/terms/',
          javascriptMode: JavascriptMode.unrestricted,
        ));
  }
}
