import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/location_items.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/branches.dart';
import 'package:tcard/utils/mycolor.dart';

class BranchesPage extends StatefulWidget {
  final productid;
  final location;
  BranchesPage({this.productid, this.location});
  @override
  State<StatefulWidget> createState() {
    return BranchesPageState(productid: productid, location: location);
  }
}

class BranchesPageState extends State<BranchesPage> {
  final productid;
  final location;
  BranchesPageState({this.productid, this.location});
  List<Allbranches> branches = [];
  @override
  void initState() {
    PostData().getBranches(productid, location).then((value) => setState(() {
          branches = value;
        }));
    super.initState();
  }

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    setState(() {
      PostData().getBranches(productid, location).then((value) => setState(() {
            branches = value;
          }));
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Check outlets',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: Theme(
          data: Theme.of(context).copyWith(accentColor: Mycolor.darkGreen),
          child: RefreshIndicator(
              key: _refreshIndicatorKey,
              onRefresh: _handleRefresh,
              child: ListView(
                children: [
                  Container(
                    // alignment: Alignment.center,
                    color: Mycolor.grey,
                    padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    // decoration:
                    //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),

                    child: Text(
                      'Outlets that are available',
                      // 'All Locations (${branches.length})',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Mycolor.darkgrey,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Column(
                      children: branches
                          .map((branch) => LocationItem(
                                branch: branch,
                              ))
                          .toList())
                ],
              )),
        ));
  }
}
