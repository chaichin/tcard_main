import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tcard/components/notification_items.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/Notification.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/back_buttons.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationPageState();
  }
}

class NotificationPageState extends State<NotificationPage> {
  Future<List<Notifications>> notifications;
  var dbHelper;
  @override
  void initState() {
    dbHelper = DBHelper();
    super.initState();
    final p = Provider.of<NotificationState>(context, listen: false);

    setState(() {
      notifications = dbHelper.getNotifications();
    });
    dbHelper.addListener(() {
      p.decrease();
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(
            push: 'replace',
          ),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Notification',
              //merchantName,
              style: TextStyle(
                  fontSize: 24,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: FutureBuilder(
            future: notifications,
            builder: (context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData) {
                List<Notifications> notification = snapshot.data;
                //notification.map((e) => print(e.day));
                //  print(notification);
                return ListView(
                  children: [
                    Container(
                      // alignment: Alignment.center,
                      color: Mycolor.grey,
                      padding: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      // decoration:
                      //     BoxDecoration(border: Border.all(color: Colors.blueAccent)),

                      child: Text(
                        'Notifications',
                        // 'All Locations (${branches.length})',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Mycolor.darkgrey,
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Column(
                      //  textDirection: TextD,
                      children: notification
                          .map((e) => NotificationItem(
                              image: e.type == 'PRODUCT' || e.type == 'VOUCHER'
                                  ? 'noti-teeth'
                                  : 'noti-event',
                              value: e.value,
                              type: e.type,
                              url: e.url,
                              productid: e.productid,
                              voucherid: e.voucherid,
                              hours: DateTime.now()
                                  .difference(DateTime.parse(e.day))
                                  .inHours,
                              minutes: DateTime.now()
                                  .difference(DateTime.parse(e.day))
                                  .inMinutes,
                              days: DateTime.now()
                                  .difference(DateTime.parse(e.day))
                                  .inDays))
                          .toList()
                          .reversed
                          .toList(),
                    )
                  ],
                );
              } else {
                return Container();
              }
            }));
  }
}
