import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/components/logo.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialogs.dart';

class ResetPage extends StatelessWidget {
  final phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Builder(
          builder: (context) => SafeArea(
              child: Container(
            decoration: BoxDecoration(color: Mycolor.white),
            child: Stack(
              children: [
                Image.asset(
                  'assets/images/background.png',
                ),
                Row(
                  children: [
                    ReturnButton(),
                  ],
                ),
                Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 120),
                    child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Mycolor.normalWhite,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Logo(),
                              Container(
                                alignment: Alignment.topLeft,
                                margin: EdgeInsets.only(top: 0, left: 20),
                                child: Text(
                                  'Please enter your phone number',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Mycolor.darkGreen),
                                ),
                              ),
                              Form(
                                  key: _formKey,
                                  child: Input(
                                    value: 'Phone Number',
                                    textArea: 1,
                                    textController: phoneController,
                                    keypad: true,
                                    validate: (value) {
                                      if (value.isEmpty) {
                                        return 'Phone Number field is required*';
                                      }
                                      return null;
                                    },
                                    onSaved: (value) {
                                      phoneController.text = value;
                                    },
                                  )),
                              Center(
                                  child: Container(
                                margin: EdgeInsets.only(top: 40),
                                child: Button('OK', () {
                                  //Proceed call API
                                  // if (phoneController.text.isEmpty) {
                                  //   showDialog(
                                  //       context: context,
                                  //       builder: (context) => DialogMessage(
                                  //           'Try Again',
                                  //           'Phone Number field is required'));
                                  // } else
                                  if (_formKey.currentState.validate()) {
                                    showDialog(
                                        context: context,
                                        builder: (context) => LoadingOverlay(
                                            color: Colors.grey,
                                            progressIndicator:
                                                tcardProgressDialog(),
                                            child: Center(),
                                            isLoading: true));

                                    PostData()
                                        .callResetAPI(phoneController.text)
                                        .then((value) {
                                      if (value.statusCode == 200) {
                                        Navigator.pop(context);
                                        showDialog(
                                            context: context,
                                            builder: (context) => DialogMessage(
                                                'Reset Successful',
                                                value.data['message'],
                                                false,
                                                '1'));
                                      } else {
                                        Navigator.pop(context);
                                        showDialog(
                                            context: context,
                                            builder: (context) => DialogMessage(
                                                'Try Again',
                                                value.data['message'],
                                                false,
                                                '1'));
                                      }
                                    });
                                  }
                                }),
                              )),
                            ]))),
              ],
            ),
          )),
        ));
  }
}
