import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/appointment_datepicker.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/datepicker.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/dropdown.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/appointment.dart';
import 'package:tcard/services/getUserService.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/components/back_buttons.dart';

class AppointmentPage extends StatefulWidget {
  final merchantname;
  final logo;
  AppointmentPage({this.merchantname, this.logo});
  @override
  State<StatefulWidget> createState() {
    return AppointmentPageState();
  }
}

class AppointmentPageState extends State<AppointmentPage> {
  String preference;
  String doctor;
  String clinic;
  String doctorSlot;
  String clinicSlot;
  String id;
  //String  availableSlot;
  String preferedSlot;
  final userController = TextEditingController();
  TextEditingController _dateCtrl = TextEditingController();
  TextEditingController _phoneCtrl = TextEditingController();
  TextEditingController _emailCtrl = TextEditingController();
  TextEditingController _messageCtrl = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  // List<String> doctorList = ['Choose your Doctor'];
  // List<String> clinicList = ['Choose your Clinic'];
  // List<String> initialdoctorSlot = ['Available Slot'];
  // List<String> initialclinicSlot = ['Available Slot'];

  // @override
  // void initState() {
  //   PostData().getAppointmentDetails().then((value) {
  //     clinicList.addAll(
  //         value.clinics.map((e) => e.branchName + " - " + e.clinicName));
  //     doctorList.addAll(value.doctors.map((e) => e.doctorName));
  //     value.clinics.map((e) {
  //       if (e.clinicName == clinic) {
  //         initialclinicSlot.addAll(e.workingHours);
  //       }
  //     });
  //     value.doctors.map((e) {
  //       if (e.doctorName == doctor) {
  //         initialdoctorSlot.addAll(e.appoinment);
  //       }
  //     });
  //   });

  //   super.initState();
  // }
  @override
  void initState() {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    super.initState();

    if (PostData.logintoken != null) {
      User().profile().then((value) => setState(() {
            userController.text = value[1];
            _phoneCtrl.text = value[2];
            _emailCtrl.text = value[3];
          }));
      Future.delayed(Duration.zero, () {
        showAlert(context);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    //print('pref ' + preference);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Mycolor.white,
          leading: ReturnButton(),
          title: Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              'Make an appointment',
              //merchantName,
              style: TextStyle(
                  fontSize: 30,
                  color: Mycolor.darkerGreen,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        body: FutureBuilder(
            future: PostData().getAppointmentDetails(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<String> doctorList = ['Choose your Doctor*'];
                List<String> clinicList = ['Choose your Clinic*'];
                List<String> initialdoctorSlot = ['Available Slot*'];
                List<String> initialclinicSlot = ['Available Slot*'];

                //List<String> doctorArray = [];
                List<Clinic> clinics = snapshot.data.clinics;
                List<Doctor> doctors = snapshot.data.doctors;
                // for (var i = 0; i < doctors.length; i++) {
                //   if (doctors[i].doctorName == doctor) {
                //     doctorArray = initialdoctorSlot + doctors[i].appoinment;
                //   }
                // }
                clinicList.addAll(
                    clinics.map((e) => e.branchName + " - " + e.clinicName));
                doctorList.addAll(doctors.map((e) => e.doctorName));
                // initialdoctorSlot.addAll(doctors.map((e) {
                //   if (e.doctorName == doctor) {
                //     return e.appoinment;
                //   }
                // }));
                // doctors.map((e) {
                //   //if (e.doctorName == doctorSlot) {
                //   initialdoctorSlot.addAll(e.appoinment);
                //   // }
                // });

                return Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      widget.merchantname != null && widget.logo != null
                          ? Padding(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              child: Stack(
                                children: [
                                  Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      margin: EdgeInsets.only(
                                          left: 20, right: 20, top: 10),
                                      child: Container(
                                        height: 40,
                                        decoration: BoxDecoration(),
                                      )),
                                  Container(
                                    margin: EdgeInsets.symmetric(horizontal: 0),
                                    padding: EdgeInsets.only(left: 40),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 60,
                                          height: 60,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image:
                                                    NetworkImage(widget.logo)),
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(left: 10),
                                          alignment: Alignment.centerLeft,
                                          decoration: BoxDecoration(),
                                          width: 180,
                                          height: 40,
                                          child: Text(
                                            widget.merchantname,
                                            textAlign: TextAlign.left,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14,
                                                color: Mycolor.darkgrey),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: DropDown(
                          //value: 'Your Preference',
                          list: ["Your Preference*", "Clinic", "Doctor"],
                          text: preference,
                          onsaved: (value) {
                            preference = value;
                          },
                          handleChange: (text) {
                            setState(() {
                              preference = text;
                            });
                          },
                          validate: (value) {
                            if (value == null) {
                              return 'Your Preference field is required*';
                            }
                            return null;
                          },
                        ),
                      ),

                      preference == 'Clinic'
                          ? Padding(
                              padding: EdgeInsets.only(bottom: 10),
                              child: DropDown(
                                // value: 'Choose your Clinic',
                                list: clinicList,
                                text: clinic,
                                onsaved: (value) {
                                  clinic = value;
                                },
                                handleChange: (text) {
                                  setState(() {
                                    clinic = text;
                                    clinicSlot = initialclinicSlot[0];
                                  });
                                },
                                validate: (value) {
                                  if (value == null) {
                                    return 'Clinic field is required*';
                                  }
                                  return null;
                                },
                              ))
                          : preference == 'Doctor'
                              ? Padding(
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: DropDown(
                                    //value: 'Choose your Doctor',
                                    list: doctorList,
                                    text: doctor,

                                    handleChange: (text) {
                                      setState(() {
                                        doctor = text;
                                        doctorSlot = initialdoctorSlot[0];
                                      });
                                    },
                                    onsaved: (value) {
                                      doctor = value;
                                    },
                                    validate: (value) {
                                      if (value == null) {
                                        return 'Doctor field is required*';
                                      }
                                      return null;
                                    },
                                  ))
                              : Container(),
                      preference == 'Doctor'
                          ? Padding(
                              padding: EdgeInsets.only(bottom: 10),
                              child: DropDown(
                                //value: 'Available Slot',
                                list: initialdoctorSlot +
                                    // doctors.reduce((value, element) =>element.appoinment),
                                    doctors
                                        .where((value) =>
                                            value.doctorName == doctor)
                                        .fold([], (previousValue, element) {
                                      {
                                        return element.appoinment;
                                      }
                                    }),
                                text: doctorSlot,
                                onsaved: (value) {
                                  doctorSlot = value;
                                },
                                handleChange: (text) {
                                  setState(() {
                                    doctorSlot = text;
                                    print('ss' + doctorSlot);
                                  });
                                },
                                validate: (value) {
                                  if (value == null) {
                                    return 'Available slot field is required*';
                                  }
                                  return null;
                                },
                              ))
                          : preference == 'Clinic'
                              ? Padding(
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: DropDown(
                                    list: initialclinicSlot +
                                        // doctors.reduce((value, element) =>element.appoinment),
                                        clinics
                                            .where((value) =>
                                                value.branchName +
                                                    " - " +
                                                    value.clinicName ==
                                                clinic)
                                            .fold([], (previousValue, element) {
                                          {
                                            return element.workingHours;
                                          }
                                        }),
                                    text: clinicSlot,
                                    onsaved: (value) {
                                      clinicSlot = value;
                                    },
                                    handleChange: (text) {
                                      setState(() {
                                        clinicSlot = text;
                                        print('ss' + clinicSlot);
                                      });
                                    },
                                    validate: (value) {
                                      if (value == null) {
                                        return 'Available slot field is required*';
                                      }
                                      return null;
                                    },
                                  ))
                              : Container(),
                      // preference == 'Clinic'
                      //     ? DropDown(
                      //         //value: 'Available Slot',
                      //         list: initialdoctorSlot +
                      //             // doctors.reduce((value, element) =>element.appoinment),
                      //             clinics
                      //                 .where((value) =>
                      //                     value.branchName +
                      //                         " - " +
                      //                         value.clinicName ==
                      //                     clinic)
                      //                 .fold([], (previousValue, element) {
                      //               {
                      //                 return element.workingHours;
                      //               }
                      //             }),
                      //         text: doctorSlot,
                      //         handleChange: (text) {
                      //           setState(() {
                      //             doctorSlot = text;
                      //             print('ss' + doctorSlot);
                      //           });
                      //         },
                      //       )
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(),
                                AppointmentDatePickerInput(
                                  label: 'Date*',
                                  controller: _dateCtrl,
                                  min: true,
                                  onsaved: (value) {
                                    _dateCtrl = value;
                                  },
                                  validate: (value) {
                                    if (value.isEmpty) {
                                      return 'Date field is required*';
                                    }
                                    return null;
                                  },
                                ),
                              ])),
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: DropDown(
                            list: [
                              'Preferred Time*',
                              'Morning Slot',
                              'Afternoon Slot'
                            ],
                            onsaved: (value) {
                              preferedSlot = value;
                            },
                            text: preferedSlot,
                            handleChange: (text) {
                              setState(() {
                                preferedSlot = text == 'Preferred Time*'
                                    ? text.isEmpty
                                    : text;
                                print('ss' + preferedSlot);
                              });
                            },
                            validate: (value) {
                              if (value == null) {
                                return 'Preferred Time field is required*';
                              }
                              return null;
                            },
                          )),
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Input(
                            value: 'Your Name*',
                            textArea: 1,
                            textController: userController,
                            //formkey: userKey,
                            validate: (value) {
                              if (value.isEmpty) {
                                return 'Name field is required*';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              userController.text = value;
                            },
                          )),
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Input(
                            value: 'Phone Number* (Example: 012-345 6789)',
                            textArea: 1,
                            textController: _phoneCtrl,
                            keypad: true,
                            validate: (value) {
                              if (value.isEmpty) {
                                return 'Phone Number field is required*';
                              } else if (checkphoneregex(value) == false) {
                                return 'Invalid Malaysia number/Singapore number';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _phoneCtrl.text = value;
                            },
                          )),
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Input(
                            value: 'Email',
                            textArea: 1,
                            textController: _emailCtrl,
                            validate: (value) {
                              if (value.isEmpty) {
                                return 'Email field is required*';
                              } else if (!RegExp(
                                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(value)) {
                                return 'Email format is wrong';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              _emailCtrl.text = value;
                            },
                          )),
                      Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Input(
                            value:
                                'Message (Please key in your required treatment as well) if applicable',
                            textArea: 5,
                            textController: _messageCtrl,
                            // validate: (value) {
                            //   if (value.isEmpty) {
                            //     return 'Message field is required*';
                            //   }
                            //   return null;
                            // },
                            // onSaved: (value) {
                            //   _messageCtrl.text = value;
                            // },
                          )),
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Row(
                            //crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Button('Confirm', () {
                                String id = preference == "Clinic"
                                    ? clinics
                                        .where((value) =>
                                            value.branchName +
                                                " - " +
                                                value.clinicName ==
                                            clinic)
                                        .fold('', (previousValue, element) {
                                        {
                                          return element.clinicId;
                                        }
                                      })
                                    : doctors
                                        .where((value) =>
                                            value.doctorName == doctor)
                                        .fold('', (previousValue, element) {
                                        {
                                          return element.id;
                                        }
                                      });
                                var availableSlot = preference == "Clinic"
                                    ? clinicSlot
                                    : doctorSlot;
                                print(userController.text);
                                print(_dateCtrl.text);
                                print(_emailCtrl.text);
                                print(_phoneCtrl.text);
                                print(_messageCtrl.text);
                                print(availableSlot);
                                print(preferedSlot);
                                // print(availableSlot +
                                //     "\n" +
                                //     "(preferred time : " +
                                //     _dateCtrl.text +
                                //     " - " +
                                //     preferedSlot +
                                //     ")");
                                print('id' + id);

                                if (_formKey.currentState.validate()) {
                                  showDialog(
                                      context: context,
                                      builder: (context) => LoadingOverlay(
                                          color: Colors.grey,
                                          progressIndicator:
                                              tcardProgressDialog(),
                                          child: Center(),
                                          isLoading: true));
                                  PostData()
                                      .submitAppointment(
                                          userController.text,
                                          _phoneCtrl.text,
                                          _emailCtrl.text,
                                          preference,
                                          id,
                                          availableSlot +
                                              "\n" +
                                              "(preferred time : " +
                                              _dateCtrl.text +
                                              " - " +
                                              preferedSlot +
                                              ")",
                                          _messageCtrl.text)
                                      .then((value) {
                                    if (value.statusCode == 200) {
                                      Navigator.pop(context);
                                      showDialog(
                                          context: context,
                                          builder: (context) => DialogMessage(
                                              'Successful',
                                              'Thanks for your appointment request, our team will contact you shortly.',
                                              true,
                                              1));
                                      // userController.text = '';
                                      // _phoneCtrl.text = '';
                                      // _emailCtrl.text = '';

                                      id = '';
                                      //availableSlot = '';
                                      _dateCtrl.text = '';
                                      _messageCtrl.text = '';
                                      setState(() {
                                        preference = 'Your Preference*';
                                        //availableSlot = 'Available Slot*';
                                        preferedSlot = 'Preferred Time*';
                                        clinicSlot = initialclinicSlot[0];

                                        doctorSlot = initialdoctorSlot[0];
                                        clinic = clinicList[0];
                                        doctor = doctorList[0];
                                      });
                                    } else {
                                      Navigator.pop(context);
                                      showDialog(
                                          context: context,
                                          builder: (context) => DialogMessage(
                                              'Fail',
                                              'Fail to make appointment',
                                              false,
                                              1));
                                    }
                                  });
                                }
                              }),
                            ],
                          ))
                    ],
                  )),
                );
              } else {
                return LoadingOverlay(
                    color: Colors.grey,
                    progressIndicator: tcardProgressDialog(),
                    child: Center(),
                    isLoading: true);
              }
            }));
  }

  bool checkphoneregex(String phone) {
    if (RegExp(r"^01\d{8,9}$").hasMatch(phone)) {
      return true;
    } else if (RegExp(r"^(65)(6|8|9)\d{7}$").hasMatch(phone)) {
      return true;
    } else {
      return false;
    }
  }

  void showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              // backgroundColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              title: Text(
                "Terms & Conditions",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Mycolor.darkGreen),
              ),
              content: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                        'Thank you for getting in touch!\n\nYou may choose your preference Doctor/Clinic.\nOur team will contact you to confirm your dental appoinment submission.\n'),
                    Text(
                      'IMPORTANT!!',
                      style: TextStyle(
                          color: Colors.red, fontWeight: FontWeight.bold),
                    ),
                    Text(
                        '-Please be noted that this request not confirmed untill you receive a call/message from our team to assist for the final arrangement. We gratefully appreciate your patience.\n'),
                    Button('OK', () {
                      Navigator.pop(context);
                    })
                  ]),
            ));
  }
}
