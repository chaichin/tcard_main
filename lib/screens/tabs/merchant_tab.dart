import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shine/flutter_shine.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/merchant_items.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/categories.dart';
import 'package:tcard/screens/merchant_page.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/utils/mycolor.dart';

class MerchantTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MerchantTabState();
  }
}

class MerchantTabState extends State<MerchantTab>
    with AutomaticKeepAliveClientMixin {
  Future<List<Categories>> _merchant;
//   List<Categories> categories = [];

//   @override
//   void initState() {
//     super.initState();
//     PostData().getMerchantApi().then((categories) {
//       setState(() {
//         this.categories = categories;

//       });
//     });
//   }
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    setState(() {
      _merchant = PostData().getMerchantApi();
    });
  }

  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    _merchant = PostData().getMerchantApi();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.build(context);
    return FlutterShine(
        config: Config(numSteps: 2, opacity: 0.5, offset: 0.1),
        builder: (BuildContext context, ShineShadow shineShadow) {
          return FutureBuilder(
              future: _merchant,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Categories> categories = snapshot.data;
                  return Theme(
                      data: Theme.of(context)
                          .copyWith(accentColor: Mycolor.darkGreen),
                      child: RefreshIndicator(
                          key: _refreshIndicatorKey,
                          onRefresh: _handleRefresh,
                          child: ListView.builder(
                              physics: const BouncingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              itemCount: 1,
                              itemBuilder: (context, index) {
                                return Column(children: [
                                  // Center(
                                  //     child: Container(
                                  //   margin: EdgeInsets.symmetric(vertical: 10),
                                  //   child: Text(categories[index].categoryName),
                                  // )),
                                  GridView.count(
                                    shrinkWrap: true,
                                    primary: false,
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 10,
                                    ),
                                    crossAxisSpacing: 20,
                                    mainAxisSpacing: 20,
                                    crossAxisCount: 3,
                                    children: categories
                                        .map((merchant) => Container(
                                            // padding: EdgeInsets.symmetric(
                                            //     horizontal: 10, vertical: 10),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(100)),
                                              boxShadow: shineShadow.boxShadows,
                                            ),
                                            child: MerchantItem(
                                              source: merchant.merchants.logo,
                                              navigation: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MerchantPage(
                                                              title: merchant
                                                                  .merchants
                                                                  .categoryName,
                                                              merchantname:
                                                                  merchant
                                                                      .merchants
                                                                      .name,
                                                              logo: merchant
                                                                  .merchants
                                                                  .logo,
                                                              merchantId: merchant
                                                                  .merchants
                                                                  .merchantDetailId,
                                                            )));
                                              },
                                            )))
                                        .toList(),
                                    //  categories[index]
                                    //     .merchants
                                    //     .map(
                                    //       (merchant) => Container(
                                    //           decoration: BoxDecoration(
                                    //               boxShadow:
                                    //                   shineShadow.boxShadows),
                                    //           child: MerchantItem(
                                    //             source: merchant.logo,
                                    //             navigation: () {
                                    //               Navigator.push(
                                    //                   context,
                                    //                   MaterialPageRoute(
                                    //                       builder: (context) =>
                                    //                           MerchantPage(
                                    //                             title: merchant
                                    //                                 .categoryName,
                                    //                             merchantname:
                                    //                                 merchant.name,
                                    //                             logo: merchant.logo,
                                    //                             merchantId: merchant
                                    //                                 .merchantDetailId,
                                    //                           )));
                                    //             },
                                    //           )),
                                    //     )
                                    //     .toList(),
                                  ),
                                ]);
                              })));
                } else if (snapshot.hasError) {
                  if (PostData.update) {
                    return DialogUpdate();
                  } else if (PostData.statusCode == 403) {
                    return DialogRelogin();
                  } else {
                    return AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      title: Column(children: [
                        Text(
                          'Connection Error',
                          style: TextStyle(
                              color: Mycolor.darkGreen,
                              fontSize: 24,
                              fontWeight: FontWeight.w900),
                        )
                      ]),
                    );
                  }
                } else {
                  return LoadingOverlay(
                      color: Colors.grey,
                      progressIndicator: tcardProgressDialog(),
                      child: Center(),
                      isLoading: true);
                  // return tcardProgressDialog();
                }
              });
        });
    // Center(
    //     child:ListView.builder(
    //         itemCount: this.categories.length,
    //         itemBuilder: (context, index) {
    //           return Column(children: [
    //             Center(
    //                 child: Container(
    //               margin: EdgeInsets.symmetric(vertical: 10),
    //               child: Text(categories[index].categoryName),
    //             )),
    //             GridView.count(
    //               shrinkWrap: true,
    //               primary: false,
    //               padding: const EdgeInsets.symmetric(horizontal: 20),
    //               crossAxisSpacing: 10,
    //               mainAxisSpacing: 10,
    //               crossAxisCount: 3,
    //               children: categories[index]
    //                   .merchants
    //                   .map((merchant) => MerchantItem(
    //                         source: merchant.logo,
    //                         navigation: () {
    //                           Navigator.push(
    //                               context,
    //                               MaterialPageRoute(
    //                                   builder: (context) => MerchantPage(
    //                                         title: merchant.categoryName,
    //                                         merchant: merchant.name,
    //                                         logo: merchant.logo,
    //                                         merchantId:
    //                                             merchant.merchantDetailId,
    //                                       )));
    //                         },
    //                       ))
    //                   .toList(),
    //             ),
    //           ]);
    //         }));
  }
}
