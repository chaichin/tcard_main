import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/carousel.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/miniSlider.dart';
import 'package:tcard/components/newSlider.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/newproduct.dart';
import 'package:tcard/utils/mycolor.dart';

class HomeTab extends StatefulWidget {
  final Function changeMerchant;
  final Function changeShopping;
  final Function changeSpecial;
  HomeTab({this.changeMerchant, this.changeShopping, this.changeSpecial});
  @override
  State<StatefulWidget> createState() {
    return HomeTabState();
  }
}

class HomeTabState extends State<HomeTab> with AutomaticKeepAliveClientMixin {
  Future<NewProduct> _products;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    setState(() {
      _products = PostData().getPageProductApi(1);
    });
  }

  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    _products = PostData().getPageProductApi(1);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.build(context);
    return FutureBuilder(
        future: _products,
        builder: (context, snapshot) {
          print(snapshot.hasError);
          // int _current = 0;
          if (snapshot.hasData) {
            NewProduct product = snapshot.data;
            List<Merchant> merchantList = product.merchant != null
                ? product.merchant.length != 0
                    ? product.merchant
                    : null
                : null;
            List<SpecialTreats> specialList = product.specialtreats != null
                ? product.specialtreats.length != 0
                    ? product.specialtreats
                    : null
                : null;
            List<Shopping> shoppingList = product.shopping != null
                ? product.shopping.length != 0
                    ? product.shopping
                    : null
                : null;
            List<BigCarousel> carouselList = product.carousel != null
                ? product.carousel.length != 0
                    ? product.carousel
                    : null
                : null;
            List<MiniCarousel> minicarouselList = product.minicarousel != null
                ? product.minicarousel.length != 0
                    ? product.minicarousel
                    : null
                : null;
            // print(merchantList[0].description);
            return Theme(
                data:
                    Theme.of(context).copyWith(accentColor: Mycolor.darkGreen),
                child: RefreshIndicator(
                    key: _refreshIndicatorKey,
                    onRefresh: _handleRefresh,
                    child: SingleChildScrollView(
                        child: Column(
                      children: [
                        Container(
                          // decoration: BoxDecoration(
                          //     border: Border.all(color: Mycolor.redOrange)),
                          height: 200,
                          child: Stack(
                            children: [
                              Container(
                                decoration:
                                    BoxDecoration(color: Mycolor.darkGreen),
                                height: 170,
                                width: MediaQuery.of(context).size.width,
                              ),
                              Container(
                                //height: 200,
                                margin: EdgeInsets.only(top: 20),
                                padding: EdgeInsets.only(left: 20, right: 20),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.transparent)),
                                // margin: EdgeInsets.symmetric(
                                //     horizontal: 20, vertical: 20),
                                child: Carousel(
                                  product: carouselList,
                                ),
                              ),
                            ],
                          ),
                        ),

                        minicarouselList != null
                            ? Column(children: [
                                Container(
                                    margin: EdgeInsets.only(
                                        left: 15, right: 15, top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      //crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "What's New",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Mycolor.darkerGreen,
                                              fontSize: 18),
                                        ),
                                        // Text(
                                        //   'MORE',
                                        //   style: TextStyle(
                                        //       fontWeight: FontWeight.w300,
                                        //       color: Mycolor.darkerGreen,
                                        //       fontSize: 8),
                                        // ),
                                      ],
                                    )),
                                Container(
                                    margin: EdgeInsets.only(bottom: 10),
                                    // decoration: BoxDecoration(
                                    //     border: Border.all(
                                    //         color: Mycolor.darkGreen)),
                                    height: 100,
                                    //color: Mycolor.white,
                                    child: ListView.builder(
                                        physics: const BouncingScrollPhysics(
                                            parent:
                                                AlwaysScrollableScrollPhysics()),
                                        itemCount: minicarouselList.length,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          return Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                MiniSlider(
                                                  title: minicarouselList[index]
                                                      .title,
                                                  button:
                                                      minicarouselList[index]
                                                          .buttonText,
                                                  image: minicarouselList[index]
                                                      .image,
                                                  description:
                                                      minicarouselList[index]
                                                          .description,
                                                  url: minicarouselList[index]
                                                      .url,
                                                  productid:
                                                      minicarouselList[index]
                                                          .productId,
                                                )
                                              ]);
                                        }))
                              ])
                            : Container(),

                        specialList != null
                            ? Column(
                                children: [
                                  PostData.panel
                                      ? Container(
                                          margin: EdgeInsets.only(
                                              left: 15, top: 0, right: 15),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            //crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Services',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Mycolor.darkerGreen,
                                                    fontSize: 18),
                                              ),
                                              InkWell(
                                                onTap: widget.changeSpecial,
                                                child: Text(
                                                  'MORE >',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color:
                                                          Mycolor.darkerGreen,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ))
                                      : Container(),
                                  PostData.panel
                                      ? Container(
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.transparent)),
                                          // margin: EdgeInsets.only(left: 30),
                                          height: 132,
                                          //color: Mycolor.white,
                                          child: ListView.builder(
                                              padding:
                                                  EdgeInsets.only(left: 15),
                                              physics: const BouncingScrollPhysics(
                                                  parent:
                                                      AlwaysScrollableScrollPhysics()),
                                              itemCount: specialList.length,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder: (context, index) {
                                                List<String> specialImages =
                                                    specialList[index]
                                                        .images
                                                        .split(',')
                                                        .map((e) => e)
                                                        .toList();
                                                String type = specialList[index]
                                                            .productType ==
                                                        'SHOPPING'
                                                    ? 'shopping'
                                                    : 'merchant';
                                                return Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 15),
                                                    child: NewSliderItem(
                                                        value: type,
                                                        flex: false,
                                                        source:
                                                            specialImages[0],
                                                        title:
                                                            specialList[index]
                                                                .title,
                                                        logo: specialList[index]
                                                            .merchantLogo,
                                                        productid:
                                                            specialList[index]
                                                                .productId,
                                                        views: specialList[
                                                                index]
                                                            .numberOfViews));
                                              }))
                                      : Container(),
                                ],
                              )
                            : Container(),

                        merchantList != null
                            ? Column(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(
                                          left: 15, top: 0, right: 15),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        //crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Product',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Mycolor.darkerGreen,
                                                fontSize: 18),
                                          ),
                                          InkWell(
                                            onTap: widget.changeMerchant,
                                            child: Text(
                                              'MORE >',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: Mycolor.darkerGreen,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.transparent)),
                                      // margin: EdgeInsets.only(top: 190),
                                      height: 132,
                                      //color: Mycolor.white,
                                      child: ListView.builder(
                                          padding: EdgeInsets.only(left: 15),
                                          physics: const BouncingScrollPhysics(
                                              parent:
                                                  AlwaysScrollableScrollPhysics()),
                                          itemCount: merchantList.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            List<String> merchantImages =
                                                merchantList[index]
                                                    .images
                                                    .split(',')
                                                    .map((e) => e)
                                                    .toList();

                                            return Padding(
                                                padding:
                                                    EdgeInsets.only(right: 15),
                                                child: NewSliderItem(
                                                    value: 'merchant',
                                                    flex: false,
                                                    source: merchantImages[0],
                                                    title: merchantList[index]
                                                        .title,
                                                    logo: merchantList[index]
                                                        .merchantLogo,
                                                    productid:
                                                        merchantList[index]
                                                            .productId,
                                                    views: merchantList[index]
                                                        .numberOfViews));
                                          })),
                                ],
                              )
                            : Container(),

                        //]),
                        shoppingList != null
                            ? Column(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(
                                          left: 15, top: 0, right: 15),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        //crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Shopping',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Mycolor.darkerGreen,
                                                fontSize: 18),
                                          ),
                                          InkWell(
                                            onTap: widget.changeShopping,
                                            child: Text(
                                              'MORE >',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: Mycolor.darkerGreen,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      )),
                                  Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Colors.transparent)),
                                      //margin: EdgeInsets.only(top: 10),
                                      height: 132,
                                      //color: Mycolor.white,
                                      child: ListView.builder(
                                          padding: EdgeInsets.only(left: 15),
                                          physics: const BouncingScrollPhysics(
                                              parent:
                                                  AlwaysScrollableScrollPhysics()),
                                          itemCount: shoppingList.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            List<String> shoppingImages =
                                                shoppingList[index]
                                                    .images
                                                    .split(',')
                                                    .map((e) => e)
                                                    .toList();
                                            return Padding(
                                                padding:
                                                    EdgeInsets.only(right: 15),
                                                child: NewSliderItem(
                                                  value: 'shopping',
                                                  flex: false,
                                                  source: shoppingImages[0],
                                                  title:
                                                      shoppingList[index].title,
                                                  logo: shoppingList[index]
                                                      .merchantLogo,
                                                  views: shoppingList[index]
                                                      .numberOfViews,
                                                  productid: shoppingList[index]
                                                      .productId,
                                                  price: shoppingList[index]
                                                      .sellingPrice,
                                                  points: shoppingList[index]
                                                      .pointsNeeded,
                                                ));
                                          })),
                                ],
                              )
                            : Container()
                      ],
                    ))));
          } else if (snapshot.hasError) {
            print('errpr' + snapshot.error);

            if (PostData.update) {
              return DialogUpdate();
            } else if (PostData.statusCode == 403) {
              return DialogRelogin();
            } else {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                title: Column(children: [
                  Text(
                    'Connection Error',
                    style: TextStyle(
                        color: Mycolor.darkGreen,
                        fontSize: 24,
                        fontWeight: FontWeight.w900),
                  )
                ]),
              );
            }
          } else {
            return LoadingOverlay(
                color: Colors.grey,
                progressIndicator: tcardProgressDialog(),
                child: Center(),
                isLoading: true);
          }
        });
  }
}
