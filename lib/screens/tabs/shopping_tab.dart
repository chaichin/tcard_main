import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/screens/tabs/productMerchant_tab.dart';
import 'package:tcard/screens/tabs/productShopping_tab.dart';
import 'package:tcard/screens/tabs/productSpecial_tab.dart';
import 'package:tcard/utils/mycolor.dart';

class ShoppingTab extends StatefulWidget {
  final int selectedTab;
  ShoppingTab({this.selectedTab});
  @override
  State<StatefulWidget> createState() {
    return ShoppingTabState(selectedTab: selectedTab);
  }
}

//List<Products> productsList;
class ShoppingTabState extends State<ShoppingTab>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  int selectedTab;
  ShoppingTabState({this.selectedTab});
  TabController _tabController;
  // List<Products> merchantsList;
  // List<Products> shoppingsList;

  //String panel = PostData.panel;

  @override
  void initState() {
    // User().profile().then((value) => setState(() {
    //       panel = value[5];
    //     }));
    //print(panel);
    _tabController = TabController(
        length: PostData.panel ? 3 : 2, vsync: this, initialIndex: selectedTab);

    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: FutureBuilder(
            future: PostData().getPageProductApi(1),
            //PostData().getProductApi(1),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Padding(
                  padding: const EdgeInsets.only(top: 0, left: 0, right: 0),
                  child: Column(
                    children: [
                      // give the tab bar a height [can change hheight to preferred height]
                      Container(
                        margin: _tabController.length == 3
                            ? const EdgeInsets.only(
                                top: 10, left: 40, right: 40, bottom: 10)
                            : const EdgeInsets.only(
                                top: 10, left: 60, right: 60, bottom: 10),
                        height: 35,
                        decoration: BoxDecoration(
                          color:
                              //Colors.grey[300],
                              Colors.white,
                          // border: Border.all(color: Mycolor.darkGreen),
                          borderRadius: BorderRadius.circular(
                            25.0,
                          ),
                        ),
                        child: TabBar(
                          controller: _tabController,
                          // give the indicator a decoration (color and border radius)
                          indicator: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              25.0,
                            ),
                            color: Mycolor.darkGreen,
                          ),
                          labelColor: Colors.white,
                          unselectedLabelColor: Mycolor.darkerGreen,
                          tabs: PostData.panel
                              ? [
                                  // first tab [you can add an icon using the icon property]
                                  Tab(
                                    // text: 'Special Treats',
                                    child: Text(
                                      'Services',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 14, height: 1),
                                    ),
                                  ),

                                  // second tab [you can add an icon using the icon property]
                                  Tab(
                                      child: Text(
                                    'Product',
                                    style: TextStyle(fontSize: 14),
                                  )),
                                  Tab(
                                      child: Text(
                                    'Shopping',
                                    style: TextStyle(fontSize: 14),
                                  )),
                                ]
                              : [
                                  // second tab [you can add an icon using the icon property]
                                  Tab(
                                      child: Text(
                                    'Product',
                                    style: TextStyle(fontSize: 14),
                                  )),
                                  Tab(
                                      child: Text(
                                    'Shopping',
                                    style: TextStyle(fontSize: 14),
                                  )),
                                ],
                        ),
                      ),
                      // tab bar view here
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: PostData.panel
                              ? [
                                  ProductSpecialTab(),
                                  ProductMerchantTab(),
                                  ProductShoppingTab()
                                ]
                              : [ProductMerchantTab(), ProductShoppingTab()],
                        ),
                      ),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                if (PostData.update) {
                  return DialogUpdate();
                } else if (PostData.statusCode == 403) {
                  return DialogRelogin();
                } else {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    title: Column(children: [
                      Text(
                        'Connection Error',
                        style: TextStyle(
                            color: Mycolor.darkGreen,
                            fontSize: 24,
                            fontWeight: FontWeight.w900),
                      )
                    ]),
                  );
                }
              } else {
                return LoadingOverlay(
                    color: Colors.grey,
                    progressIndicator: tcardProgressDialog(),
                    child: Center(),
                    isLoading: true);
              }
            }));
  }
}
