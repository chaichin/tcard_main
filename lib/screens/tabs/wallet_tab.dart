import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shine/flutter_shine.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialog_relogin.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/tcard_progress_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/useVoucher.dart';
import 'package:tcard/models/wallets.dart';
import 'package:tcard/screens/walletscan_page.dart';
import 'package:tcard/services/getUserService.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/label_buttons.dart';
import 'package:tcard/components/voucher.dart';
import 'package:percent_indicator/percent_indicator.dart';

class WalletTab extends StatefulWidget {
  final selectedTab;
  @override
  WalletTab({this.selectedTab});
  State<StatefulWidget> createState() {
    return WalletTabState();
  }
}

class WalletTabState extends State<WalletTab>
    with AutomaticKeepAliveClientMixin {
  Future<Wallet> _wallet;
  String name;
  String phone;
  String pointsneeded;
  String profileimage;
  String panelType;
  List<String> getpoints;
  List<String> maxPoint;
  List<String> finalPoint;
  int vouchernum;
  List<UseVoucher> vouchers = [];
  @override
  void initState() {
    super.initState();
    if (PostData.logintoken != null) {
      User().profile().then((value) => setState(() {
            name = value[1];
            phone = value[2];
            profileimage = value[7];
            panelType = value[9];
          }));

      PostData().getUseVoucher().then((value) {
        setState(() {
          vouchers = value.where((e) => e.status == 'VALID').toList();

          print(vouchers.length);
        });
        //print(voucher.length);
      });
    }
  }

  @override
  bool get wantKeepAlive => true;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<Null> _handleRefresh() async {
    setState(() {
      _wallet = PostData().getWallet();
    });
    PostData().getUseVoucher().then((value) {
      setState(() {
        vouchers = value.where((e) => e.status == 'VALID').toList();

        print(vouchers.length);
      });
      //print(voucher.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    Size size = MediaQuery.of(context).size;
    _wallet = PostData.logintoken != null ? PostData().getWallet() : null;
    return PostData.logintoken == null
        ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Hi Guest !',
                  style: TextStyle(fontSize: 14),
                ),
                SizedBox(
                  width: 10,
                ),
                Button('LOGIN/SIGNUP HERE !', () {
                  Navigator.popUntil(context, ModalRoute.withName('/home'));
                })
              ],
            )
          ])
        : FlutterShine(
            config: Config(numSteps: 2, opacity: 0.5, offset: 0.1),
            builder: (BuildContext context, ShineShadow shineShadow) {
              return FutureBuilder(
                  future: _wallet,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      Wallet wallet = snapshot.data;
                      wallet.value.reduce((value, element) => null);

                      // for (var i = 0; i < wallet.value.length; i++) {
                      //   if (int.parse(wallet.pointBalance) <
                      //       double.parse(wallet.value[i].pointsNeeded)) {
                      //     print(wallet.value[i].pointsNeeded);
                      //     //print(wallet.pointBalance);

                      //     //array.add(wallet.value[i].pointsNeeded);
                      //     getpoints = [wallet.value[i].pointsNeeded];
                      //     print(getpoints);
                      //     //getpoints = array.reduce((value, element) => element);
                      //   }
                      // }

                      getpoints = wallet.value
                          .where((element) =>
                              int.parse(wallet.pointBalance) <
                              double.parse(element.pointsNeeded))
                          .map((e) => e.pointsNeeded)
                          .toList();
                      print(getpoints);

                      maxPoint = wallet.value
                          .where((element) =>
                              int.parse(wallet.pointBalance) ==
                              double.parse(element.pointsNeeded))
                          .map((e) => e.pointsNeeded)
                          .toList();
                      finalPoint =
                          wallet.value.map((e) => e.pointsNeeded).toList();
                      //print(finalPoint[2]);
                      pointsneeded = getpoints.length == 0
                          ? maxPoint.length != 0
                              ? maxPoint[0]
                              : '0'
                          : getpoints[0];
                      // wallet.value.forEach((item) {
                      //   if (int.parse(wallet.pointBalance) <
                      //       double.parse(item.pointsNeeded))
                      //     getpoints = item.pointsNeeded;
                      // });
                      double percentage = int.parse(wallet.pointBalance) /
                          double.parse(pointsneeded);
                      return Theme(
                          data: Theme.of(context)
                              .copyWith(accentColor: Mycolor.darkGreen),
                          child: RefreshIndicator(
                              key: _refreshIndicatorKey,
                              onRefresh: _handleRefresh,
                              child: ListView(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                    child: Row(
                                      // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Container(
                                          //alignment: Alignment.topCenter,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 10),
                                          child: profileimage != ""
                                              ? Container(
                                                  child: CachedNetworkImage(
                                                      imageUrl: profileimage,
                                                      imageBuilder: (context,
                                                          imageProvider) {
                                                        return Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            boxShadow:
                                                                shineShadow
                                                                    .boxShadows,
                                                            // borderRadius:
                                                            //     BorderRadius
                                                            //         .circular(
                                                            //             100),
                                                            shape:
                                                                BoxShape.circle,
                                                            image:
                                                                DecorationImage(
                                                              image:
                                                                  imageProvider,

                                                              // alignment:
                                                              //     Alignment
                                                              //         .topCenter,
                                                              fit: BoxFit
                                                                  .contain,
                                                              //scale: 1,
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      placeholder:
                                                          (BuildContext context,
                                                                  String url) =>
                                                              Container(),
                                                      // If error happens load image by using NetworkImage
                                                      errorWidget: (context,
                                                          url, error) {
                                                        return Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            boxShadow:
                                                                shineShadow
                                                                    .boxShadows,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            100)),
                                                            image:
                                                                DecorationImage(
                                                              image: NetworkImage(
                                                                  profileimage),
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              fit: BoxFit.cover,
                                                              //scale: 1,
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                      // width: size.width * 0.2,
                                                      height:
                                                          size.height * 0.09),
                                                )
                                              // Container(
                                              //     child: CachedNetworkImage(
                                              //       imageUrl: profileimage,
                                              //     ),
                                              //     decoration: BoxDecoration(
                                              //         boxShadow:
                                              //             shineShadow.boxShadows))
                                              : Container(
                                                  child: Image.asset(
                                                      'assets/images/profile_pic.png'),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  100)),
                                                      boxShadow: shineShadow
                                                          .boxShadows)),
                                          width: 80,
                                          height: 80,
                                        ),
                                        Expanded(
                                            flex: 2,
                                            child: Container(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'Hi, $name !',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Mycolor.darkGreen),
                                                ),
                                                Text(
                                                  '$phone',
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      color: Mycolor.darkGreen),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    // const SizedBox(width: 10),
                                                    Row(
                                                      children: [
                                                        Text(
                                                            '${wallet.pointBalance} points',
                                                            style: TextStyle(
                                                                //fontWeight: FontWeight.w300,
                                                                color: Mycolor
                                                                    .darkGreen)),
                                                        Image.asset(
                                                          'assets/images/teeth.png',
                                                          width: 15,
                                                          height: 15,
                                                        ),
                                                      ],
                                                    ),

                                                    Text(
                                                      '${percentage > 1 ? finalPoint[finalPoint.length - 1] : pointsneeded} pts',
                                                      style: TextStyle(
                                                          color: Mycolor
                                                              .darkGreen),
                                                    )
                                                  ],
                                                ),
                                                Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [],
                                                  ),
                                                ),
                                                LinearPercentIndicator(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 5),
                                                  animation: true,
                                                  lineHeight: 10.0,
                                                  animationDuration: 2500,
                                                  percent: percentage > 1
                                                      ? 1
                                                      : percentage,
                                                  linearStrokeCap:
                                                      LinearStrokeCap.roundAll,
                                                  progressColor:
                                                      Mycolor.darkGreen,
                                                ),
                                                Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 0,
                                                      vertical: 0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                        'Reach ${percentage > 1 ? finalPoint[finalPoint.length - 1] : pointsneeded} pts to get rewards',
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w300,
                                                            fontSize: 14,
                                                            color: Mycolor
                                                                .darkGreen),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            )))
                                      ],
                                    ),
                                  ),
                                  Card(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 10),
                                        // height: 40,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Text('Member',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w700,
                                                        color:
                                                            Mycolor.darkgrey)),
                                                Container(
                                                    width: 120,
                                                    child: Text('  $panelType',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Mycolor
                                                                .darkGreen)))
                                              ],
                                            ),
                                            // Expanded(
                                            //     child: Row(
                                            //   mainAxisAlignment:
                                            //       MainAxisAlignment.end,
                                            //   children: [
                                            //     Image.asset(
                                            //       'assets/images/voucher_profile.png',
                                            //       width: 30,
                                            //     ),
                                            //     InkWell(
                                            //         onTap: () {
                                            //           Navigator.pushNamed(
                                            //               context, '/voucher');
                                            //         },
                                            //         child: Column(children: [
                                            //           Text(
                                            //             '  ${vouchers.length} Vouchers',
                                            //             style: TextStyle(
                                            //                 fontWeight:
                                            //                     FontWeight.w700,
                                            //                 color: Mycolor
                                            //                     .darkgrey),
                                            //           ),
                                            //         ]))
                                            //   ],
                                            // ))
                                          ],
                                        )),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        // mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          LabelButton('scanqr', 'QR scan', () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        WalletScanPage()));
                                          }, 'image', 'medium'),
                                          LabelButton('voucher', 'E-News', () {
                                            Navigator.pushNamed(
                                                context, '/voucher');
                                          }, 'image', 'medium'),
                                          LabelButton('history', 'History', () {
                                            Navigator.pushNamed(
                                              context,
                                              '/history',
                                            );
                                          }, 'image', 'medium'),
                                          LabelButton('convert', 'Convert', () {
                                            Navigator.pushNamed(
                                                context, '/convert');
                                          }, 'image', 'medium')
                                        ],
                                      )),
                                  // Container(
                                  //   alignment: Alignment.centerRight,
                                  //   height: 10,
                                  //   width: MediaQuery.of(context).size.width,
                                  //   child: FlatButton(
                                  //     onPressed: () {
                                  //       print('seeall');
                                  //     },
                                  //     child: Text(
                                  //       'SEE ALL',
                                  //       style: TextStyle(
                                  //           color: Mycolor.darkGreen,
                                  //           fontSize: 10,
                                  //           fontWeight: FontWeight.bold),
                                  //     ),
                                  //   ),
                                  // ),
                                  Column(
                                      children: wallet.value
                                          .map((e) => Voucher(
                                                redeemed: int.parse(wallet
                                                            .pointBalance) <
                                                        double.parse(
                                                            e.pointsNeeded)
                                                    ? true
                                                    : false,
                                                myvoucher: false,
                                                title: e.title,
                                                description: e.description,
                                                pointBalance: e.pointsNeeded,
                                                voucherid: e.voucherId,
                                                changeTab: widget.selectedTab,
                                              ))
                                          .toList())
                                ],
                              )));
                    } else if (snapshot.hasError) {
                      if (PostData.update) {
                        return DialogUpdate();
                      } else if (PostData.statusCode == 403) {
                        return DialogRelogin();
                      } else {
                        return AlertDialog(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.0))),
                          title: Column(children: [
                            Text(
                              'Connection Error',
                              style: TextStyle(
                                  color: Mycolor.darkGreen,
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900),
                            )
                          ]),
                        );
                      }
                    } else {
                      return LoadingOverlay(
                          color: Colors.grey,
                          progressIndicator: tcardProgressDialog(),
                          child: Center(),
                          isLoading: true);
                    }
                  });
            });
  }
}
