import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:tcard/components/newSlider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/models/newproduct.dart';
import 'package:tcard/utils/mycolor.dart';

class ProductShoppingTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProductShoppingTabState();
  }
}

class ProductShoppingTabState extends State<ProductShoppingTab> {
  final PagingController<int, Shopping> _pagingController =
      PagingController(firstPageKey: 1);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      // PostData().getProductApi(pageKey).then((value) {
      //   newItems = value.merchant;
      // });
      //PostData().getProductApi(pageKey)
      PostData().getPageProductApi(pageKey).then((value) {
        final isLastPage = value.merchant.length == 0;

        if (isLastPage) {
          _pagingController.appendLastPage(value.shopping);
        } else {
          final nextPageKey = pageKey + 1;
          _pagingController.appendPage(value.shopping, nextPageKey);
        }
      });
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Theme(
        data: Theme.of(context).copyWith(accentColor: Mycolor.darkGreen),
        child: RefreshIndicator(
            onRefresh: () => Future.sync(
                  // 2
                  () => _pagingController.refresh(),
                ),
            child: PagedListView<int, Shopping>(
              padding: EdgeInsets.only(
                right: 15,
                left: 15,
              ),
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              pagingController: _pagingController,
              builderDelegate: PagedChildBuilderDelegate<Shopping>(
                  noItemsFoundIndicatorBuilder: (BuildContext context) {
                return Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/coming.png',
                      width: 120,
                      height: 120,
                    ),
                    Text(
                      'Something exciting\nis coming soon!',
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: Mycolor.grey),
                      textAlign: TextAlign.center,
                    )
                  ],
                ));
              }, itemBuilder: (context, item, index) {
                List<String> imgList =
                    item.images.split(',').map((e) => e).toList();
                return NewSliderItem(
                  flex: true,
                  value: 'merchant',
                  logo: item.merchantLogo,
                  source: imgList[0],
                  title: item.title,
                  productid: item.productId,
                  views: item.numberOfViews,
                  price: item.sellingPrice,
                  points: item.pointsNeeded,
                );
              }),
            )));
  }
}
