import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:tcard/components/buttons.dart';
import 'package:tcard/components/dialog_update.dart';
import 'package:tcard/components/dialogs.dart';
import 'package:tcard/components/scan_dialog.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:url_launcher/url_launcher.dart';

class ScanTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ScanTabState();
}

class _ScanTabState extends State<ScanTab> with AutomaticKeepAliveClientMixin {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController controller;
  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return PostData.logintoken == null
        ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Hi Guest !',
                  style: TextStyle(fontSize: 14),
                ),
                SizedBox(
                  width: 10,
                ),
                Button('LOGIN/SIGNUP HERE !', () {
                  Navigator.popUntil(context, ModalRoute.withName('/home'));
                })
              ],
            )
          ])
        : PostData.update
            ? DialogUpdate()
            : Column(
                children: <Widget>[
                  Expanded(
                      flex: 5,
                      child: Stack(alignment: Alignment.topCenter, children: [
                        QRView(
                          overlay: QrScannerOverlayShape(
                            borderColor: Mycolor.darkGreen,
                            borderRadius: 10,
                            borderLength: 30,
                            borderWidth: 10,
                            cutOutSize: 300,
                          ),
                          key: qrKey,
                          onQRViewCreated: _onQRViewCreated,
                        ),
                        Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.only(top: 40),
                          child: Text(
                            'Scan for point collection only',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          decoration: BoxDecoration(
                              color: Mycolor.normalWhite,
                              borderRadius: BorderRadius.circular(5)),
                        )
                      ])),
                  // Expanded(
                  //   flex: 1,
                  //   child: Center(
                  //     child: Text('Scan result: $qrText'),
                  //   ),
                  // )
                ],
              );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      controller.pauseCamera();
      setState(() {
        qrText = scanData;
        //Navigator.pushNamed(context, '/voucher');
      });
      print(scanData);
      var validateqr = scanData.split('://');
      if (validateqr[0] == "tcardclaimreceipt") {
        // print(validateqr[1]);
        //var receiptdata = validateqr[1].split('#');

        PostData().claimReceipt(scanData).then((value) => showDialog(
            context: context,
            builder: (context) => ScanDialogMessage(
                  title: value.data['status'],
                  message: value.data['message'],
                  success: false,
                  action: () {
                    Navigator.pop(context);
                    controller.resumeCamera();
                  },
                )));

        // controller.resumeCamera();
      } else if (validateqr[0] == "tcardurl") {
        String url = validateqr[1];

        if (await canLaunch(url)) {
          await launch(url);
          controller.resumeCamera();
        } else {
          showDialog(
              context: context,
              builder: (context) => ScanDialogMessage(
                    title: "Try Again",
                    message: "Could not launch $url",
                    success: false,
                    action: () {
                      Navigator.pop(context);
                      controller.resumeCamera();
                    },
                  ));
          throw 'Could not launch $url';
        }
      } else {
        showDialog(
            context: context,
            builder: (context) => ScanDialogMessage(
                  title: "Try Again",
                  message: "Invalid QR Code",
                  success: false,
                  action: () {
                    Navigator.pop(context);
                    controller.resumeCamera();
                  },
                ));
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
