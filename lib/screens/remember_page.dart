import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcard/components/back_buttons.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/components/inputs.dart';
import 'package:tcard/components/logo.dart';
import 'package:tcard/components/buttons.dart';

class RememberPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RememberState();
  }
}

class _RememberState extends State<RememberPage> {
  final userController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        body: Builder(
      builder: (context) => SafeArea(
          child: Container(
        child: Column(
          children: [
            Row(
              children: [
                ReturnButton(),
              ],
            ),
            Expanded(
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Logo(),
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Text(
                      "Please enter your phone number",
                      style: TextStyle(
                          color: Mycolor.darkGreen,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                      //textAlign: TextAlign.left,
                    ),
                  ),
                  Input(
                      value: 'Phone Number',
                      textArea: 1,
                      textController: userController),
                  Center(
                      child: Container(
                    margin: EdgeInsets.only(top: 40),
                    child: Button('OK', () {}),
                  )),
                ],
              ),
            ),
          ],
        ),
      )),
    ));
  }
}
