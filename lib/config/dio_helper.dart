import 'dart:io';

import 'package:dio/dio.dart';
import 'package:tcard/config/tcardconstant.dart';

class DioHelper {
  static Dio getDioInstance() {
    Dio dio = new Dio();
    dio.options.baseUrl = TcardConstant.API_URL;
    dio.options.headers['X-Api-Key'] = "ZVtFmZDq9XFgWz892TxGSGMgWzKufBaD";
    dio.options.headers['Operating-System'] =
        Platform.isAndroid ? 'ANDROID' : 'IOS';
    dio.options.headers['App-Version'] = TcardConstant.VERSION;
    return dio;
  }

  static Dio getDioInstanceUser(String token, String deviceid) {
    Dio dio = new Dio();
    dio.options.baseUrl = TcardConstant.API_URL;
    dio.options.headers['X-Api-Key'] = token;
    dio.options.headers['Android-Device-Id'] = deviceid;
    dio.options.headers['Operating-System'] =
        Platform.isAndroid ? 'ANDROID' : 'IOS';
    dio.options.headers['App-Version'] = TcardConstant.VERSION;
    return dio;
  }

  static Dio getAppointment() {
    Dio dio = new Dio();
    dio.options.baseUrl = TcardConstant.API_APPOINTMENT_URL;
    dio.options.headers['X-Api-Key'] = "ZVtFmZDq9XFgWz892TxGSGMgWzKufBaD";
    return dio;
  }
}
