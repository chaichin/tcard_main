import 'dart:async';
import 'dart:io';
import 'package:geolocator/geolocator.dart';
import 'package:platform_device_id/platform_device_id.dart';
import 'package:dio/dio.dart';
import 'package:crypto/crypto.dart';
import 'package:tcard/models/dialogimage.dart';
import 'dart:convert';
import 'package:tcard/config/dio_helper.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/appointment.dart';
import 'package:tcard/models/branches.dart';
import 'package:tcard/models/categories.dart';
import 'package:tcard/models/historyPoint.dart';
import 'package:tcard/models/historyTransaction.dart';
import 'package:tcard/models/merchants.dart';
import 'package:tcard/models/newproduct.dart';
import 'package:tcard/models/product.dart';
import 'package:tcard/models/profile.dart';
import 'package:tcard/models/search.dart';
import 'package:tcard/models/useVoucher.dart';
import 'package:tcard/models/voucher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tcard/models/wallets.dart';

class PostData {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  static String logintoken;
  static int statusCode;
  static List<String> profile;
  static bool panel;
  static int pageSize;
  static bool update = false;
  var dbHelper = DBHelper();
  // Declare the response item

  Response response;

  // New instance of dio
  var dio = DioHelper.getDioInstance();

  Future<Response> callRegisterAPI(registerdata) async {
    String deviceId = await PlatformDeviceId.getDeviceId;
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    // String value =
    //   "Thank you for signing up with tcard!\nYour tcard account have been created successfully!\nYou'll be the first to hear about all of our latest arrivals, new services, and exclusive promotions!\n\n\nBest Regards,\ntcard App Team";
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    print('register' + registerdata.values.toList().toString());
    // Create the form data
    // print('pane' + registerdata['panelcode']);
    FormData formData = new FormData.fromMap({
      // "name": "wendux",
      // "age": 25,
      "isPanel": registerdata['panel'],
      "panelPasscode": registerdata['panelcode'],
      "name": registerdata['name'],
      "phoneNumber": registerdata['phone'],
      "vCode": registerdata['vCode'],
      "dateOfBirth": registerdata['dob'],
      "gender": registerdata['gender'],
      "email": registerdata['email'],
      "deviceId": "$deviceId",
      "deviceType": registerdata['deviceType'],
      "millis": "$millis",
      "secret": "$digest",
    });

    try {
      response = await dio.post("/register", data: formData);
      // Notifications notification = Notifications(
      //     null,
      //     "tcard account created successfully','Dear  $registerdata['name']",
      //     'https://via.placeholder.com/50x50',
      //     "sd",
      //     value,
      //     null,
      //     null,
      //     'NORMAL',
      //     'unseen',
      //     DateTime.now().toString());
      // dbHelper.save(notification);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Response> callOtpAPI(String phonenumber) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    print(phonenumber);
    // try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      // "name": "wendux",
      // "age": 25,
      "phoneNumber": "$phonenumber",
      "millis": "$millis",
      "secret": "$digest",
    });

    // Call the API
    try {
      response = await dio.post("/verify", data: formData);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
    // Print the response
    //   var finalValue = response.data;
    //   print(finalValue['value'].toString());
    //   return finalValue;
    // } catch (e) {
    //   if (e is DioError) {
    //     print(e);
    //   }
    // }
  }

  Future<Response> callLoginAPI(String phonenumber, String password) async {
    final SharedPreferences prefs = await _prefs;
    String deviceId = await PlatformDeviceId.getDeviceId;
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    print(digest);
    // Create the form data
    FormData formData = new FormData.fromMap({
      // "name": "wendux",
      // "age": 25,
      "phoneNumber": "$phonenumber",
      "password": "$password",
      "deviceId": "$deviceId",
      "deviceType": Platform.isIOS ? "IOS" : "Android",
      "millis": "$millis",
      "secret": "$digest",
    });

    try {
      response = await dio.post("/login", data: formData);
      print(response.data['token']);
      print(response.data);
      PostData.logintoken = response.data['token'];
      PostData.panel = response.data['isPanel'];
      // PostData.profile = [
      //   response.data['token'],
      //   response.data['name'],
      //   response.data['email'],
      //   response.data['gender'],
      //   response.data['isPanel'],
      //   response.data['panelImage'],
      //   response.data['panelType']
      // ];
      prefs.setStringList('profile', [
        response.data['token'],
        response.data['name'],
        phonenumber,
        response.data['email'],
        response.data['gender'],
        response.data['isPanel'].toString(),
        response.data['PointBalance'],
        response.data['panelImage'] == null ? "" : response.data['panelImage'],
        response.data['panelType'] == null ? "" : response.data['panelType'],
        response.data['panel'] == null ? "Normal" : response.data['panel']
      ]);
      prefs.setString('PointBalance', response.data['PointBalance']);
      print('loginto' + logintoken);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }

    //   if (response.statusCode == 200) {
    //     print(response.data);
    //     return response.data;
    //   } else {
    //     return null;
    //   }
    //   //print(finalValue['value'].toString());
    // } on DioError catch (e) {
    //   return e.response.statusCode;
  }

  Future<Response> tokenValidation() async {
    String deviceId = await PlatformDeviceId.getDeviceId;
    String getToken;
    print('deviceID' + deviceId);
    try {
      getToken = await _prefs.then((value) {
        return value.getStringList('profile')[0];
      });
      print("hi" + getToken);
    } catch (e) {
      print('where');
      print(e);
    }
    //print('gettoken ' + getToken);

    try {
      var userDio = DioHelper.getDioInstanceUser(getToken, deviceId);
      response = await userDio.post("/token_validator");
      print('hi');
      PostData.logintoken = getToken;

      print('token validation ' + response.toString());
      // print('response' + response.data);
      PostData.panel = response.data['IsPanel'];
      return response;
    } on DioError catch (e) {
      print("das" + e.response.toString());
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Response> updateFCMtoken(String fcmtoken, String googleService) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);

    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "fcmToken": "$fcmtoken",
      "GoogleService": "$googleService"
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(logintoken, deviceId);
      response = await userDio.post('/update_fcm_token', data: formData);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Response> updateHuaweitoken(String huaweitoken) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);

    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "huaweiToken": "$huaweitoken",
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(logintoken, deviceId);
      response = await userDio.post('/update_huawei_token', data: formData);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Response> callResetAPI(String phonenumber) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);

    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      // "name": "wendux",
      // "age": 25,
      "phoneNumber": "$phonenumber",
      "millis": "$millis",
      "secret": "$digest",
    });

    // Call the API

    try {
      response = await dio.post("/reset_pin_number", data: formData);
      print('response' + response.toString());
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
    // Print the response
    //var finalValue = response.data;
    //   print(finalValue['value'].toString());
    // } on DioError catch (e) {
    //   // The request was made and the server responded with a status code
    //   // that falls out of the range of 2xx and is also not 304.
    //   print(e.response);
    // }
  }

  Future<List<Categories>> getMerchantApi() async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    bool getPanel = PostData.panel;
    String deviceId = await PlatformDeviceId.getDeviceId;
    // String fileName = "merchants.json";
    // var dir = await getTemporaryDirectory();
    // File file = new File(dir.path + "/" + fileName);
    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      // "name": "wendux",
      // "age": 25,

      "millis": "$millis",
      "secret": "$digest",
    });
    print('getPanel' + getPanel.toString());
    var userDio = DioHelper.getDioInstanceUser(logintoken, deviceId);
    try {
      // if (file.existsSync()) {
      //   print("Loading from cache");
      //   var jsonData = file.readAsStringSync();
      //   return categoriesFromJson(json.decode(jsonData));
      // } else {
      print("Loading from Api");
      response = getPanel
          ? await userDio.post("/get_all_merchants", data: formData)
          : await dio.post("/get_all_merchants", data: formData);

      //print(response.data['categories'].runtimeType);
      // file.writeAsStringSync(
      //     categoriesToJson(categoriesFromJson(response.data['categories'])),
      //     flush: true,
      //     mode: FileMode.write);
      return categoriesFromJson(response.data['categories']);
      // }
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<NewProduct> getProductApi(int pageNumber) async {
    print('page' + pageNumber.toString());
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    bool getPanel = PostData.panel;
    print('panel ' + getPanel.toString());
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);

    String deviceId = await PlatformDeviceId.getDeviceId;
    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      "page": pageNumber,
      "numOfItems": "10",
      "millis": "$millis",
      "secret": "$digest",
    });
    //file.delete();
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    try {
      print("Loading from Api");
      //response = await dio.post("/get_products", data: formData);
      //response = await userDio.post("/get_products", data: formData);
      response = PostData.logintoken == null
          ? await dio.post('/get_products', data: formData)
          : await userDio.post('/get_products', data: formData);
      PostData.pageSize = response.data['totalPages'];
      return getPanel == null
          ? NewProduct.fromJsons(response.data['products'])
          : getPanel
              ? NewProduct.fromJson(response.data['products'])
              : NewProduct.fromJsons(response.data['products']);
      // }

      // var products = productsFromJson(response.data['products'][0]);
      // print(products);
      // return products;

      //print(response.data['products']['MERCHANT'].runtimeType);

      // return productsFromJson(response.data['products']['MERCHANT']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<NewProduct> getPageProductApi(int pageNumber) async {
    print('page' + pageNumber.toString());
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    bool getPanel = PostData.panel;
    print('panel' + getPanel.toString());
    print('panel ' + getPanel.toString());
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);

    String deviceId = await PlatformDeviceId.getDeviceId;
    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      "page": pageNumber,
      "numOfItems": "5",
      "millis": "$millis",
      "secret": "$digest",
    });
    //file.delete();
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    try {
      print("Loading from Api");
      //response = await dio.post("/get_products", data: formData);
      //response = await userDio.post("/get_products", data: formData);
      response = PostData.logintoken == null
          ? await dio.post('/get_page_products', data: formData)
          : await userDio.post('/get_page_products', data: formData);
      PostData.pageSize = response.data['totalPages'];
      return getPanel == null
          ? NewProduct.fromJsons(response.data['products'])
          : getPanel
              ? NewProduct.fromJson(response.data['products'])
              : NewProduct.fromJsons(response.data['products']);
      // }

      // var products = productsFromJson(response.data['products'][0]);
      // print(products);
      // return products;

      //print(response.data['products']['MERCHANT'].runtimeType);

      // return productsFromJson(response.data['products']['MERCHANT']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<List<Merchants>> getMerchant(merchantid, pageNumber) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);

    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      "page": pageNumber,
      "numOfItems": "5",
      "millis": "$millis",
      "secret": "$digest",
      "merchantId": "$merchantid"
    });
    try {
      print("Loading from Api");
      response = await dio.post("/get_merchant_products", data: formData);

      // var products = productsFromJson(response.data['products'][0]);
      // print(products);
      // return products;

      //print(response.data['products']['MERCHANT'].runtimeType);
      return merchantsFromJson(response.data['products']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Product> getProduct(productid) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    String deviceId = await PlatformDeviceId.getDeviceId;
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    Position geolocation;
    try {
      geolocation = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
    } catch (e) {
      geolocation = null;
    }
    // geolocation = await Geolocator.getCurrentPosition(
    //       desiredAccuracy: LocationAccuracy.high);

    // print('pls' + permission.toString());
    // print('pls' + serviceEnabled.toString());
    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "productId": "$productid",
      "latitude": "${geolocation == null ? null : geolocation.latitude}",
      "longitude": "${geolocation == null ? null : geolocation.longitude}"
    });

    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    try {
      response = PostData.logintoken == null
          ? await dio.post("/get_product_by_product_id", data: formData)
          : await userDio.post("/get_product_by_product_id", data: formData);
      print(response);

      return Product.fromJson(response.data['product']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Profile> getProfile() async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;
    print(PostData.logintoken);
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
    });
    try {
      response = await userDio.post('/get_my_profile', data: formData);
      return Profile.fromJson(response.data['value']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<List<UseVoucher>> getUseVoucher() async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;
    print(PostData.logintoken);
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
    });
    try {
      response = await userDio.post('/get_my_vouchers', data: formData);
      print(response);
      return useVoucherFromJson(response.data['value']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<List<HistoryTransaction>> getHistory(
      String type, int pageNumber) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;
    print(PostData.logintoken);
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "type": "$type",
      "page": pageNumber,
      "numOfItems": "11"
    });
    try {
      response = await userDio.post('/get_history', data: formData);
      print(response);
      return historyTransactionFromJson(response.data['value']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<List<HistoryPoint>> getHistoryPoint(
      String type, int pageNumber) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;
    print(PostData.logintoken);
    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "type": "$type",
      "page": pageNumber,
      "numOfItems": "11"
    });
    try {
      response = await userDio.post('/get_history', data: formData);
      print(response);
      return historyPointFromJson(response.data['value']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> updateProfile(String email) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;

    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    FormData formData = new FormData.fromMap(
        {"millis": "$millis", "secret": "$digest", "email": "$email"});
    try {
      response = await userDio.post('update_my_profile', data: formData);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> getViews(String productid) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap(
        {"millis": "$millis", "secret": "$digest", "productId": "$productid"});

    try {
      response = await dio.post('/update_number_of_views', data: formData);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<List<Allbranches>> getBranches(
      String productid, Position location) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);

    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "productId": "$productid",
      "latitude": "${location.latitude}",
      "longitude": "${location.longitude}"
    });
    try {
      response = await dio.post('/get_product_branches', data: formData);
      return allbranchesFromJson(response.data['branches']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> getRedeem(String productid, String type, String branchCode,
      String points, String amountPaid, int quantity) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    final SharedPreferences prefs = await _prefs;
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);
    print('product id ' + productid);
    print("type" + type.toString());
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "productId": "$productid",
      "mode": "$type",
      "amountPayed": "$amountPaid",
      "branchCode": "$branchCode",
      "pointsNeeded": "$points",
      "quantity": quantity
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/get_redeem', data: formData);
      print(response);
      prefs.setString('PointBalance', response.data['PointBalance']);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Voucher> getVoucherDetails(String voucherid) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap(
        {"millis": "$millis", "secret": "$digest", "vTransId": "$voucherid"});
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/get_voucher_with_id', data: formData);
      print(response);
      return Voucher.fromJson(response.data['value']);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> getRedeemVoucher(String voucherid, String branchCode) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    final SharedPreferences prefs = await _prefs;
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);
    print('VOUCHER id ' + voucherid);

    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "vTransId": "$voucherid",
      "branchCode": "$branchCode"
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/redeem_voucher', data: formData);
      prefs.setString('PointBalance', response.data['PointBalance']);
      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<Response> claimReceipt(String value) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();

    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    String deviceId = await PlatformDeviceId.getDeviceId;
    var digest = sha1.convert(bytes);

    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "value": "$value",
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/claim_receipt', data: formData);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<List<Search>> searchProduct(String keyword, String filter) async {
    String deviceId = await PlatformDeviceId.getDeviceId;
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    //print('keyword ' + keyword);
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap({
      "page": "1",
      "numOfItems": "5",
      "millis": "$millis",
      "secret": "$digest",
      "keyword": "$keyword",
      "filter": "$filter",
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = PostData.logintoken == null
          ? await dio.post('/search_product', data: formData)
          : await userDio.post('/search_product', data: formData);

      print(response.data);

      return response.statusCode == 200
          ? searchFromJson(response.data['products'])
          : [];
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      return [];
    }
  }

  Future<Wallet> getWallet() async {
    final SharedPreferences prefs = await _prefs;
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    String deviceId = await PlatformDeviceId.getDeviceId;
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/get_wallet_info', data: formData);
      // print(response);
      prefs.setString('PointBalance', response.data['PointBalance']);
      return Wallet.fromJson(response.data);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      PostData.statusCode = e.response.statusCode;
      PostData.update = e.response.statusCode == 401 ? true : false;
      return e.response.data;
    }
  }

  Future<Appointment> getAppointmentDetails() async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
    });
    try {
      var appointmentDio = DioHelper.getAppointment();
      response =
          await appointmentDio.post('/get_appointment_details', data: formData);
      print(response);

      return Appointment.fromJson(response.data);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> submitAppointment(String name, String phone, String email,
      String preference, String id, String time, String remark) async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "name": "$name",
      "phoneNumber": "$phone",
      "email": "$email", //(OPTIONAL)
      "doctorId": preference == "Doctor" ? "$id" : '', //(OPTIONAL)
      "clinicId": preference == "Clinic" ? "$id" : '', //(OPTIONAL)
      "time": "$time",
      "remark": "$remark" //(OPTIONAL)
    });
    try {
      var appointmentDio = DioHelper.getAppointment();
      response =
          await appointmentDio.post('/submit_appointment', data: formData);
      print(response);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response.data;
    }
  }

  Future<Response> redeemCashVoucher(
      String pointBalance, String voucherId) async {
    final SharedPreferences prefs = await _prefs;
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    String deviceId = await PlatformDeviceId.getDeviceId;
    FormData formData = new FormData.fromMap({
      "millis": "$millis",
      "secret": "$digest",
      "pointsNeeded": "$pointBalance",
      "voucherId": "$voucherId"
    });
    try {
      var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
      response = await userDio.post('/redeem_cash_voucher', data: formData);
      prefs.setString('PointBalance', response.data['PointBalance']);
      print(response);

      return response;
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      print(e.response.statusMessage);
      return e.response;
    }
  }

  Future<DialogImage> getDialog() async {
    String millis = DateTime.now().millisecondsSinceEpoch.toString();
    String deviceId = await PlatformDeviceId.getDeviceId;
    var bytes = utf8.encode("$millis" + "VxcCav2Tm2wm6EkB9VyeGFd6wfcZtk5c");
    var digest = sha1.convert(bytes);
    Position geolocation;
    try {
      geolocation = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
    } catch (e) {
      geolocation = null;
    }
    // geolocation = await Geolocator.getCurrentPosition(
    //       desiredAccuracy: LocationAccuracy.high);

    // print('pls' + permission.toString());
    // print('pls' + serviceEnabled.toString());
    //try {
    // Create the form data
    FormData formData = new FormData.fromMap({});

    var userDio = DioHelper.getDioInstanceUser(PostData.logintoken, deviceId);
    try {
      response = PostData.logintoken == null
          ? await dio.post("/get_popup", data: formData)
          : await userDio.post("/get_popup", data: formData);
      print(response);

      return DialogImage.fromJson(response.data);
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.statusCode);
      //PostData.update = e.response.statusCode == 401 ? true : false;
      print(e.response.statusMessage);
      return e.response.data;
    }
  }
}
