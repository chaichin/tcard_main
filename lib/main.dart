//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/Notification.dart';
import 'package:tcard/models/notficationstate.dart';
import 'package:tcard/screens/appointment_page.dart';
import 'package:tcard/screens/branches_page.dart';
import 'package:tcard/screens/checkout_page.dart';
import 'package:tcard/screens/convert_page.dart';
import 'package:tcard/screens/historyTransaction_page.dart';
import 'package:tcard/screens/history_page.dart';
import 'package:tcard/screens/launch_screen.dart';
import 'package:tcard/screens/merchant_page.dart';
import 'package:tcard/screens/notification_page.dart';
import 'package:tcard/screens/scan_page.dart';
import 'package:tcard/screens/search_page.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/t&c_page.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:tcard/screens/walletscan_page.dart';
import 'package:tcard/utils/mycolor.dart';
import 'package:tcard/screens/signup_page.dart';
import 'package:tcard/screens/home_page.dart';
import 'package:tcard/screens/otpverification_page.dart';
import 'package:tcard/screens/login_page.dart';
import 'package:tcard/screens/main_page.dart';
import 'package:tcard/screens/voucher_page.dart';
import 'package:tcard/screens/reset_page.dart';
import 'package:firebase_core/firebase_core.dart';

import 'services/notificationService.dart';

var id;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  // await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
  //   alert: true,
  //   badge: true,
  //   sound: true,
  // );

  runApp(ChangeNotifierProvider(
      create: (context) => NotificationState(), child: MyApp()));
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  // print(message.data);
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'channel id', 'channel name', 'channel description',
      importance: Importance.max, priority: Priority.high);
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);

  Notifications notification = Notifications(
      null,
      message.data['title'],
      message.data['image'],
      message.data['message'],
      message.data['productId'],
      message.data['url'],
      message.data['vTransId'],
      message.data['type'],
      DateTime.now().toString(),
      'unseen');
  DBHelper().save(notification);
  // await NotificationService.flutterLocalNotificationsPlugin.show(
  //   message.hashCode,
  //   message.data['title'],
  //   message.data['message'],
  //   platformChannelSpecifics,
  // );

  print("Handling a background message: ${message.messageId}");
  //NotificationService().showNotification(message);
  print("Handling a background message: ${message.data}");
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AppState();
  }
}

class _AppState extends State<MyApp> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  @override
  void initState() {
    super.initState();
    //initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initialization,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                fontFamily: "DarkerGrotesque",
                primaryColor: Mycolor.darkGreen,
              ),
              navigatorKey: NavKey.navKey,
              routes: {
                '/': (context) => LaunchScreen(),
                '/main': (context) => MainPage(
                      shoppingTab: 0,
                    ),
                '/home': (context) => MyHomePage(),
                '/login': (context) => LoginPage(),
                '/signup': (context) => SignupPage(),
                '/otp_verification': (context) => OtpVerificationPage(),
                '/appointment': (context) => AppointmentPage(),
                '/history': (context) => HistoryPage(),
                '/historytransaction': (context) => HistoryTransactionPage(),
                '/voucher': (context) => VoucherPage(),
                '/notification': (context) => NotificationPage(),
                '/reset': (context) => ResetPage(),
                '/walletscan': (context) => WalletScanPage(),
                'merchant': (context) => MerchantPage(),
                '/shopping': (context) => ShoppingPage(),
                '/t&c': (context) => Term(),
                '/checkout': (context) => CheckoutPage(),
                '/branches': (context) => BranchesPage(),
                '/scan': (context) => ScanPage(),
                '/voucherDetails': (context) => VoucherDescriptionPage(),
                '/search': (context) => SearchPage(),
                '/convert': (context) => ConvertPage(),
              },
            );
          }
          return CircularProgressIndicator();
        });
    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   theme: ThemeData(
    //     fontFamily: "Montserrat",
    //     primaryColor: Mycolor.darkGreen,
    //   ),
    //   initialRoute: '/',
    //   routes: {
    //     '/': (context) => MyHomePage(),
    //     '/login': (context) => LoginPage(),
    //     '/signup': (context) => SignupPage(),
    //     '/otp_verification': (context) => OtpVerificationPage(),
    //     '/home': (context) => MainPage(selectedPage: 0),
    //     '/appointment': (context) => AppointmentPage(),
    //     '/history': (context) => HistoryPage(),
    //     '/historytransaction': (context) => HistoryTransactionPage(),
    //     '/profiledit': (context) => ProfileEdit(),
    //     '/voucher': (context) => VoucherPage(),
    //     '/notification': (context) => NotificationPage(),
    //     '/reset': (context) => ResetPage(),
    //     '/wallet': (context) => WalletPage(),
    //     'merchant': (context) => MerchantPage(),
    //     '/shopping': (context) => ShoppingPage(),
    //     '/t&c': (context) => Term(),
    //     '/branches': (context) => BranchesPage(),
    //     '/scan': (context) => ScanPage(),
    //     '/voucherDetails': (context) => VoucherDescriptionPage()
    //   },
    // );
  }
}

class NavKey {
  static final navKey = new GlobalKey<NavigatorState>();
}
