class Notifications {
  int id;
  String title;
  String image;
  String value;
  String productid;
  String voucherid;
  String type;
  String url;
  String day;
  String status;
  Notifications(this.id, this.title, this.image, this.value, this.productid,
      this.url, this.voucherid, this.type, this.day, this.status);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'title': title,
      'image': image,
      'value': value,
      'productid': productid,
      'url': url,
      'voucherid': voucherid,
      'type': type,
      'day': day,
      'status': status,
    };
    return map;
  }

  Notifications.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    title = map['title'];
    image = map['image'];
    value = map['value'];
    productid = map['productid'];
    url = map['url'];
    voucherid = map['voucherid'];
    type = map['type'];
    day = map['day'];
    status = map['status'];
  }
}
