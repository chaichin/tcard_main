import 'dart:convert';

List<UseVoucher> useVoucherFromJson(List voucher) =>
    (voucher).map((x) => UseVoucher.fromJson(x)).toList();

String useVoucherToJson(List<UseVoucher> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UseVoucher {
  UseVoucher({
    this.vTransId,
    this.voucherPrefix,
    this.voucherSerial,
    this.userId,
    this.name,
    this.phoneNumber,
    this.expiryDate,
    this.voucherId,
    this.redeemDate,
    this.redeemArea,
    this.status,
    this.createDate,
    this.insertBy,
    this.lastUpdate,
    this.updateBy,
    this.title,
    this.image,
    this.description,
    this.termsCondition,
    this.merchantId,
    this.insertDate,
  });

  String vTransId;
  String voucherPrefix;
  String voucherSerial;
  String userId;
  String name;
  String phoneNumber;
  DateTime expiryDate;
  String voucherId;
  dynamic redeemDate;
  dynamic redeemArea;
  String status;
  String createDate;
  String insertBy;
  String lastUpdate;
  String updateBy;
  String title;
  String image;
  String description;
  String termsCondition;
  String merchantId;
  String insertDate;

  factory UseVoucher.fromJson(Map<String, dynamic> json) => UseVoucher(
        vTransId: json["VTransID"],
        voucherPrefix: json["VoucherPrefix"],
        voucherSerial: json["VoucherSerial"],
        userId: json["UserID"],
        name: json["Name"],
        phoneNumber: json["PhoneNumber"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        voucherId: json["VoucherID"],
        redeemDate: json["RedeemDate"],
        redeemArea: json["RedeemArea"],
        status: json["Status"],
        createDate: json["CreateDate"],
        insertBy: json["InsertBy"],
        lastUpdate: json["LastUpdate"],
        updateBy: json["UpdateBy"],
        title: json["Title"],
        image: json["Image"],
        description: json["Description"],
        termsCondition: json["TermsCondition"],
        merchantId: json["MerchantID"],
        insertDate: json["InsertDate"],
      );

  Map<String, dynamic> toJson() => {
        "VTransID": vTransId,
        "VoucherPrefix": voucherPrefix,
        "VoucherSerial": voucherSerial,
        "UserID": userId,
        "Name": name,
        "PhoneNumber": phoneNumber,
        "ExpiryDate": expiryDate,
        "VoucherID": voucherId,
        "RedeemDate": redeemDate,
        "RedeemArea": redeemArea,
        "Status": status,
        "CreateDate": createDate,
        "InsertBy": insertBy,
        "LastUpdate": lastUpdate,
        "UpdateBy": updateBy,
        "Title": title,
        "Image": image,
        "Description": description,
        "TermsCondition": termsCondition,
        "MerchantID": merchantId,
        "InsertDate": insertDate,
      };
}
