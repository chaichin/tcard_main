// To parse this JSON data, do
//
//     final newProduct = newProductFromJson(jsonString);

import 'dart:convert';

import 'package:tcard/config/postData.dart';

NewProduct newProductFromJson(String str) =>
    NewProduct.fromJson(json.decode(str));

String newProductToJson(NewProduct data) =>
    json.encode(PostData.panel ? data.toJson() : data.toJsons());

class NewProduct {
  // NewProduct(
  //     {this.shopping,
  //     this.merchant,
  //     this.specialtreats,
  //     this.carousel,
  //     this.minicarousel});
  NewProduct(
      {this.shopping,
      this.merchant,
      this.specialtreats,
      this.carousel,
      this.minicarousel});

  List<Shopping> shopping;
  List<Merchant> merchant;
  List<SpecialTreats> specialtreats;
  List<BigCarousel> carousel;
  List<MiniCarousel> minicarousel;

  factory NewProduct.fromJson(Map<String, dynamic> json) => NewProduct(
        shopping: json['SHOPPING'] != null
            ? List<Shopping>.from(
                json["SHOPPING"].map((x) => Shopping.fromJson(x)))
            : null,
        specialtreats: json['SpecialTreats'] != null
            ? List<SpecialTreats>.from(
                json["SpecialTreats"].map((x) => SpecialTreats.fromJson(x)))
            : null,
        merchant: json['MERCHANT'] != null
            ? List<Merchant>.from(
                json["MERCHANT"].map((x) => Merchant.fromJson(x)))
            : null,
        carousel: json['CAROUSEL'] != null
            ? List<BigCarousel>.from(
                json["CAROUSEL"].map((x) => BigCarousel.fromJson(x)))
            : null,
        minicarousel: json['MINICAROUSEL'] != null
            ? List<MiniCarousel>.from(
                json["MINICAROUSEL"].map((x) => MiniCarousel.fromJson(x)))
            : null,
      );
  factory NewProduct.fromJsons(Map<String, dynamic> json) => NewProduct(
        shopping: json['SHOPPING'] != null
            ? List<Shopping>.from(
                json["SHOPPING"].map((x) => Shopping.fromJson(x)))
            : null,
        merchant: json['MERCHANT'] != null
            ? List<Merchant>.from(
                json["MERCHANT"].map((x) => Merchant.fromJson(x)))
            : null,
        // specialtreats: List<SpecialTreats>.from(
        //     json["SpecialTreats"].map((x) => SpecialTreats.fromJson(x))),
        carousel: json['CAROUSEL'] != null
            ? List<BigCarousel>.from(
                json["CAROUSEL"].map((x) => BigCarousel.fromJson(x)))
            : null,
        minicarousel: json['MINICAROUSEL'] != null
            ? List<MiniCarousel>.from(
                json["MINICAROUSEL"].map((x) => MiniCarousel.fromJson(x)))
            : null,
      );
  factory NewProduct.fromJsonsearch(Map<String, dynamic> json) => NewProduct(
        shopping: List<Shopping>.from(
            json["SHOPPING"].map((x) => Shopping.fromJson(x))),
        merchant: List<Merchant>.from(
            json["MERCHANT"].map((x) => Merchant.fromJson(x))),
        specialtreats: List<SpecialTreats>.from(
            json["SpecialTreats"].map((x) => SpecialTreats.fromJson(x))),
      );
  factory NewProduct.fromJsonsearch2(Map<String, dynamic> json) => NewProduct(
        shopping: List<Shopping>.from(
            json["SHOPPING"].map((x) => Shopping.fromJson(x))),
        merchant: List<Merchant>.from(
            json["MERCHANT"].map((x) => Merchant.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "SHOPPING": List<dynamic>.from(shopping.map((x) => x.toJson())),
        "SpecialTreats":
            List<dynamic>.from(specialtreats.map((x) => x.toJson())),
        "MERCHANT": List<dynamic>.from(merchant.map((x) => x.toJson())),
        "CAROUSEL": List<dynamic>.from(carousel.map((x) => x.toJson())),
        "MINICAROUSEL": List<dynamic>.from(minicarousel.map((x) => x.toJson())),
      };
  Map<String, dynamic> toJsons() => {
        "SHOPPING": List<dynamic>.from(shopping.map((x) => x.toJson())),
        "MERCHANT": List<dynamic>.from(merchant.map((x) => x.toJson())),
        // "SpecialTreats":
        //     List<dynamic>.from(specialtreats.map((x) => x.toJson())),
        "CAROUSEL": List<dynamic>.from(carousel.map((x) => x.toJson())),
        "MINICAROUSEL": List<dynamic>.from(minicarousel.map((x) => x.toJson())),
      };
}

class Merchant {
  Merchant({
    this.panelType,
    this.isPanel,
    this.numberOfViews,
    this.sequence,
    this.quantity,
    this.productId,
    this.productType,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.sellingPrice,
    this.displayPrice,
    this.discountText,
    this.discountType,
    this.expiryDate,
    this.termsCondition,
    this.status,
    this.insertDate,
    this.lastUpdate,
    this.merchantLogo,
  });

  dynamic panelType;
  String isPanel;
  String numberOfViews;
  String sequence;
  String quantity;
  String productId;
  String productType;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String displayPrice;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;

  factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
        panelType: json["PanelType"],
        isPanel: json["IsPanel"],
        numberOfViews: json["NumberOfViews"],
        sequence: json["Sequence"],
        quantity: json["Quantity"],
        productId: json["ProductID"],
        productType: json["ProductType"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        sellingPrice: json["SellingPrice"],
        displayPrice: json["DisplayPrice"],
        discountText: json["DiscountText"],
        discountType: json["DiscountType"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        merchantLogo: json["MerchantLogo"],
      );

  Map<String, dynamic> toJson() => {
        "PanelType": panelType,
        "IsPanel": isPanel,
        "NumberOfViews": numberOfViews,
        "Sequence": sequence,
        "Quantity": quantity,
        "ProductID": productId,
        "ProductType": productType,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "DisplayPrice": displayPrice,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
      };
}

class Shopping {
  Shopping({
    this.panelType,
    this.isPanel,
    this.numberOfViews,
    this.sequence,
    this.quantity,
    this.productId,
    this.productType,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.sellingPrice,
    this.displayPrice,
    this.pointsNeeded,
    this.discountText,
    this.discountType,
    this.expiryDate,
    this.termsCondition,
    this.status,
    this.insertDate,
    this.lastUpdate,
    this.merchantLogo,
  });

  dynamic panelType;
  String isPanel;
  String numberOfViews;
  String sequence;
  String quantity;
  String productId;
  String productType;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String displayPrice;
  String pointsNeeded;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;

  factory Shopping.fromJson(Map<String, dynamic> json) => Shopping(
        panelType: json["PanelType"],
        isPanel: json["IsPanel"],
        numberOfViews: json["NumberOfViews"],
        sequence: json["Sequence"],
        quantity: json["Quantity"],
        productId: json["ProductID"],
        productType: json["ProductType"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        sellingPrice: json["SellingPrice"],
        displayPrice: json["DisplayPrice"],
        pointsNeeded: json["PointsNeeded"],
        discountText: json["DiscountText"],
        discountType: json["DiscountType"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        merchantLogo: json["MerchantLogo"],
      );

  Map<String, dynamic> toJson() => {
        "PanelType": panelType,
        "IsPanel": isPanel,
        "NumberOfViews": numberOfViews,
        "Sequence": sequence,
        "Quantity": quantity,
        "ProductID": productId,
        "ProductType": productType,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "DisplayPrice": displayPrice,
        "PointsNeeded": pointsNeeded,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
      };
}

class SpecialTreats {
  SpecialTreats({
    this.panelType,
    this.isPanel,
    this.numberOfViews,
    this.sequence,
    this.quantity,
    this.productId,
    this.productType,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.sellingPrice,
    this.displayPrice,
    this.pointsNeeded,
    this.discountText,
    this.discountType,
    this.expiryDate,
    this.termsCondition,
    this.status,
    this.insertDate,
    this.lastUpdate,
    this.merchantLogo,
  });

  dynamic panelType;
  String isPanel;
  String numberOfViews;
  String sequence;
  String quantity;
  String productId;
  String productType;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String displayPrice;
  String pointsNeeded;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;

  factory SpecialTreats.fromJson(Map<String, dynamic> json) => SpecialTreats(
        panelType: json["PanelType"],
        isPanel: json["IsPanel"],
        numberOfViews: json["NumberOfViews"],
        sequence: json["Sequence"],
        quantity: json["Quantity"],
        productId: json["ProductID"],
        productType: json["ProductType"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        sellingPrice: json["SellingPrice"],
        displayPrice: json["DisplayPrice"],
        pointsNeeded: json["PointsNeeded"],
        discountText: json["DiscountText"],
        discountType: json["DiscountType"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        merchantLogo: json["MerchantLogo"],
      );

  Map<String, dynamic> toJson() => {
        "PanelType": panelType,
        "IsPanel": isPanel,
        "NumberOfViews": numberOfViews,
        "Sequence": sequence,
        "Quantity": quantity,
        "ProductID": productId,
        "ProductType": productType,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "DisplayPrice": displayPrice,
        "PointsNeeded": pointsNeeded,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
      };
}

class BigCarousel {
  BigCarousel({
    this.carouselId,
    this.title,
    this.description,
    this.type,
    this.buttonText,
    this.isPanel,
    this.panelType,
    this.image,
    this.productId,
    this.url,
    this.sequence,
    this.status,
    this.insertBy,
    this.insertDate,
    this.lastUpdateDate,
  });

  String carouselId;
  dynamic title;
  dynamic description;
  String type;
  dynamic buttonText;
  String isPanel;
  dynamic panelType;
  String image;
  dynamic productId;
  String url;
  String sequence;
  String status;
  String insertBy;
  DateTime insertDate;
  dynamic lastUpdateDate;

  factory BigCarousel.fromJson(Map<String, dynamic> json) => BigCarousel(
        carouselId: json["CarouselID"],
        title: json["Title"],
        description: json["Description"],
        type: json["Type"],
        buttonText: json["ButtonText"],
        isPanel: json["IsPanel"],
        panelType: json["PanelType"],
        image: json["Image"],
        productId: json["ProductID"],
        url: json["Url"],
        sequence: json["Sequence"],
        status: json["Status"],
        insertBy: json["InsertBy"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdateDate: json["LastUpdateDate"],
      );

  Map<String, dynamic> toJson() => {
        "CarouselID": carouselId,
        "Title": title,
        "Description": description,
        "Type": type,
        "ButtonText": buttonText,
        "IsPanel": isPanel,
        "PanelType": panelType,
        "Image": image,
        "ProductID": productId,
        "Url": url,
        "Sequence": sequence,
        "Status": status,
        "InsertBy": insertBy,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdateDate": lastUpdateDate,
      };
}

class MiniCarousel {
  MiniCarousel({
    this.carouselId,
    this.title,
    this.description,
    this.type,
    this.buttonText,
    this.isPanel,
    this.panelType,
    this.image,
    this.productId,
    this.url,
    this.sequence,
    this.status,
    this.insertBy,
    this.insertDate,
    this.lastUpdateDate,
  });

  String carouselId;
  dynamic title;
  dynamic description;
  String type;
  dynamic buttonText;
  String isPanel;
  dynamic panelType;
  String image;
  dynamic productId;
  String url;
  String sequence;
  String status;
  String insertBy;
  DateTime insertDate;
  dynamic lastUpdateDate;

  factory MiniCarousel.fromJson(Map<String, dynamic> json) => MiniCarousel(
        carouselId: json["CarouselID"],
        title: json["Title"],
        description: json["Description"],
        type: json["Type"],
        buttonText: json["ButtonText"],
        isPanel: json["IsPanel"],
        panelType: json["PanelType"],
        image: json["Image"],
        productId: json["ProductID"],
        url: json["Url"],
        sequence: json["Sequence"],
        status: json["Status"],
        insertBy: json["InsertBy"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdateDate: json["LastUpdateDate"],
      );

  Map<String, dynamic> toJson() => {
        "CarouselID": carouselId,
        "Title": title,
        "Description": description,
        "Type": type,
        "ButtonText": buttonText,
        "IsPanel": isPanel,
        "PanelType": panelType,
        "Image": image,
        "ProductID": productId,
        "Url": url,
        "Sequence": sequence,
        "Status": status,
        "InsertBy": insertBy,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdateDate": lastUpdateDate,
      };
}
