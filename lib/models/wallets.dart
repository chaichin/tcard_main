// To parse this JSON data, do
//
//     final wallet = walletFromJson(jsonString);

import 'dart:convert';

Wallet walletFromJson(String str) => Wallet.fromJson(json.decode(str));

String walletToJson(Wallet data) => json.encode(data.toJson());

class Wallet {
  Wallet({
    this.value,
    this.pointBalance,
  });

  List<Value> value;
  String pointBalance;

  factory Wallet.fromJson(Map<String, dynamic> json) => Wallet(
        value: List<Value>.from(json["value"].map((x) => Value.fromJson(x))),
        pointBalance: json["PointBalance"],
      );

  Map<String, dynamic> toJson() => {
        "value": List<dynamic>.from(value.map((x) => x.toJson())),
        "PointBalance": pointBalance,
      };
}

class Value {
  Value({
    this.voucherId,
    this.title,
    this.image,
    this.description,
    this.termsCondition,
    this.merchantId,
    this.pointsNeeded,
    this.type,
    this.status,
    this.insertDate,
    this.insertBy,
    this.lastUpdate,
    this.updateBy,
  });

  String voucherId;
  String title;
  String image;
  String description;
  String termsCondition;
  String merchantId;
  String pointsNeeded;
  String type;
  String status;
  DateTime insertDate;
  String insertBy;
  DateTime lastUpdate;
  String updateBy;

  factory Value.fromJson(Map<String, dynamic> json) => Value(
        voucherId: json["VoucherID"],
        title: json["Title"],
        image: json["Image"],
        description: json["Description"],
        termsCondition: json["TermsCondition"],
        merchantId: json["MerchantID"],
        pointsNeeded: json["PointsNeeded"],
        type: json["Type"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        insertBy: json["InsertBy"],
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        updateBy: json["UpdateBy"],
      );

  Map<String, dynamic> toJson() => {
        "VoucherID": voucherId,
        "Title": title,
        "Image": image,
        "Description": description,
        "TermsCondition": termsCondition,
        "MerchantID": merchantId,
        "PointsNeeded": pointsNeeded,
        "Type": type,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "InsertBy": insertBy,
        "LastUpdate": lastUpdate.toIso8601String(),
        "UpdateBy": updateBy,
      };
}
