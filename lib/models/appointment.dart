// To parse this JSON data, do
//
//     final appointment = appointmentFromJson(jsonString);

import 'dart:convert';

Appointment appointmentFromJson(String str) =>
    Appointment.fromJson(json.decode(str));

String appointmentToJson(Appointment data) => json.encode(data.toJson());

class Appointment {
  Appointment({
    this.clinics,
    this.doctors,
  });

  List<Clinic> clinics;
  List<Doctor> doctors;

  factory Appointment.fromJson(Map<String, dynamic> json) => Appointment(
        clinics:
            List<Clinic>.from(json["clinics"].map((x) => Clinic.fromJson(x))),
        doctors:
            List<Doctor>.from(json["doctors"].map((x) => Doctor.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "clinics": List<dynamic>.from(clinics.map((x) => x.toJson())),
        "doctors": List<dynamic>.from(doctors.map((x) => x.toJson())),
      };
}

class Clinic {
  Clinic({
    this.clinicId,
    this.branchName,
    this.clinicName,
    this.workingHours,
  });

  String clinicId;
  String branchName;
  String clinicName;
  List<String> workingHours;

  factory Clinic.fromJson(Map<String, dynamic> json) => Clinic(
        clinicId: json["ClinicID"],
        branchName: json["BranchName"],
        clinicName: json["ClinicName"],
        workingHours: List<String>.from(json["WorkingHours"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "ClinicID": clinicId,
        "BranchName": branchName,
        "ClinicName": clinicName,
        "WorkingHours": List<dynamic>.from(workingHours.map((x) => x)),
      };
}

enum ClinicName {
  TIEW_DENTAL_CLINIC,
  WHITE_DENTAL_COSMETIC_CENTRE,
  TIEW_DENTAL_CENTRE,
  MY_DENTAL_CLINIC
}

final clinicNameValues = EnumValues({
  "MyDental Clinic": ClinicName.MY_DENTAL_CLINIC,
  "Tiew Dental Centre": ClinicName.TIEW_DENTAL_CENTRE,
  "Tiew Dental Clinic": ClinicName.TIEW_DENTAL_CLINIC,
  "White Dental Cosmetic Centre": ClinicName.WHITE_DENTAL_COSMETIC_CENTRE
});

class Doctor {
  Doctor({
    this.id,
    this.doctorName,
    this.appoinment,
  });

  String id;
  String doctorName;
  List<String> appoinment;

  factory Doctor.fromJson(Map<String, dynamic> json) => Doctor(
        id: json["ID"],
        doctorName: json["DoctorName"],
        appoinment: List<String>.from(json["Appoinment"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "DoctorName": doctorName,
        "Appoinment": List<dynamic>.from(appoinment.map((x) => x)),
      };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
