import 'dart:convert';

Voucher voucherFromJson(String str) => Voucher.fromJson(json.decode(str));

String voucherToJson(Voucher data) => json.encode(data.toJson());

class Voucher {
  Voucher({
    this.vTransId,
    this.voucherPrefix,
    this.voucherSerial,
    this.userId,
    this.name,
    this.phoneNumber,
    this.expiryDate,
    this.voucherId,
    this.redeemDate,
    this.redeemArea,
    this.status,
    this.createDate,
    this.insertBy,
    this.lastUpdate,
    this.updateBy,
    this.title,
    this.image,
    this.description,
    this.termsCondition,
    this.merchantId,
    this.insertDate,
  });

  String vTransId;
  String voucherPrefix;
  String voucherSerial;
  String userId;
  String name;
  String phoneNumber;
  DateTime expiryDate;
  String voucherId;
  dynamic redeemDate;
  dynamic redeemArea;
  String status;
  DateTime createDate;
  String insertBy;
  DateTime lastUpdate;
  String updateBy;
  String title;
  String image;
  String description;
  String termsCondition;
  String merchantId;
  DateTime insertDate;

  factory Voucher.fromJson(Map<String, dynamic> json) => Voucher(
        vTransId: json["VTransID"],
        voucherPrefix: json["VoucherPrefix"],
        voucherSerial: json["VoucherSerial"],
        userId: json["UserID"],
        name: json["Name"],
        phoneNumber: json["PhoneNumber"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        voucherId: json["VoucherID"],
        redeemDate: json["RedeemDate"],
        redeemArea: json["RedeemArea"],
        status: json["Status"],
        createDate: DateTime.parse(json["CreateDate"]),
        insertBy: json["InsertBy"],
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        updateBy: json["UpdateBy"],
        title: json["Title"],
        image: json["Image"],
        description: json["Description"],
        termsCondition: json["TermsCondition"],
        merchantId: json["MerchantID"],
        insertDate: DateTime.parse(json["InsertDate"]),
      );

  Map<String, dynamic> toJson() => {
        "VTransID": vTransId,
        "VoucherPrefix": voucherPrefix,
        "VoucherSerial": voucherSerial,
        "UserID": userId,
        "Name": name,
        "PhoneNumber": phoneNumber,
        "ExpiryDate": expiryDate.toIso8601String(),
        "VoucherID": voucherId,
        "RedeemDate": redeemDate,
        "RedeemArea": redeemArea,
        "Status": status,
        "CreateDate": createDate.toIso8601String(),
        "InsertBy": insertBy,
        "LastUpdate": lastUpdate.toIso8601String(),
        "UpdateBy": updateBy,
        "Title": title,
        "Image": image,
        "Description": description,
        "TermsCondition": termsCondition,
        "MerchantID": merchantId,
        "InsertDate": insertDate.toIso8601String(),
      };
}
