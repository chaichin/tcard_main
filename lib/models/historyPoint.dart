// To parse this JSON data, do
//
//     final historyPoint = historyPointFromJson(jsonString);

import 'dart:convert';

List<HistoryPoint> historyPointFromJson(List point) =>
    point.map((x) => HistoryPoint.fromJson(x)).toList();

String historyPointToJson(List<HistoryPoint> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HistoryPoint {
  HistoryPoint({
    this.walletId,
    this.productId,
    this.userId,
    this.walletType,
    this.transferAmount,
    this.beginAmount,
    this.endAmount,
    this.status,
    this.insertDate,
    this.lastUpdateBy,
    this.lastUpdateDate,
    this.voucherSerial,
    this.invoiceId,
    this.title,
  });
  String title;
  String walletId;
  String userId;
  String walletType;
  String transferAmount;
  String beginAmount;
  String endAmount;
  Status status;
  DateTime insertDate;
  dynamic lastUpdateBy;
  dynamic lastUpdateDate;
  String voucherSerial;
  String invoiceId;
  String productId;

  factory HistoryPoint.fromJson(Map<String, dynamic> json) => HistoryPoint(
      walletId: json["WalletID"],
      userId: json["UserID"],
      walletType: json["WalletType"],
      transferAmount: json["TransferAmount"],
      beginAmount: json["BeginAmount"],
      endAmount: json["EndAmount"],
      status: statusValues.map[json["Status"]],
      insertDate: DateTime.parse(json["InsertDate"]),
      lastUpdateBy: json["LastUpdateBy"],
      lastUpdateDate: json["LastUpdateDate"],
      voucherSerial: json["VoucherSerial"],
      invoiceId: json["InvoiceID"],
      title: json["Title"],
      productId: json["ProductID"]);

  Map<String, dynamic> toJson() => {
        "WalletID": walletId,
        "UserID": userId,
        "WalletType": walletTypeValues.reverse[walletType],
        "TransferAmount": transferAmount,
        "BeginAmount": beginAmount,
        "EndAmount": endAmount,
        "Status": statusValues.reverse[status],
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdateBy": lastUpdateBy,
        "LastUpdateDate": lastUpdateDate,
      };
}

enum Status { VALID }

final statusValues = EnumValues({"VALID": Status.VALID});

enum WalletType { OUT }

final walletTypeValues = EnumValues({"OUT": WalletType.OUT});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
