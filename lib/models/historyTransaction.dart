import 'dart:convert';

List<HistoryTransaction> historyTransactionFromJson(List history) =>
    (history).map((x) => HistoryTransaction.fromJson(x)).toList();

// String historyTransactionToJson(List<HistoryTransaction> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HistoryTransaction {
  // HistoryTransaction({
  //   this.transId,
  //   this.amountCharged,
  //   this.amountPayed,
  //   this.transactionDate,
  //   this.phoneNumber,
  //   this.deviceId,
  //   this.fullName,
  //   this.email,
  //   this.remarks,
  //   this.status,
  //   this.gatewayStatus,
  //   this.gatewayRef,
  //   this.merchantName,
  //   this.branchId,
  //   this.productId,
  //   this.productTitle,
  //   this.productLastUpdateDate,
  //   this.title,
  //   this.description,
  //   this.shortDescription,
  //   this.images,
  //   this.productType,
  //   this.sellingPrice,
  //   this.pointsNeeded,
  //   this.displayPrice,
  //   this.discountText,
  //   this.isPanel,
  //   this.panelType,
  //   this.discountType,
  //   this.quantity,
  //   this.startDate,
  //   this.expiryDate,
  //   this.withinPeriod,
  //   this.termsCondition,
  //   this.liveChat,
  //   this.sequence,
  //   this.insertDate,
  //   this.lastUpdate,
  //   this.numberOfViews,
  //   this.tags,
  // });
  HistoryTransaction(
      {this.transId,
      this.title,
      this.images,
      this.expiryDate,
      this.transactionDate,
      this.discountText,
      this.discountType,
      this.displayPrice,
      this.productType,
      this.status});

  // String transId;
  // String amountCharged;
  // String amountPayed;
  // DateTime transactionDate;
  // String phoneNumber;
  // String deviceId;
  // dynamic fullName;
  // dynamic email;
  // dynamic remarks;
  // String status;
  // String gatewayStatus;
  // dynamic gatewayRef;
  // dynamic merchantName;
  // String branchId;
  // String productId;
  // String productTitle;
  // DateTime productLastUpdateDate;
  // String title;
  // String description;
  // String shortDescription;
  // String images;
  // String productType;
  // String sellingPrice;
  // dynamic pointsNeeded;
  // String displayPrice;
  // String discountText;
  // String isPanel;
  // String panelType;
  // String discountType;
  // String quantity;
  // String startDate;
  // DateTime expiryDate;
  // dynamic withinPeriod;
  // String termsCondition;
  // dynamic liveChat;
  // String sequence;
  // DateTime insertDate;
  // DateTime lastUpdate;
  // String numberOfViews;
  // dynamic tags;

  String transId;
  String title;
  String images;
  DateTime expiryDate;
  DateTime transactionDate;
  String discountText;
  String discountType;
  String displayPrice;
  String productType;
  String status;
  // factory HistoryTransaction.fromJson(Map<String, dynamic> json) =>
  //     HistoryTransaction(
  //       transId: json["TransID"],
  //       amountCharged: json["AmountCharged"],
  //       amountPayed: json["AmountPayed"],
  //       transactionDate: DateTime.parse(json["TransactionDate"]),
  //       phoneNumber: json["PhoneNumber"],
  //       deviceId: json["DeviceID"],
  //       fullName: json["FullName"],
  //       email: json["Email"],
  //       remarks: json["Remarks"],
  //       status: json["Status"],
  //       gatewayStatus: json["GatewayStatus"],
  //       gatewayRef: json["GatewayRef"],
  //       merchantName: json["MerchantName"],
  //       branchId: json["BranchID"],
  //       productId: json["ProductID"],
  //       productTitle: json["ProductTitle"],
  //       productLastUpdateDate: DateTime.parse(json["ProductLastUpdateDate"]),
  //       title: json["Title"],
  //       description: json["Description"],
  //       shortDescription: json["ShortDescription"],
  //       images: json["Images"],
  //       productType: json["ProductType"],
  //       sellingPrice: json["SellingPrice"],
  //       pointsNeeded: json["PointsNeeded"],
  //       displayPrice: json["DisplayPrice"],
  //       discountText: json["DiscountText"],
  //       isPanel: json["IsPanel"],
  //       panelType: json["PanelType"],
  //       discountType: json["DiscountType"],
  //       quantity: json["Quantity"],
  //       startDate: json["StartDate"],
  //       expiryDate: DateTime.parse(json["ExpiryDate"]),
  //       withinPeriod: json["WithinPeriod"],
  //       termsCondition: json["TermsCondition"],
  //       liveChat: json["LiveChat"],
  //       sequence: json["Sequence"],
  //       insertDate: DateTime.parse(json["InsertDate"]),
  //       lastUpdate: DateTime.parse(json["LastUpdate"]),
  //       numberOfViews: json["NumberOfViews"],
  //       tags: json["Tags"],
  //     );
  factory HistoryTransaction.fromJson(Map<String, dynamic> json) =>
      HistoryTransaction(
          transId: json["TransID"],
          title: json["Title"],
          images: json["Images"],
          expiryDate: DateTime.parse(json["ExpiryDate"]),
          transactionDate: json["TransactionDate"] == null
              ? null
              : DateTime.parse(json["TransactionDate"]),
          discountText: json["DiscountText"],
          discountType: json["DiscountType"],
          displayPrice: json["DisplayPrice"],
          productType: json["ProductType"],
          status: json["Status"]);

  // Map<String, dynamic> toJson() => {
  //       "TransID": transId,
  //       "AmountCharged": amountCharged,
  //       "AmountPayed": amountPayed,
  //       "TransactionDate": transactionDate.toIso8601String(),
  //       "PhoneNumber": phoneNumber,
  //       "DeviceID": deviceId,
  //       "FullName": fullName,
  //       "Email": email,
  //       "Remarks": remarks,
  //       "Status": status,
  //       "GatewayStatus": gatewayStatus,
  //       "GatewayRef": gatewayRef,
  //       "MerchantName": merchantName,
  //       "BranchID": branchId,
  //       "ProductID": productId,
  //       "ProductTitle": productTitle,
  //       "ProductLastUpdateDate": productLastUpdateDate.toIso8601String(),
  //       "Title": title,
  //       "Description": description,
  //       "ShortDescription": shortDescription,
  //       "Images": images,
  //       "ProductType": productType,
  //       "SellingPrice": sellingPrice,
  //       "PointsNeeded": pointsNeeded,
  //       "DisplayPrice": displayPrice,
  //       "DiscountText": discountText,
  //       "IsPanel": isPanel,
  //       "PanelType": panelType,
  //       "DiscountType": discountType,
  //       "Quantity": quantity,
  //       "StartDate": startDate,
  //       "ExpiryDate": expiryDate.toIso8601String(),
  //       "WithinPeriod": withinPeriod,
  //       "TermsCondition": termsCondition,
  //       "LiveChat": liveChat,
  //       "Sequence": sequence,
  //       "InsertDate": insertDate.toIso8601String(),
  //       "LastUpdate": lastUpdate.toIso8601String(),
  //       "NumberOfViews": numberOfViews,
  //       "Tags": tags,
  //     };
}

//   Map<String, dynamic> toJson() => {
//         "TransID": transId,
//         "AmountCharged": amountCharged,
//         "AmountPayed": amountPayed,
//         "TransactionDate": transactionDate.toIso8601String(),
//         "PhoneNumber": phoneNumber,
//         "DeviceID": deviceId,
//         "FullName": fullName,
//         "Email": email,
//         "Remarks": remarks,
//         "Status": status,
//         "GatewayStatus": gatewayStatus,
//         "GatewayRef": gatewayRef,
//         "MerchantName": merchantName,
//         "BranchID": branchId,
//         "ProductID": productId,
//         "ProductTitle": productTitle,
//         "ProductLastUpdateDate": productLastUpdateDate.toIso8601String(),
//       };
// }
