import 'package:flutter/material.dart';

class NotificationState extends ChangeNotifier {
  int _counter = 0;

  int get counter => _counter;
  void add() {
    _counter++;
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  void decrease() {
    _counter = 0;
    print('decrease');
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}
