// To parse this JSON data, do
//
//     final allbranches = allbranchesFromJson(jsonString);

import 'dart:convert';

List<Allbranches> allbranchesFromJson(List branches) =>
    (branches).map((x) => Allbranches.fromJson(x)).toList();

String allbranchesToJson(List<Allbranches> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Allbranches {
  Allbranches({
    this.phoneNumber,
    this.branchId,
    this.name,
    this.latitude,
    this.longitude,
    this.address,
    this.distance,
  });

  String phoneNumber;
  String branchId;
  String name;
  String latitude;
  String longitude;
  String address;
  String distance;

  factory Allbranches.fromJson(Map<String, dynamic> json) => Allbranches(
        phoneNumber: json["PhoneNumber"],
        branchId: json["BranchID"],
        name: json["Name"],
        latitude: json["Latitude"],
        longitude: json["Longitude"],
        address: json["Address"],
        distance: json["Distance"],
      );

  Map<String, dynamic> toJson() => {
        "PhoneNumber": phoneNumber,
        "BranchID": branchId,
        "Name": name,
        "Latitude": latitude,
        "Longitude": longitude,
        "Address": address,
        "Distance": distance,
      };
}
