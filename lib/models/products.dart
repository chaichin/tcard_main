// To parse this JSON data, do
//
//     final products = productsFromJson(jsonString);

import 'dart:convert';

List<Products> productsFromJson(List str) =>
    (str).map((e) => Products.fromJson(e)).toList();

String productsToJson(List<Products> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Products {
  Products({
    this.isPanel,
    this.panelType,
    this.numberOfViews,
    this.sequence,
    this.quantity,
    this.productId,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.sellingPrice,
    this.displayPrice,
    this.pointsNeeded,
    this.discountText,
    this.discountType,
    this.expiryDate,
    this.termsCondition,
    this.status,
    this.insertDate,
    this.lastUpdate,
    this.merchantLogo,
    this.productType,
  });

  String isPanel;
  dynamic panelType;
  String numberOfViews;
  String sequence;
  String quantity;
  String productId;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String displayPrice;
  String pointsNeeded;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;
  String productType;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
        isPanel: json["IsPanel"],
        panelType: json["PanelType"],
        numberOfViews: json["NumberOfViews"],
        sequence: json["Sequence"],
        quantity: json["Quantity"],
        productId: json["ProductID"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        sellingPrice: json["SellingPrice"],
        displayPrice: json["DisplayPrice"],
        pointsNeeded: json["PointsNeeded"],
        discountText: json["DiscountText"],
        discountType: json["DiscountType"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        merchantLogo: json["MerchantLogo"],
        productType: json["ProductType"],
      );

  Map<String, dynamic> toJson() => {
        "IsPanel": isPanel,
        "PanelType": panelType,
        "NumberOfViews": numberOfViews,
        "Sequence": sequence,
        "Quantity": quantity,
        "ProductID": productId,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "DisplayPrice": displayPrice,
        "PointsNeeded": pointsNeeded,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
        "ProductType": productType,
      };
}
