// To parse this JSON data, do
//
//     final merchants = merchantsFromJson(jsonString);

import 'dart:convert';

List<Merchants> merchantsFromJson(List merchants) =>
    (merchants).map((x) => Merchants.fromJson(x)).toList();

String merchantsToJson(List<Merchants> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Merchants {
  Merchants({
    this.panelType,
    this.isPanel,
    this.numberOfViews,
    this.sequence,
    this.quantity,
    this.productId,
    this.productType,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.sellingPrice,
    this.displayPrice,
    this.discountText,
    this.discountType,
    this.expiryDate,
    this.termsCondition,
    this.status,
    this.insertDate,
    this.lastUpdate,
    this.merchantLogo,
  });

  dynamic panelType;
  String isPanel;
  String numberOfViews;
  String sequence;
  String quantity;
  String productId;
  String productType;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String displayPrice;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;

  factory Merchants.fromJson(Map<String, dynamic> json) => Merchants(
        panelType: json["PanelType"],
        isPanel: json["IsPanel"],
        numberOfViews: json["NumberOfViews"],
        sequence: json["Sequence"],
        quantity: json["Quantity"],
        productId: json["ProductID"],
        productType: json["ProductType"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        sellingPrice: json["SellingPrice"],
        displayPrice: json["DisplayPrice"],
        discountText: json["DiscountText"],
        discountType: json["DiscountType"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        merchantLogo: json["MerchantLogo"],
      );

  Map<String, dynamic> toJson() => {
        "PanelType": panelType,
        "IsPanel": isPanel,
        "NumberOfViews": numberOfViews,
        "Sequence": sequence,
        "Quantity": quantity,
        "ProductID": productId,
        "ProductType": productType,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "DisplayPrice": displayPrice,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
      };
}
