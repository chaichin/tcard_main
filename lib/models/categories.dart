// To parse this JSON data, do
//
//     final categories = categoriesFromJson(jsonString);

import 'dart:convert';

// List<Categories> categoriesFromJson(List value) =>
//     (value).map((x) => Categories.fromJson(x)).toList();
// String categoriesToJson(List<Categories> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Categories {
//   Categories({
//     this.categoryName,
//     this.merchants,
//   });

//   String categoryName;
//   List<Merchant> merchants;

//   factory Categories.fromJson(Map<String, dynamic> json) => Categories(
//         categoryName: json["CategoryName"],
//         merchants: List<Merchant>.from(
//             json["Merchants"].map((x) => Merchant.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "CategoryName": categoryName,
//         "Merchants": List<dynamic>.from(merchants.map((x) => x.toJson())),
//       };
// }

// class Merchant {
//   Merchant({
//     this.merchantDetailId,
//     this.name,
//     this.description,
//     this.logo,
//     this.categoryId,
//     this.categoryName,
//     this.sequence,
//   });

//   String merchantDetailId;
//   String name;
//   Description description;
//   String logo;
//   String categoryId;
//   String categoryName;
//   String sequence;

//   factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
//         merchantDetailId: json["MerchantDetailID"],
//         name: json["Name"],
//         description: descriptionValues.map[json["Description"]],
//         logo: json["Logo"],
//         categoryId: json["CategoryID"],
//         categoryName: json["CategoryName"],
//         sequence: json["Sequence"] == null ? null : json["Sequence"],
//       );

//   Map<String, dynamic> toJson() => {
//         "MerchantDetailID": merchantDetailId,
//         "Name": name,
//         "Description": descriptionValues.reverse[description],
//         "Logo": logo,
//         "CategoryID": categoryId,
//         "CategoryName": categoryName,
//         "Sequence": sequence == null ? null : sequence,
//       };
// }

// enum Description { ASD }

// final descriptionValues = EnumValues({"asd": Description.ASD});

// class EnumValues<T> {
//   Map<String, T> map;
//   Map<T, String> reverseMap;

//   EnumValues(this.map);

//   Map<T, String> get reverse {
//     if (reverseMap == null) {
//       reverseMap = map.map((k, v) => new MapEntry(v, k));
//     }
//     return reverseMap;
//   }
// }

List<Categories> categoriesFromJson(List value) =>
    (value).map((x) => Categories.fromJson(x)).toList();
// List<Categories> categoriesFromJson(List value) =>
//     (value).map((x) => Categories.fromJson(x)).toList();
String categoriesToJson(List<Categories> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Categories {
  Categories({
    this.categoryName,
    this.merchants,
  });

  String categoryName;
  Merchant merchants;

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
        categoryName: json["CategoryName"],
        merchants: Merchant.fromJson(json["Merchants"]),
      );

  Map<String, dynamic> toJson() => {
        "CategoryName": categoryName,
        "Merchants": merchants.toJson(),
      };
}

class Merchant {
  Merchant({
    this.merchantDetailId,
    this.name,
    this.logo,
    this.categoryId,
    this.categoryName,
    this.sequence,
  });

  String merchantDetailId;
  String name;
  String logo;
  String categoryId;
  String categoryName;
  String sequence;

  factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
        merchantDetailId: json["MerchantDetailID"],
        name: json["Name"],
        logo: json["Logo"],
        categoryId: json["CategoryID"],
        categoryName: json["CategoryName"],
        sequence: json["Sequence"] == null ? null : json["Sequence"],
      );

  Map<String, dynamic> toJson() => {
        "MerchantDetailID": merchantDetailId,
        "Name": name,
        "Logo": logo,
        "CategoryID": categoryId,
        "CategoryName": categoryName,
        "Sequence": sequence == null ? null : sequence,
      };
}
