import 'dart:convert';

DialogImage dialogImageFromJson(String str) =>
    DialogImage.fromJson(json.decode(str));

String dialogImageToJson(DialogImage data) => json.encode(data.toJson());

class DialogImage {
  DialogImage({
    this.carouselId,
    this.title,
    this.description,
    this.type,
    this.buttonText,
    this.isPanel,
    this.panelType,
    this.image,
    this.productId,
    this.url,
    this.sequence,
    this.status,
    this.insertBy,
    this.insertDate,
    this.lastUpdateDate,
  });

  String carouselId;
  String title;
  String description;
  String type;
  String buttonText;
  String isPanel;
  dynamic panelType;
  String image;
  dynamic productId;
  dynamic url;
  String sequence;
  String status;
  String insertBy;
  DateTime insertDate;
  dynamic lastUpdateDate;

  factory DialogImage.fromJson(Map<String, dynamic> json) => DialogImage(
        carouselId: json["CarouselID"],
        title: json["Title"],
        description: json["Description"],
        type: json["Type"],
        buttonText: json["ButtonText"],
        isPanel: json["IsPanel"],
        panelType: json["PanelType"],
        image: json["Image"],
        productId: json["ProductID"],
        url: json["Url"],
        sequence: json["Sequence"],
        status: json["Status"],
        insertBy: json["InsertBy"],
        lastUpdateDate: json["LastUpdateDate"],
      );

  Map<String, dynamic> toJson() => {
        "CarouselID": carouselId,
        "Title": title,
        "Description": description,
        "Type": type,
        "ButtonText": buttonText,
        "IsPanel": isPanel,
        "PanelType": panelType,
        "Image": image,
        "ProductID": productId,
        "Url": url,
        "Sequence": sequence,
        "Status": status,
        "InsertBy": insertBy,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdateDate": lastUpdateDate,
      };
}
