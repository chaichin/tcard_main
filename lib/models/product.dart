// To parse this JSON data, do
//
//     final product = productFromJson(jsonString);

import 'dart:convert';

// Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product(
      {this.quantity,
      this.productId,
      this.title,
      this.description,
      this.shortDescription,
      this.images,
      this.sellingPrice,
      this.pointsNeeded,
      this.displayPrice,
      this.discountText,
      this.discountType,
      this.expiryDate,
      this.termsCondition,
      this.status,
      this.insertDate,
      this.lastUpdate,
      this.merchantLogo,
      this.merchantDetailId,
      this.branches,
      this.branchesCoordinates,
      this.nearestBranch,
      this.nearestBranchAddress,
      this.nearestBranchLatitude,
      this.nearestBranchLongitude,
      this.nearestBranchDistance,
      this.shouldShowAppointment,
      this.type,
      this.numberofViews,
      this.chatLink,
      this.merchantName,
      this.sellingPriceWithPoint,
      this.hasBeenRedeemed});

  String quantity;
  String productId;
  String title;
  String description;
  String shortDescription;
  String images;
  String sellingPrice;
  String pointsNeeded;
  String displayPrice;
  String discountText;
  String discountType;
  DateTime expiryDate;
  String termsCondition;
  String status;
  DateTime insertDate;
  DateTime lastUpdate;
  String merchantLogo;
  String merchantDetailId;
  String branches;
  String branchesCoordinates;
  String nearestBranch;
  String nearestBranchAddress;
  String nearestBranchLatitude;
  String nearestBranchLongitude;
  String nearestBranchDistance;
  bool shouldShowAppointment;
  String type;
  String numberofViews;
  String chatLink;
  String merchantName;
  String sellingPriceWithPoint;
  bool hasBeenRedeemed;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      quantity: json["Quantity"],
      productId: json["ProductID"],
      title: json["Title"],
      description: json["Description"],
      shortDescription: json["ShortDescription"],
      images: json["Images"],
      sellingPrice: json["SellingPrice"],
      pointsNeeded: json["PointsNeeded"],
      displayPrice: json["DisplayPrice"],
      discountText: json["DiscountText"],
      discountType: json["DiscountType"],
      expiryDate: DateTime.parse(json["ExpiryDate"]),
      termsCondition: json["TermsCondition"],
      status: json["Status"],
      insertDate: DateTime.parse(json["InsertDate"]),
      lastUpdate: DateTime.parse(json["LastUpdate"]),
      merchantLogo: json["MerchantLogo"],
      merchantDetailId: json["MerchantDetailID"],
      branches: json["Branches"],
      branchesCoordinates: json["BranchesCoordinates"],
      nearestBranch: json["NearestBranch"],
      nearestBranchAddress: json["NearestBranchAddress"],
      nearestBranchLatitude: json["NearestBranchLatitude"],
      nearestBranchLongitude: json["NearestBranchLongitude"],
      nearestBranchDistance: json["NearestBranchDistance"],
      shouldShowAppointment: json["ShouldShowAppointment"],
      type: json["Type"],
      numberofViews: json["NumberOfViews"],
      chatLink: json["LiveChat"],
      merchantName: json["MerchantName"],
      sellingPriceWithPoint: json["SellingPriceWithPoints"],
      hasBeenRedeemed: json["HasBeenRedeemed"]);

  Map<String, dynamic> toJson() => {
        "Quantity": quantity,
        "ProductID": productId,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "SellingPrice": sellingPrice,
        "PointsNeeded": pointsNeeded,
        "DisplayPrice": displayPrice,
        "DiscountText": discountText,
        "DiscountType": discountType,
        "ExpiryDate": expiryDate.toIso8601String(),
        "TermsCondition": termsCondition,
        "Status": status,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "MerchantLogo": merchantLogo,
        "MerchantDetailID": merchantDetailId,
        "Branches": branches,
        "BranchesCoordinates": branchesCoordinates,
        "NearestBranch": nearestBranch,
        "NearestBranchAddress": nearestBranchAddress,
        "NearestBranchLatitude": nearestBranchLatitude,
        "NearestBranchLongitude": nearestBranchLongitude,
        "NearestBranchDistance": nearestBranchDistance,
        "ShouldShowAppointment": shouldShowAppointment,
        "LiveChat": chatLink,
        "MerchantName": merchantName
      };
}
