// To parse this JSON data, do
//
//     final search = searchFromJson(jsonString);

import 'dart:convert';

List<Search> searchFromJson(List str) =>
    (str).map((e) => Search.fromJson(e)).toList();

String searchToJson(List<Search> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Search {
  Search({
    this.productId,
    this.title,
    this.description,
    this.shortDescription,
    this.images,
    this.productType,
    this.sellingPrice,
    this.pointsNeeded,
    this.displayPrice,
    this.discountText,
    this.isPanel,
    this.panelType,
    this.discountType,
    this.quantity,
    this.startDate,
    this.expiryDate,
    this.withinPeriod,
    this.termsCondition,
    this.status,
    this.liveChat,
    this.sequence,
    this.insertDate,
    this.lastUpdate,
    this.numberOfViews,
    this.tags,
    this.merchantLogo,
  });

  String productId;
  String title;
  String description;
  String shortDescription;
  String images;
  String productType;
  String sellingPrice;
  String pointsNeeded;
  String displayPrice;
  String discountText;
  String isPanel;
  dynamic panelType;
  String discountType;
  String quantity;
  String startDate;
  DateTime expiryDate;
  dynamic withinPeriod;
  String termsCondition;
  String status;
  dynamic liveChat;
  String sequence;
  DateTime insertDate;
  DateTime lastUpdate;
  String numberOfViews;
  String tags;
  String merchantLogo;

  factory Search.fromJson(Map<String, dynamic> json) => Search(
        productId: json["ProductID"],
        title: json["Title"],
        description: json["Description"],
        shortDescription: json["ShortDescription"],
        images: json["Images"],
        productType: json["ProductType"],
        sellingPrice: json["SellingPrice"],
        pointsNeeded: json["PointsNeeded"],
        displayPrice: json["DisplayPrice"],
        discountText: json["DiscountText"],
        isPanel: json["IsPanel"],
        panelType: json["PanelType"],
        discountType: json["DiscountType"],
        quantity: json["Quantity"],
        startDate: json["StartDate"],
        expiryDate: DateTime.parse(json["ExpiryDate"]),
        withinPeriod: json["WithinPeriod"],
        termsCondition: json["TermsCondition"],
        status: json["Status"],
        liveChat: json["LiveChat"],
        sequence: json["Sequence"],
        insertDate: DateTime.parse(json["InsertDate"]),
        lastUpdate: DateTime.parse(json["LastUpdate"]),
        numberOfViews: json["NumberOfViews"],
        tags: json["Tags"],
        merchantLogo: json["MerchantLogo"],
      );

  Map<String, dynamic> toJson() => {
        "ProductID": productId,
        "Title": title,
        "Description": description,
        "ShortDescription": shortDescription,
        "Images": images,
        "ProductType": productType,
        "SellingPrice": sellingPrice,
        "PointsNeeded": pointsNeeded,
        "DisplayPrice": displayPrice,
        "DiscountText": discountText,
        "IsPanel": isPanel,
        "PanelType": panelType,
        "DiscountType": discountType,
        "Quantity": quantity,
        "StartDate": startDate,
        "ExpiryDate": expiryDate.toIso8601String(),
        "WithinPeriod": withinPeriod,
        "TermsCondition": termsCondition,
        "Status": status,
        "LiveChat": liveChat,
        "Sequence": sequence,
        "InsertDate": insertDate.toIso8601String(),
        "LastUpdate": lastUpdate.toIso8601String(),
        "NumberOfViews": numberOfViews,
        "Tags": tags,
        "MerchantLogo": merchantLogo,
      };
}
