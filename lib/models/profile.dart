// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

Profile profileFromJson(String str) => Profile.fromJson(json.decode(str));

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
  Profile({
    this.phoneNumber,
    this.deviceType,
    this.name,
    this.gender,
    this.isPanel,
    this.panelId,
    this.dateOfBirth,
    this.email,
    this.pointBalance,
    this.fcmToken,
    this.huaweiToken,
    this.birthdayVoucherReceiveDate,
    this.fcmLastUpdated,
    this.checkFcm,
    this.hwmLastUpdated,
    this.checkHwmToken,
    this.panelImage,
  });

  String phoneNumber;
  String deviceType;
  String name;
  String gender;
  String isPanel;
  String panelId;
  DateTime dateOfBirth;
  String email;
  String pointBalance;
  String fcmToken;
  dynamic huaweiToken;
  String birthdayVoucherReceiveDate;
  DateTime fcmLastUpdated;
  String checkFcm;
  dynamic hwmLastUpdated;
  dynamic checkHwmToken;
  String panelImage;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        phoneNumber: json["PhoneNumber"],
        deviceType: json["DeviceType"],
        name: json["Name"],
        gender: json["Gender"],
        isPanel: json["IsPanel"],
        panelId: json["PanelID"],
        dateOfBirth: DateTime.parse(json["DateOfBirth"]),
        email: json["Email"],
        pointBalance: json["PointBalance"],
        fcmToken: json["FCMToken"],
        huaweiToken: json["HuaweiToken"],
        birthdayVoucherReceiveDate: json["BirthdayVoucherReceiveDate"],
        fcmLastUpdated: DateTime.parse(json["FCMLastUpdated"]),
        checkFcm: json["CheckFCM"],
        hwmLastUpdated: json["HWMLastUpdated"],
        checkHwmToken: json["CheckHWMToken"],
        panelImage: json["PanelImage"],
      );

  Map<String, dynamic> toJson() => {
        "PhoneNumber": phoneNumber,
        "DeviceType": deviceType,
        "Name": name,
        "Gender": gender,
        "IsPanel": isPanel,
        "PanelID": panelId,
        "DateOfBirth": dateOfBirth.toIso8601String(),
        "Email": email,
        "PointBalance": pointBalance,
        "FCMToken": fcmToken,
        "HuaweiToken": huaweiToken,
        "BirthdayVoucherReceiveDate": birthdayVoucherReceiveDate,
        "FCMLastUpdated": fcmLastUpdated.toIso8601String(),
        "CheckFCM": checkFcm,
        "HWMLastUpdated": hwmLastUpdated,
        "CheckHWMToken": checkHwmToken,
        "PanelImage": panelImage,
      };
}
