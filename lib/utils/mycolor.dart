import 'dart:ui';

// D0F4EE (Light)
// 4BC6BA (Medium)
// 008B80 (Dark)

class Mycolor {
  static const Color normalWhite = const Color(0xFFFFFFFF);
  static const Color white = const Color(0xFFF8F8F8);
  static const Color lightGreen = const Color(0xFFD0F4EE);
  static const Color mediumGreen = const Color(0xFF4BC6BA);
  static const Color darkGreen = const Color(0xFF018B80);
  static const Color darkerGreen = const Color(0xFF004343);
  static const Color urlBlue = const Color(0xFF3F8AD7);
  static const Color grey = const Color(0xFFE8E8E8);
  static const Color darkgrey = const Color(0xFFc1c1c1);
  static const Color darkergrey = const Color(0xFF888888);
  static const Color highlightGreen = const Color(0xFF3C8980);
  static const Color startColor = const Color(0xFFCEEBEF);
  static const Color endColor = const Color(0xFFFACFE2);
  static const Color redOrange = const Color(0xFFF26A6B);
}
