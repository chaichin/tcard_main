import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:huawei_push/local_notification/local_notification.dart';
import 'package:huawei_push/model/remote_message.dart';
import 'package:huawei_push/push.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/main.dart';
import 'package:tcard/models/Notification.dart';
import 'package:flutter/scheduler.dart';
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:url_launcher/url_launcher.dart';

class HuaweiNotification extends ChangeNotifier {
  void getToken() {
    Push.getToken("");
    //Push.getNotifications();

    Push.getTokenStream.listen(_onTokenEvent, onError: _onTokenError);
    Push.onMessageReceivedStream
        .listen(onMessageReceived, onError: onMessageReceiveError);
    Push.registerBackgroundMessageHandler(
            HuaweiNotification.backgroundMessageCallback)
        .then((value) => print('test' + value.toString()));
    // Push.onLocalNotificationClick.listen((event) {
    //   print('clicks' + event.toString());
    // });
    //  Push.getIntentStream.listen(_onNewIntent);

    Push.onNotificationOpenedApp.listen(_onNotificationOpenedApp);
    // getInitialNotification();
  }

  void getInitialNotification() async {
    var initialNotification = await Push.getInitialNotification();
    print("getInitialNotification: " + initialNotification.toString());
    Notifications notification = Notifications(
        null,
        initialNotification['extras']['title'],
        initialNotification['extras']['image'],
        initialNotification['extras']['message'],
        initialNotification['extras']['productId'],
        initialNotification['extras']['url'],
        initialNotification['extras']['vTransId'],
        initialNotification['extras']['type'],
        DateTime.now().toString(),
        'unseen');
    if (initialNotification['extras']['title'] != null) {
      DBHelper().save(notification);
    }
    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialNotification['extras']['type'] == 'PRODUCT') {
      Timer(Duration(seconds: 3), () {
        NavKey.navKey.currentState.push(MaterialPageRoute(
            builder: (context) => ShoppingPage(
                  productid: initialNotification['extras']['productId'],
                )));
      });
    } else if (initialNotification['extras']['type'] == 'URL') {
      if (await canLaunch(
        initialNotification['extras']['url'],
      )) {
        await launch(initialNotification['extras']['url']);
      } else {
        throw 'Could not launch ${initialNotification['extras']['url']}';
      }
    } else if (initialNotification['extras']['type'] == 'VOUCHER') {
      Timer(Duration(seconds: 3), () {
        NavKey.navKey.currentState.push(MaterialPageRoute(
            builder: (context) => VoucherDescriptionPage(
                  voucherid: initialNotification['extras']['vTransId'],
                )));
      });
    }
  }

  void _onNotificationOpenedApp(var remoteMessage) async {
    print("onNotificationOpenedApp: " + remoteMessage.toString());
    Notifications notification = Notifications(
        null,
        remoteMessage['extras']['title'],
        remoteMessage['extras']['image'],
        remoteMessage['extras']['message'],
        remoteMessage['extras']['productId'],
        remoteMessage['extras']['url'],
        remoteMessage['extras']['vTransId'],
        remoteMessage['extras']['type'],
        DateTime.now().toString(),
        'unseen');
    DBHelper().save(notification);

    if (remoteMessage['extras']['type'] == 'PRODUCT') {
      NavKey.navKey.currentState.push(MaterialPageRoute(
          builder: (context) =>
              ShoppingPage(productid: remoteMessage['extras']['productId'])));
    } else if (remoteMessage['extras']['type'] == 'URL') {
      if (await canLaunch(
        remoteMessage['extras']['url'],
      )) {
        await launch(remoteMessage['extras']['url']);
      } else {
        throw 'Could not launch ${remoteMessage['extras']['url']}';
      }
    } else if (remoteMessage['extras']['type'] == 'VOUCHER') {
      NavKey.navKey.currentState.push(MaterialPageRoute(
          builder: (context) => VoucherDescriptionPage(
                voucherid: remoteMessage['extras']['vTransId'],
              )));
    }
  }

  _onTokenEvent(String event) {
    PostData().updateHuaweitoken(event).then((value) => print(value));
    print("Token obtained: " + event);
  }

  _onTokenError(Object error) {
    PlatformException e = error;
    print("TokenErrorEvent" + e.message);
    getToken();
  }

  void increment() {
    notifyListeners();
    print('added');
  }

  void onMessageReceived(RemoteMessage message) {
    // String data = remoteMessage.data;
    print(message.notification);
    Notifications notification = Notifications(
        null,
        message.dataOfMap['title'],
        message.dataOfMap['image'],
        message.dataOfMap['message'],
        message.dataOfMap['productId'],
        message.dataOfMap['url'],
        message.dataOfMap['vTransId'],
        message.dataOfMap['type'],
        DateTime.now().toString(),
        'unseen');

    DBHelper().save(notification);
    increment();

    Push.localNotification({
      HMSLocalNotificationAttr.TITLE: message.dataOfMap['title'],
      HMSLocalNotificationAttr.MESSAGE: message.dataOfMap['message'],
      HMSLocalNotificationAttr.SMALL_ICON: '@mipmap/logo',
      HMSLocalNotificationAttr.LARGE_ICON_URL: message.dataOfMap['image'],
      HMSLocalNotificationAttr.IMPORTANCE: Importance.MAX,
      HMSLocalNotificationAttr.PRIORITY: NotificationPriority.MAX,
      HMSLocalNotificationAttr.ACTION: 'yes'
    });
    if (message.notification != null) {
      Push.localNotification({
        HMSLocalNotificationAttr.TITLE: message.notification.title,
        HMSLocalNotificationAttr.MESSAGE: message.notification.body,
        HMSLocalNotificationAttr.SMALL_ICON: '@mipmap/logo',
        HMSLocalNotificationAttr.LARGE_ICON_URL: message.dataOfMap['image'],
        HMSLocalNotificationAttr.IMPORTANCE: Importance.MAX,
        HMSLocalNotificationAttr.PRIORITY: NotificationPriority.MAX,
        HMSLocalNotificationAttr.ACTION: 'yes'
      });
    }
    // NavKey.navKey.currentState.push(MaterialPageRoute(
    //     builder: (context) => ShoppingPage(
    //           productid: message.dataOfMap['productId'],
    //         )));
  }

  void onMessageReceiveError(Object error) {
    print('error messaging ' + error.toString());
  }

  static void backgroundMessageCallback(RemoteMessage message) async {
    print('please');
    Notifications notification = Notifications(
        null,
        message.dataOfMap['title'],
        message.dataOfMap['image'],
        message.dataOfMap['message'],
        message.dataOfMap['productId'],
        message.dataOfMap['url'],
        message.dataOfMap['vTransId'],
        message.dataOfMap['type'],
        DateTime.now().toString(),
        'unseen');

    // Notifications notification = Notifications(null, null, null, null, null,
    //     null, null, null, DateTime.now().toString(), 'unseen');
    DBHelper().save(notification);
    HuaweiNotification().increment();
    Push.localNotification({
      HMSLocalNotificationAttr.TITLE: message.dataOfMap['title'],
      HMSLocalNotificationAttr.MESSAGE: message.dataOfMap['message'],
      HMSLocalNotificationAttr.SMALL_ICON: '@mipmap/logo',
      HMSLocalNotificationAttr.LARGE_ICON_URL: message.dataOfMap['image'],
      HMSLocalNotificationAttr.IMPORTANCE: Importance.MAX,
      HMSLocalNotificationAttr.PRIORITY: NotificationPriority.MAX,
    });
  }

  void _onNewIntent(String intentString) {
    // For navigating to the custom intent page (deep link)
    // The custom intent that sent from the push kit console is:
    // app://open.my.app/CustomIntentPage
    if (intentString != null) {
      print('CustomIntentEvent: ' + intentString);
      List parsedString = intentString.split("://open.my.app/");
      if (parsedString[1] == "CustomIntentPage") {
        // Schedule the navigation after the widget is builded.
        SchedulerBinding.instance.addPostFrameCallback((_) {
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) => CustomIntentPage()));
          NavKey.navKey.currentState.push(MaterialPageRoute(
              builder: (context) => ShoppingPage(
                    productid: parsedString,
                  )));
        });
      }
    }
  }
}
