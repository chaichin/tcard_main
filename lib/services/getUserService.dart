import 'package:shared_preferences/shared_preferences.dart';

class User {
  Future<List<String>> profile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getStringList('profile');
  }

  Future<String> point() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString('PointBalance');
  }
}
