import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tcard/config/postData.dart';
import 'package:tcard/local_database/dp_helper.dart';
import 'package:tcard/models/Notification.dart';
import 'dart:io' show File;
import 'package:http/http.dart' as http;
import 'package:tcard/screens/shopping_page.dart';
import 'package:tcard/screens/voucherDescription_page.dart';
import 'package:url_launcher/url_launcher.dart';
import '../main.dart';

class NotificationService extends ChangeNotifier {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  // final GlobalKey context;
  // NotificationService({this.context});
  FlutterLocalNotificationsPlugin fltrNotification;
  static final flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  var type;

  Future<void> showNotification(RemoteMessage message) async {
    var attachmentPicture = message.data['image'] == null
        ? null
        : await downloadAndSaveFile(message.data['image'], 'attachment.png');
    // var bigPictureStyleInformation =
    //     BigPictureStyleInformation(FilePathAndroidBitmap(attachmentPicture));
    print(message.notification);
    var payload;
    type = message.data['type'];
    if (message.data['type'] == 'URL') {
      payload = message.data['url'];
    } else if (message.data['type'] == 'PRODUCT') {
      payload = message.data['productId'];
    } else if (message.data['type'] == 'VOUCHER') {
      payload = message.data['vTransId'];
    }
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'channel id', 'channel name', 'channel description',
        importance: Importance.max,
        priority: Priority.high,
        largeIcon: FilePathAndroidBitmap(attachmentPicture));
    // styleInformation:
    //     message.data['image'] == null ? null : bigPictureStyleInformation);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    await NotificationService.flutterLocalNotificationsPlugin.show(
        message.hashCode,
        message.data['title'],
        message.data['message'],
        platformChannelSpecifics,
        payload: payload);

    increment();
    //print(checkcounter);
  }

  downloadAndSaveFile(String url, String filename) async {
    var directory = await getApplicationDocumentsDirectory();
    var filePath = "${directory.path}/$filename";
    var response = await http.get(url);
    var file = File(filePath);
    await file.writeAsBytes(response.bodyBytes);
    return filePath;
  }

  // Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  //   // If you're going to use other Firebase services in the background, such as Firestore,
  //   // make sure you call `initializeApp` before using other Firebase services.
  //   //await Firebase.initializeApp();

  //   _showNotification(message);
  // }

  void increment() {
    notifyListeners();
    print('added');
  }

  void fcm() async {
    Firebase.initializeApp();
    var androidInitilize = new AndroidInitializationSettings('@mipmap/logo');
    var iOSInitilize = IOSInitializationSettings();
    var initilizationSettings =
        InitializationSettings(android: androidInitilize, iOS: iOSInitilize);
    fltrNotification = FlutterLocalNotificationsPlugin();
    fltrNotification.initialize(initilizationSettings,
        onSelectNotification: notificationSelected);
    _firebaseMessaging.getToken().then((token) {
      print("token " + token);
      PostData().updateFCMtoken(token, 'Y').then((value) => print(value.data));
    });

    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    print('User granted permission: ${settings.authorizationStatus}');

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');
      Notifications notification = Notifications(
          null,
          message.data['title'],
          message.data['image'],
          message.data['message'],
          message.data['productId'],
          message.data['url'],
          message.data['vTransId'],
          message.data['type'],
          DateTime.now().toString(),
          'unseen');

      DBHelper().save(notification);

      showNotification(message);
    });

    // FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  }

  Future openApp(String payload) async {
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage?.data['type'] == 'PRODUCT') {
      NavKey.navKey.currentState.push(MaterialPageRoute(
          builder: (context) => ShoppingPage(
                productid: payload,
              )));
    }
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      if (message.data['type'] == 'PRODUCT') {
        NavKey.navKey.currentState.push(MaterialPageRoute(
            builder: (context) => ShoppingPage(
                  productid: payload,
                )));
      }
    });
  }

  Future notificationSelected(String payload) async {
    print('paylo' + payload);
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    if (type == 'PRODUCT') {
      NavKey.navKey.currentState.push(MaterialPageRoute(
          builder: (context) => ShoppingPage(
                productid: payload,
              )));
    } else if (type == 'URL') {
      if (await canLaunch(
        payload,
      )) {
        await launch(payload);
      } else {
        throw 'Could not launch $payload';
      }
    } else if (type == 'VOUCHER') {
      NavKey.navKey.currentState.push(MaterialPageRoute(
          builder: (context) => VoucherDescriptionPage(
                voucherid: payload,
              )));
    }

    // await Navigator.push(
    //   context,
    //   MaterialPageRoute<void>(
    //       builder: (context) => ShoppingPage(productid: payload)),
    // );
  }
}
