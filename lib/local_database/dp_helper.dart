import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tcard/models/Notification.dart';
import 'package:path/path.dart';

class DBHelper extends ChangeNotifier {
  static Database _db;
  static const String ID = 'id';
  static const String Title = 'title';
  static const String Image = 'image';
  static const String Value = 'value';
  static const String ProductID = 'productid';
  static const String VoucherID = 'voucherid';
  static const String Type = 'type';
  static const String Url = 'url';
  static const String Day = 'day';
  static const String Status = "status";
  static const String TABLE = 'Notification';
  static const String DB_NAME = 'notification.db';
  Future<Database> get db async {
    if (_db != null) {
      print('got db');
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    // io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    // String path = join(documentsDirectory.path, DB_NAME);
    // var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    // return db;

    var dir = await getDatabasesPath();
    var path = join(dir, DB_NAME);

    var db = await openDatabase(path, version: 1, onCreate: (db, version) {
      db.execute(
          '''CREATE TABLE $TABLE($ID INTEGER PRIMARY KEY,$Title TEXT,$Image TEXT,$Value TEXT,$ProductID TEXT,$Url TEXT,$VoucherID TEXT,$Type TEXT,$Day TEXT,$Status TEXT)''');
    });
    print('created db');
    return db;
  }

  // _onCreate(Database db, int version) async {
  //   await db.execute(
  //       "CREATE TABLE $TABLE($ID INTEGER PRIMARY KEY,$Title TEXT,$Image TEXT,$Value TEXT,$ProductID INTEGER,$Url TEXT");
  // }

  Future<Notifications> save(Notifications notification) async {
    var dbClient = await db;
    notification.id = await dbClient.insert(TABLE, notification.toMap());
    return notification;
    // await dbClient.transaction((txn) async {
    //   var query =
    //       "INSERT INTO $TABLE ($Title,$Image,$Value,$ProductID,$Url) VALUES('" +
    //           notification.title +
    //           "," +
    //           notification.image +
    //           "," +
    //           notification.value +
    //           ",${notification.id}," +
    //           notification.url +
    //           "')";
    //   return await txn.rawInsert(query);
    // });
  }

  Future<List<Notifications>> getNotifications() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE, columns: [
      ID,
      Title,
      Image,
      Value,
      ProductID,
      Url,
      VoucherID,
      Type,
      Day,
      Status
    ]);
    // List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<Notifications> notification = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        notification.add(Notifications.fromMap(maps[i]));
      }
    }
    updateAll();
    return notification;
  }

  Future<int> getCount() async {
    var dbClient = await db;
    return Sqflite.firstIntValue(await dbClient
        .rawQuery('SELECT COUNT(*) FROM $TABLE WHERE $Status="unseen"'));
  }

  // Future<List<Notifications>> getNotificationsNumber() async {
  //   var dbClient = await db;
  //   List<Map> maps = await dbClient.query(TABLE,
  //       columns: [
  //         ID,
  //         Title,
  //         Image,
  //         Value,
  //         ProductID,
  //         Url,
  //         VoucherID,
  //         Type,
  //         Day,
  //         Status
  //       ],
  //       where: '$Status=unseen');
  //   // List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
  //   List<Notifications> notification = [];
  //   if (maps.length > 0) {
  //     for (int i = 0; i < maps.length; i++) {
  //       notification.add(Notifications.fromMap(maps[i]));
  //     }
  //   }
  //   return notification;
  // }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(TABLE, where: '$ID=?', whereArgs: [id]);
  }

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.delete(TABLE);
  }

  Future<int> update(Notifications notification) async {
    var dbClient = await db;
    return await dbClient.update(TABLE, notification.toMap(),
        where: '$ID=?', whereArgs: [notification.id]);
  }

  Future<int> updateAll() async {
    var dbClient = await db;
    Map<String, dynamic> row = {
      'status': 'seen',
    };
    print("updated");
    notifyListeners();
    return await dbClient.update(TABLE, row);
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }

  Future deletedatabase() async {
    var dir = await getDatabasesPath();
    var path = dir + DB_NAME;
    // String path = join(dir, 'notification.db');
// Delete the database
    await deleteDatabase(path);
    print('db deleted');
  }
}
