package com.tdental.tcard
import io.flutter.embedding.android.FlutterActivity
import com.huawei.hms.flutter.push.PushPlugin;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import com.tekartik.sqflite.SqflitePlugin
class MainApplication : FlutterApplication(), PluginRegistry.PluginRegistrantCallback {

    override fun onCreate() {
        super.onCreate()
        PushPlugin.setPluginRegistrant(this)  
      
    }

    override fun registerWith(registry: PluginRegistry?) {
        if (!registry!!.hasPlugin("com.huawei.hms.flutter.push.PushPlugin")) {
            PushPlugin.registerWith(registry?.registrarFor("com.huawei.hms.flutter.push.PushPlugin"))
            
        } 
            SqflitePlugin.registerWith(registry?.registrarFor("com.tekartik.sqflite.SqflitePlugin"))
        
    }
}